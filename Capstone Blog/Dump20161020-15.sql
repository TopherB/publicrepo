CREATE DATABASE  IF NOT EXISTS `tmcblog` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tmcblog`;
-- MySQL dump 10.13  Distrib 5.5.52, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: tmcblog
-- ------------------------------------------------------
-- Server version	5.5.52-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorities` (
  `username` varchar(20) NOT NULL,
  `authority` varchar(20) NOT NULL,
  KEY `username` (`username`),
  CONSTRAINT `authorities_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES ('Admin','ROLE_ADMIN'),('Assistant','ROLE_USER'),('Admin','ROLE_USER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(75) NOT NULL,
  `blog_content` longtext NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `enabled` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=355 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (342,'Lorem Ipsum','<p>Accumsan ante bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>\n<p>#Lorem #Ipsum</p>','2016-10-19 03:00:00',NULL,1),(343,'Lorem Ipsum','<p>#Accumsan #ante #bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-10-18 03:00:00',NULL,1),(344,'Lorem Ipsum','<p>Accumsan ante bibendum #conubia #diam #egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-10-17 03:00:00',NULL,1),(345,'Lorem Impsum','<p>Accumsan ante bibendum conubia diam egestas et #facilisis #feugiat #gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-10-15 03:00:00',NULL,1),(346,'Lorem Ipsum','<p>Accumsan ante bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-10-14 03:00:00',NULL,1),(347,'Lorem Ipsum','<p>Accumsan ante bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-10-08 03:00:00',NULL,1),(348,'Lorem Ipsum','<p>Accumsan ante bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-09-29 03:00:00',NULL,1),(349,'Lorem Ipsum','<p>Accumsan ante bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-09-28 03:00:00',NULL,1),(350,'Lorem Ipsum','<p>Accumsan ante bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-10-02 03:00:00',NULL,1),(351,'Lorem Ipsum','<p>Accumsan ante bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-09-25 03:00:00',NULL,1),(352,'Lorem Ipsum','<p>Accumsan ante bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-09-25 03:00:00',NULL,1),(353,'Lorem PIpsum','<p>Accumsan ante bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-09-29 00:00:00',NULL,0),(354,'Bouncy Balls reign supreme','<p>Yes, they do</p>','2016-10-17 03:00:00',NULL,0);
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_hashtag`
--

DROP TABLE IF EXISTS `blog_hashtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_hashtag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `blog_id` bigint(20) NOT NULL,
  `hashtag_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_blog_hashtag_1_idx` (`blog_id`),
  KEY `fk_blog_hashtag_2_idx` (`hashtag_id`),
  CONSTRAINT `fk_blog_hashtag_1` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_blog_hashtag_2` FOREIGN KEY (`hashtag_id`) REFERENCES `hashtags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_hashtag`
--

LOCK TABLES `blog_hashtag` WRITE;
/*!40000 ALTER TABLE `blog_hashtag` DISABLE KEYS */;
INSERT INTO `blog_hashtag` VALUES (1,342,2),(2,342,1),(3,343,3),(4,343,4),(5,343,5),(6,344,6),(7,344,7),(8,344,8),(9,345,9),(10,345,10),(11,345,11);
/*!40000 ALTER TABLE `blog_hashtag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_UNIQUE` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,''),(4,'Bibendum'),(3,'Ipsum'),(2,'Lorem');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_blog`
--

DROP TABLE IF EXISTS `categories_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_blog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) NOT NULL,
  `blog_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_categories_blog_1_idx` (`category_id`),
  KEY `fk_categories_blog_2_idx` (`blog_id`),
  CONSTRAINT `fk_categories_blog_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_categories_blog_2` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_blog`
--

LOCK TABLES `categories_blog` WRITE;
/*!40000 ALTER TABLE `categories_blog` DISABLE KEYS */;
INSERT INTO `categories_blog` VALUES (2,3,343),(4,2,344),(6,3,346),(8,4,347),(10,2,349),(12,2,354);
/*!40000 ALTER TABLE `categories_blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_staticpages`
--

DROP TABLE IF EXISTS `categories_staticpages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_staticpages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) NOT NULL,
  `static_page_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_categories_staticpages_1_idx` (`category_id`),
  KEY `fk_categories_staticpages_2_idx` (`static_page_id`),
  CONSTRAINT `fk_categories_staticpages_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_categories_staticpages_2` FOREIGN KEY (`static_page_id`) REFERENCES `static_pages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_staticpages`
--

LOCK TABLES `categories_staticpages` WRITE;
/*!40000 ALTER TABLE `categories_staticpages` DISABLE KEYS */;
INSERT INTO `categories_staticpages` VALUES (1,2,9),(2,4,10);
/*!40000 ALTER TABLE `categories_staticpages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(75) NOT NULL,
  `author` varchar(75) NOT NULL,
  `comment` longtext NOT NULL,
  `email` varchar(75) NOT NULL,
  `date` date DEFAULT NULL,
  `blog_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (8,'LOL','John Donne','THIS WEBSITE LOOKS LIKE IT WAS MADE BY A 7 YEAR OLD LOLOLOL','johndonne@gmail.com','2016-10-20',342),(9,'BEAUTIFUL WEBSITE','Def not the admin','I think it looks great given the resources given','admin@superbouncyballs.com','2016-10-20',342),(10,'WHERE IS BOUNCY BALLS','Roman','I COME IN THIS WEBSITE FOR FIND BOUNCY Ball WHERE CAN i FIND?','roman@poland.com','2016-10-20',342);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hashtags`
--

DROP TABLE IF EXISTS `hashtags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hashtags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hashtag` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hashtags`
--

LOCK TABLES `hashtags` WRITE;
/*!40000 ALTER TABLE `hashtags` DISABLE KEYS */;
INSERT INTO `hashtags` VALUES (1,'Lorem'),(2,'Ipsum'),(3,'Accumsan'),(4,'ante'),(5,'bibendum'),(6,'conubia'),(7,'diam'),(8,'egestas'),(9,'facilisis'),(10,'feugiat'),(11,'gravida');
/*!40000 ALTER TABLE `hashtags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pending`
--

DROP TABLE IF EXISTS `pending`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pending` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `change_type` varchar(45) NOT NULL,
  `object_id` bigint(20) NOT NULL,
  `table_type` varchar(45) NOT NULL,
  `temp_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pending`
--

LOCK TABLES `pending` WRITE;
/*!40000 ALTER TABLE `pending` DISABLE KEYS */;
INSERT INTO `pending` VALUES (27,'delete',351,'blog',351),(28,'edit',353,'blog',348),(29,'add',354,'blog',354);
/*!40000 ALTER TABLE `pending` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `static_pages`
--

DROP TABLE IF EXISTS `static_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `static_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `page_content` longtext NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `enabled` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `static_pages`
--

LOCK TABLES `static_pages` WRITE;
/*!40000 ALTER TABLE `static_pages` DISABLE KEYS */;
INSERT INTO `static_pages` VALUES (9,'Lorem Ipsum','<p>Accumsan ante bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-10-18 20:00:00',NULL,NULL),(10,'Lorem Ipsum','<p>Accumsan ante bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-10-02 20:00:00',NULL,NULL),(11,'Lorem Ipsum','<p>Accumsan ante bibendum conubia diam egestas et facilisis feugiat gravida inceptos litora mi nam orci porta pulvinar quis rutrum sem senectus suspendisse torquent vulputate. Blandit consectetur cras elit faucibus in ligula massa morbi per porttitor sed semper ultricies. A congue curabitur dapibus dis feugiat montes nunc phasellus placerat senectus tristique. Adipiscing bibendum commodo condimentum consequat curabitur eu himenaeos litora lorem magnis mattis montes nostra odio penatibus pulvinar rhoncus suscipit tincidunt tortor vehicula.</p>','2016-10-09 20:00:00',NULL,NULL);
/*!40000 ALTER TABLE `static_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','$2a$10$b8buySpl7bQL00c0gh.P0eHJEJDUdiBJPJzZvBKsWSRjMJE9p8WjG',1),(2,'Assistant','$2a$10$KBxq.OnODYA4wbAz.eGRKO.3Rxkm7sIEB56gnhvfJ4lPQAG6sL6P2',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-20  4:30:43
