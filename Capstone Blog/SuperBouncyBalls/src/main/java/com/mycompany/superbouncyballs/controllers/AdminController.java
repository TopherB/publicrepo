/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.controllers;

import com.mycompany.superbouncyballs.dao.BlogDao;
import com.mycompany.superbouncyballs.dao.CategoryDao;
import com.mycompany.superbouncyballs.dao.HashtagDao;
import com.mycompany.superbouncyballs.dto.Blog;
import com.mycompany.superbouncyballs.dto.Category;
import com.mycompany.superbouncyballs.dto.Hashtag;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author crystal
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    private final BlogDao bdao;
    private final CategoryDao cdao;
    private final HashtagDao hdao;

    public AdminController(BlogDao b, CategoryDao c, HashtagDao h) {
        this.bdao = b;
        this.cdao = c;
        this.hdao = h;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String admin(ModelMap model) {
        List<Category> allCategories = cdao.readAll();
        List<Blog> blogs = bdao.getBlogs(1);
        List<Blog> modBlogs = new ArrayList();
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> categories = cdao.currentList();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        blogs.stream().map((b) -> {
            String content = b.getContent();
            if (content.contains("</p>")) {
                String[] firstParagraph = StringUtils.split(content, "</p>");
                String paragraph = firstParagraph[0];
                b.setContent(paragraph);
            } else if (content.contains("</div>")) {
                String[] firstParagraph = StringUtils.split(content, "</div>");
                String paragraph = firstParagraph[0];
                b.setContent(paragraph);
            } else if (content.contains("</pre>")) {
                String[] firstParagraph = StringUtils.split(content, "<br />");
                String paragraph = firstParagraph[0];
                b.setContent(paragraph);
            } else {
                b.setContent(content);
            }
            return b;
        }).forEach((b) -> {
            String blogContent = turnHashtagsIntoLinks(b.getContent(), b);
            b.setContent(blogContent);
            modBlogs.add(b);
        });

        int blogSize = bdao.blogSize();

        int totalPages = (int) (blogSize % 10 > 0 ? (int) blogSize / 10 + 1 : (int) blogSize / 10);

        int startpage = 1;
        int endpage = (int) (5 > totalPages ? totalPages : 5);

        model.addAttribute("allCategories", allCategories);
        model.addAttribute("blogs", modBlogs);
        model.addAttribute("hashtags", hashtags);
        model.addAttribute("firstLetter", firstLetter);
        model.addAttribute("startpage", startpage);
        model.addAttribute("endpage", endpage);
        model.addAttribute("flCategories", flCategories);
        model.addAttribute("categories", categories);
        model.addAttribute("otherCategories", otherCategories);
        model.addAttribute("otherFlCategories", otherFlCategories);

        return "admin";
    }

    public String turnHashtagsIntoLinks(String blogContent, Blog b) {

        Pattern regEx = Pattern.compile("#(\\w+)");

        Matcher match = regEx.matcher(blogContent);

        while (match.find()) {
            String result = match.group(1);
            Hashtag h = hdao.readString(result);
            String searchHTML = "<a href=\"/SuperBouncyBalls/hashtag/" + h.getId() + "\">#" + result + "</a>";
            blogContent = blogContent.replace("#" + result, searchHTML);
        }
        return blogContent;
    }
}
