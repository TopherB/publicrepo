/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.controllers;

import com.mycompany.superbouncyballs.dao.BlogCategoryDao;
import com.mycompany.superbouncyballs.dao.BlogDao;
import com.mycompany.superbouncyballs.dao.BlogHashtagDao;
import com.mycompany.superbouncyballs.dao.CategoryDao;
import com.mycompany.superbouncyballs.dao.CommentDao;
import com.mycompany.superbouncyballs.dao.HashtagDao;
import com.mycompany.superbouncyballs.dto.BlogCommandObject;
import com.mycompany.superbouncyballs.dao.PendingDao;
import com.mycompany.superbouncyballs.dto.Blog;
import com.mycompany.superbouncyballs.dto.BlogCategory;
import com.mycompany.superbouncyballs.dto.BlogHashtag;
import com.mycompany.superbouncyballs.dto.Category;
import com.mycompany.superbouncyballs.dto.Comment;
import com.mycompany.superbouncyballs.dto.Hashtag;
import com.mycompany.superbouncyballs.dto.Pending;
import com.mycompany.superbouncyballs.dto.ShowBlogViewModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/blog")
public class BlogController {

    private final BlogDao bdao;
    private final HashtagDao hdao;
    private final BlogHashtagDao bhdao;
    private final CategoryDao cdao;
    private final BlogCategoryDao bcdao;
    private final PendingDao pdao;
    private final CommentDao cmdao;

    public BlogController(BlogDao bd, HashtagDao hd, BlogHashtagDao bhd, CategoryDao cd, BlogCategoryDao bcd, PendingDao pd, CommentDao cmd) {
        this.bdao = bd;
        this.hdao = hd;
        this.bhdao = bhd;
        this.cdao = cd;
        this.bcdao = bcd;
        this.pdao = pd;
        this.cmdao = cmd;
    }

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String getBlogs(@RequestParam(value = "page", required = false) Integer page, ModelMap model) {

        List<Blog> blogs = bdao.getBlogs(page);
        List<Blog> modBlogs = new ArrayList();
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> categories = cdao.currentList();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        blogs.stream().map((b) -> {
            String content = b.getContent();
            if (content.contains("</p>")) {
                String[] firstParagraph = StringUtils.split(content, "</p>");
                String paragraph = firstParagraph[0];
                b.setContent(paragraph);
            } else if (content.contains("</div>")) {
                String[] firstParagraph = StringUtils.split(content, "</div>");
                String paragraph = firstParagraph[0];
                b.setContent(paragraph);
            } else if (content.contains("</pre>")) {
                String[] firstParagraph = StringUtils.split(content, "<br />");
                String paragraph = firstParagraph[0];
                b.setContent(paragraph);
            } else {
                b.setContent(content);
            }
            return b;
        }).forEach((b) -> {
            modBlogs.add(b);
        });

        int blogSize = bdao.blogSize();

        int totalPages = (int) (blogSize % 10 > 0 ? (int) blogSize / 10 + 1 : (int) blogSize / 10);

        int startpage = (int) (page > 2 ? page - 2 : 1);
        int endpage = (int) (totalPages > 5 && page < totalPages - 2 ? page + 2 : totalPages);

        model.addAttribute("blogs", modBlogs);
        model.addAttribute("flCategories", flCategories);
        model.addAttribute("hashtags", hashtags);
        model.addAttribute("firstLetter", firstLetter);
        model.addAttribute("categories", categories);
        model.addAttribute("startpage", startpage);
        model.addAttribute("endpage", endpage);
        model.addAttribute("otherCategories", otherCategories);
        model.addAttribute("otherFlCategories", otherFlCategories);

        return "list";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String showBlog(@PathVariable("id") Integer blogId, Map model, ModelMap m) {
        Blog blog = bdao.read(blogId);

        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        String blogContent = turnHashtagsIntoLinks(blog.getContent(), blog);
        blog.setContent(blogContent);
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        m.addAttribute("hashtags", hashtags);
        m.addAttribute("firstLetter", firstLetter);
        m.addAttribute("flCategories", flCategories);
        m.addAttribute("categories", categories);
        m.addAttribute("otherCategories", otherCategories);
        m.addAttribute("otherFlCategories", otherFlCategories);

        List<Comment> comments = cmdao.getComments(blogId);
        
        model.put("blog", blog);
        model.put("comments", comments);

        return "blog";
    }

    public List<ShowBlogViewModel> getBlogsByCategory(Integer categoryId) {
        return null;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Blog add(@Valid @RequestBody BlogCommandObject abc) {
        Blog b = new Blog();
        b.setTitle(abc.getTitle());
        b.setContent(abc.getContent());
        b.setStartDate(abc.getStartDate());
        b.setEndDate(abc.getEndDate());
        b.setEnabled(abc.getEnabled());

        List<Category> categories = getCategoriesFromIds(abc.getCategories());
        b.setCategories(categories);

        b = bdao.create(b);
        getHashtags(b.getContent(), b);

        bdao.update(b); 
        

        if (b.getEnabled() == 0) {
            Pending pending = new Pending();
            pending.setObjectId(b.getId());
            pending.setTableType("blog");
            pending.setChangeType("add");
            pending.setTempId(b.getId());
            pdao.create(pending);
        }

        return b;
    }

    @RequestMapping(value = "editblog/{id}", method = RequestMethod.GET)
    public String editBlogPage(HttpServletRequest request, @PathVariable("id") Integer blogId, Map model, ModelMap m) {

        String referer = request.getHeader("Referer");

        Blog blog = bdao.read(blogId);
        List<Category> allCategories = cdao.readAll();
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        m.addAttribute("hashtags", hashtags);
        m.addAttribute("firstLetter", firstLetter);
        m.addAttribute("categories", categories);
        m.addAttribute("flCategories", flCategories);
        m.addAttribute("otherCategories", otherCategories);
        m.addAttribute("otherFlCategories", otherFlCategories);
        

        for (Category c : blog.getCategories()) {
            for (Category d : allCategories) {
                if (Objects.equals(c.getId(), d.getId())) {
                    allCategories.remove(d);
                    break;
                }
            }
        }
        model.put("blog", blog);
        model.put("referer", referer);
        model.put("allCategories", allCategories);

        return "editblog";

    }

    @RequestMapping(value = "editblog/{id}", method = RequestMethod.POST)
    public String editSubmit(@Valid @ModelAttribute BlogCommandObject abc, BindingResult result, Map model, @PathVariable("id") Integer blogId,
            @RequestParam(value = "referer", required = false) String referer) {

        Blog editedBlog = new Blog();
        editedBlog.setId(abc.getId());
        editedBlog.setTitle(abc.getTitle());
        editedBlog.setContent(abc.getContent());
        editedBlog.setStartDate(abc.getStartDate());
        editedBlog.setEndDate(abc.getEndDate());
        editedBlog.setCategories(getCategoriesFromIds(abc.getCategories()));
        editedBlog.setEnabled(abc.getEnabled());
        editedBlog.setPending(abc.getPending());
        
        if (!editedBlog.getPending().getChangeType().equals("")) {
            
            if (editedBlog.getPending().getChangeType().equals("edit")) {
            
            Blog blog = new Blog();
            blog.setId(editedBlog.getPending().getObjectId());
            bdao.delete(blog);
            }
            
            Pending pending = editedBlog.getPending();
            pdao.delete(pending);
            
            editedBlog.setId(abc.getPending().getTempId());
        }

        if (result.hasErrors()) {
            List<Category> allCategories = cdao.readAll();

            model.put("blog", editedBlog);
            model.put("allCategories", allCategories);
            return "editblog";
        }

        if (abc.getEnabled() == 0) {

            Pending pending = new Pending();
            pending.setTempId(editedBlog.getId());
            editedBlog.setId(null);
            Blog pendingBlog = bdao.create(editedBlog);
            pending.setObjectId(pendingBlog.getId());
            pending.setTableType("blog");
            pending.setChangeType("edit");
            pdao.create(pending);

        } else {

            
            bhdao.delete(blogId);
            bdao.update(editedBlog);
            getHashtags(editedBlog.getContent(), editedBlog);

        }

        return "redirect:" + referer;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Blog deleteBlog(@RequestBody Blog toBeDeleted) {

        if (toBeDeleted.getEnabled() == 0) {

            Pending pending = new Pending();
            pending.setTempId(toBeDeleted.getId());
            pending.setObjectId(toBeDeleted.getId());
            pending.setTableType("blog");
            pending.setChangeType("delete");
            pdao.create(pending);
        } else {

            bhdao.delete(toBeDeleted.getId());
            cmdao.deleteByBlog(toBeDeleted.getId());

            bdao.delete(toBeDeleted);
        }

        return toBeDeleted;
    }

    public void getHashtags(String blogContent, Blog b) {

        Pattern regEx = Pattern.compile("#(\\w+)");

        Matcher match = regEx.matcher(blogContent);

        List<String> hashtagsInBlog = new ArrayList();

        while (match.find()) {
            hashtagsInBlog.add(match.group(1));
        }

        List<String> nonDbHashtags = compareHashtagsAgainstDb(hashtagsInBlog);
        List<String> nonDuplicateHashtagsInDb = checkForDuplicates(nonDbHashtags);
        createHashtag(nonDuplicateHashtagsInDb);
        List<String> nonDuplicateHashtagsInBlog = checkForDuplicates(hashtagsInBlog);
        createBlogHashtag(nonDuplicateHashtagsInBlog, b);
    }

    public String turnHashtagsIntoLinks(String blogContent, Blog b) {

        Pattern regEx = Pattern.compile("#(\\w+)");

        Matcher match = regEx.matcher(blogContent);

        while (match.find()) {
            String result = match.group(1);
            Hashtag h = hdao.readString(result);
            String searchHTML = "<a href=\"/SuperBouncyBalls/hashtag/" + h.getId() + "\">#" + result + "</a>";
            blogContent = blogContent.replace("#" + result, searchHTML);
        }
        return blogContent;
    }

    public List<String> compareHashtagsAgainstDb(List<String> hashtagsInBlog) {

        List<String> nonDbHashtags = new ArrayList();
        List<String> dbHashtagNames = getDbHashtagNames();

        hashtagsInBlog.stream().filter((s) -> (!dbHashtagNames.contains(s.toLowerCase()))).forEach((s) -> {
            nonDbHashtags.add(s);
        });
        return nonDbHashtags;
    }

    public List<String> checkForDuplicates(List<String> hashtags) {

        List<String> nonDuplicateHashtags = new ArrayList();

        hashtags.stream().filter((s) -> (!nonDuplicateHashtags.contains(s))).forEach((s) -> {
            nonDuplicateHashtags.add(s);
        });
        return nonDuplicateHashtags;
    }

    public List<String> getDbHashtagNames() {

        List<Hashtag> hashtagsInDb = hdao.list();
        List<String> dbHashtagNames = new ArrayList();

        hashtagsInDb.stream().map((h) -> h.getHashtagName()).forEach((hashName) -> {
            dbHashtagNames.add(hashName);
        });
        return dbHashtagNames;
    }

    public void createHashtag(List<String> hashtags) {

        hashtags.stream().map((s) -> {
            Hashtag h = new Hashtag();
            h.setHashtagName(s);
            return h;
        }).forEach((h) -> {
            hdao.create(h);
        });
    }

    public void createBlogHashtag(List<String> nonDuplicateHashtagsInBlog, Blog b) {

        List<Hashtag> dbHashtags = hdao.list();
        List<Hashtag> finalList = new ArrayList();

        dbHashtags.stream().forEach((h) -> {
            nonDuplicateHashtagsInBlog.stream().filter((s) -> (h.getHashtagName().equals(s.toLowerCase()))).forEach((_item) -> {
                finalList.add(h);
            });
        });

        finalList.stream().map((ht) -> {
            BlogHashtag bh = new BlogHashtag();
            bh.setBlog(b);
            bh.setHashtag(ht);
            return bh;
        }).forEach((bh) -> {
            bhdao.create(bh);
        });

    }

    public void createBlogCategory(List<Integer> categoryIds, Blog b) {
        categoryIds.stream().forEach((i) -> {
            BlogCategory bc = new BlogCategory();
            bc.setBlog(b);
            bc.setCategory(cdao.read(i));
            bcdao.create(bc);
        });

    }

    private List<Category> getCategoriesFromIds(List<Integer> categoryIds) {
        List<Category> categories = new ArrayList();
        if (categoryIds != null && !categoryIds.isEmpty()) {
            for (Integer i : categoryIds) {
                categories.add(cdao.read(i));
            }
        }
        return categories;
    }
}
