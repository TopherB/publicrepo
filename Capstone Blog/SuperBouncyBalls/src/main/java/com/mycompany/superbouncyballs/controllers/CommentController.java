/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.controllers;

import com.mycompany.superbouncyballs.dao.CommentDao;
import com.mycompany.superbouncyballs.dto.Comment;
import java.util.Date;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/comment")
public class CommentController {

    private CommentDao cmdao;

    public CommentController(CommentDao cmd) {
        this.cmdao = cmd;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Comment addComment(@Valid @RequestBody Comment c) {
        
        Date date = new Date();
      
        c.setDate(date);
        c = cmdao.create(c);
        return c;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Comment deleteComment(@RequestBody Comment c) {
        cmdao.delete(c);
        return c;
    }
}
