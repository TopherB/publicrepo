/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.controllers;

import com.mycompany.superbouncyballs.dao.BlogDao;
import com.mycompany.superbouncyballs.dao.CategoryDao;
import com.mycompany.superbouncyballs.dao.HashtagDao;
import com.mycompany.superbouncyballs.dto.Blog;
import com.mycompany.superbouncyballs.dto.Category;
import com.mycompany.superbouncyballs.dto.Hashtag;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/hashtag")
public class HashtagController {

    private final BlogDao bdao;
    private final HashtagDao hdao;
    private final CategoryDao cdao;

    public HashtagController(BlogDao b, HashtagDao h, CategoryDao c) {
        this.bdao = b;
        this.hdao = h;
        this.cdao = c;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getBlogsContainingHashtag(@PathVariable("id") Integer hashId, ModelMap model) {

        List<Blog> modBlogs = new ArrayList();
        List<Blog> getBlogsList = bdao.getByHashtag(hashId, 1);
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        getBlogsList.stream().map((b) -> {
            String content = b.getContent();
            if (content.contains("</p>")) {
                String[] firstParagraph = StringUtils.split(content, "</p>");
                String paragraph = firstParagraph[0];
                b.setContent(paragraph);
            } else if (content.contains("</div>")) {
                String[] firstParagraph = StringUtils.split(content, "</div>");
                String paragraph = firstParagraph[0];
                b.setContent(paragraph);
            } else if (content.contains("</pre>")) {
                String[] firstParagraph = StringUtils.split(content, "<br />");
                String paragraph = firstParagraph[0];
                b.setContent(paragraph);
            } else {
                b.setContent(content);
            }
            return b;
        }).forEach((b) -> {
            String blogContent = turnHashtagsIntoLinks(b.getContent(), b);
            b.setContent(blogContent);
            modBlogs.add(b);
        });

        int blogSize = bdao.blogHashtagSize(hashId);

        int totalPages = (int) (blogSize % 10 > 0 ? (int) blogSize / 10 + 1 : (int) blogSize / 10);

        int startpage = 1;
        int endpage = (int) (5 > totalPages ? totalPages : 5);

        model.addAttribute("blogs", modBlogs);
        model.addAttribute("categories", categories);
        model.addAttribute("startpage", startpage);
        model.addAttribute("endpage", endpage);
        model.addAttribute("hashtags", hashtags);
        model.addAttribute("firstLetter", firstLetter);
        model.addAttribute("flCategories", flCategories);
        model.addAttribute("otherCategories", otherCategories);
        model.addAttribute("otherFlCategories", otherFlCategories);
        model.put("hashId", hashId);

        return "hashtaglist";
    }

    @RequestMapping(value = "{id}/list", method = RequestMethod.GET)
    public String getBlogsWithHash(@PathVariable("id") Integer hashId, @RequestParam(value = "page", required = false) Integer page, ModelMap model) {

        List<Blog> modBlogs = new ArrayList();
        List<Blog> blogs = bdao.getByHashtag(hashId, page);
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        Hashtag h = hdao.read(hashId);
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        blogs.stream().map((b) -> {
            String content = b.getContent();
            if (content.contains("</p>")) {
                String[] firstParagraph = StringUtils.split(content, "</p>");
                String paragraph = firstParagraph[0];
                b.setContent(paragraph);
            } else if (content.contains("</div>")) {
                String[] firstParagraph = StringUtils.split(content, "</div>");
                String paragraph = firstParagraph[0];
                b.setContent(paragraph);
            } else if (content.contains("</pre>")) {
                String[] firstParagraph = StringUtils.split(content, "<br />");
                String paragraph = firstParagraph[0];
                b.setContent(paragraph);
            } else {
                b.setContent(content);
            }
            return b;
        }).forEach((b) -> {
            String blogContent = turnHashtagsIntoLinks(b.getContent(), b);
            b.setContent(blogContent);
            modBlogs.add(b);
        });

        int blogSize = bdao.blogHashtagSize(hashId);

        int totalPages = (int) (blogSize % 10 > 0 ? (int) blogSize / 10 + 1 : (int) blogSize / 10);

        int startpage = (int) (page > 2 ? page - 2 : 1);
        int endpage = (int) (totalPages > 5 && page < totalPages - 2 ? page + 2 : totalPages);

        model.addAttribute("blogs", modBlogs);

        model.addAttribute("hashtags", hashtags);
        model.addAttribute("firstLetter", firstLetter);
        model.put("hashId", h);
        model.addAttribute("startpage", startpage);
        model.addAttribute("endpage", endpage);
        model.addAttribute("flCategories", flCategories);
        model.addAttribute("categories", categories);
        model.addAttribute("otherCategories", otherCategories);
        model.addAttribute("otherFlCategories", otherFlCategories);

        return "hashtaglist";
    }

    public String turnHashtagsIntoLinks(String blogContent, Blog b) {

        Pattern regEx = Pattern.compile("#(\\w+)");

        Matcher match = regEx.matcher(blogContent);

        while (match.find()) {
            String result = match.group(1);
            Hashtag h = hdao.readString(result);
            String searchHTML = "<a href=\"/SuperBouncyBalls/hashtag/" + h.getId() + "\">#" + result + "</a>";
            blogContent = blogContent.replace("#" + result, searchHTML);
        }
        return blogContent;
    }
}
