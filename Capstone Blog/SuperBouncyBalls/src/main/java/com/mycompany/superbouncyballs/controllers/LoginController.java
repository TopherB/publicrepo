/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.controllers;

import com.mycompany.superbouncyballs.dao.CategoryDao;
import com.mycompany.superbouncyballs.dao.HashtagDao;
import com.mycompany.superbouncyballs.dto.Category;
import com.mycompany.superbouncyballs.dto.Hashtag;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class LoginController {

    private final HashtagDao hdao;
    private final CategoryDao cdao;

    public LoginController(HashtagDao h, CategoryDao c) {
        this.hdao = h;
        this.cdao = c;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(ModelMap model) {
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        model.addAttribute("hashtags", hashtags);
        model.addAttribute("firstLetter", firstLetter);
        model.addAttribute("flCategories", flCategories);
        model.addAttribute("categories", categories);
        model.addAttribute("otherCategories", otherCategories);
        model.addAttribute("otherFlCategories", otherFlCategories);
        return "login";
    }
}
