/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.controllers;

import com.mycompany.superbouncyballs.dao.BlogDao;
import com.mycompany.superbouncyballs.dao.CategoryDao;
import com.mycompany.superbouncyballs.dao.HashtagDao;
import com.mycompany.superbouncyballs.dao.PendingDao;
import com.mycompany.superbouncyballs.dto.Blog;
import com.mycompany.superbouncyballs.dto.Category;
import com.mycompany.superbouncyballs.dto.Hashtag;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/pending")
public class PendingController {

    private final PendingDao pdao;
    private final BlogDao bdao;
    private final HashtagDao hdao;
    private final CategoryDao cdao;

    public PendingController(PendingDao pd, BlogDao bd, HashtagDao h, CategoryDao c) {
        this.pdao = pd;
        this.bdao = bd;
        this.hdao = h;
        this.cdao = c;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String home(Map model, ModelMap m) {

        List<Blog> pendingList = bdao.getPendingBlogs();
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> categories = cdao.currentList();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        model.put("pendingList", pendingList);
        m.addAttribute("hashtags", hashtags);
        m.addAttribute("firstLetter", firstLetter);
        m.addAttribute("flCategories", flCategories);
        m.addAttribute("categories", categories);
        m.addAttribute("otherCategories", otherCategories);
        m.addAttribute("otherFlCategories", otherFlCategories);

        return "pending";
    }

}
