/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.controllers;

import com.mycompany.superbouncyballs.dao.BlogDao;
import com.mycompany.superbouncyballs.dao.CategoryDao;
import com.mycompany.superbouncyballs.dao.HashtagDao;
import com.mycompany.superbouncyballs.dao.StaticPageCategoryDao;
import com.mycompany.superbouncyballs.dao.StaticPageDao;
import com.mycompany.superbouncyballs.dto.Category;
import com.mycompany.superbouncyballs.dto.Hashtag;
import com.mycompany.superbouncyballs.dto.StaticCommandObject;
import com.mycompany.superbouncyballs.dto.StaticPage;
import com.mycompany.superbouncyballs.dto.StaticPageCategory;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/static")
public class StaticPageController {

    private final StaticPageDao sdao;
    private final HashtagDao hdao;
    private final CategoryDao cdao;
    private final StaticPageCategoryDao scdao;
    private final BlogDao bdao;

    public StaticPageController(StaticPageDao sd, HashtagDao h, CategoryDao cd, StaticPageCategoryDao scd, BlogDao b) {
        this.sdao = sd;
        this.hdao = h;
        this.cdao = cd;
        this.scdao = scd;
        this.bdao = b;
    }

    @RequestMapping(value = "list", method = RequestMethod.GET)
    @ResponseBody
    public List<StaticPage> listStaticPages(ModelMap model) {
        List<StaticPage> staticPages = sdao.getStaticPages();
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        model.addAttribute("hashtags", hashtags);
        model.addAttribute("firstLetter", firstLetter);
        model.addAttribute("flCategories", flCategories);
        model.addAttribute("categories", categories);
        model.addAttribute("otherCategories", otherCategories);
        model.addAttribute("otherFlCategories", otherFlCategories);

        return staticPages;
    }

    @RequestMapping(value = "addstatic", method = RequestMethod.GET)
    public String addStaticPage(ModelMap model) {
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<Category> allCategories = cdao.readAll();
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        model.addAttribute("hashtags", hashtags);
        model.addAttribute("firstLetter", firstLetter);
        model.addAttribute("allCategories", allCategories);
        model.addAttribute("categories", categories);
        model.addAttribute("flCategories", flCategories);
        model.addAttribute("otherCategories", otherCategories);
        model.addAttribute("otherFlCategories", otherFlCategories);
        return "addstatic";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public StaticPage add(@Valid @RequestBody StaticCommandObject sco) {
        StaticPage s = new StaticPage();

        s.setTitle(sco.getTitle());
        s.setPageContent(sco.getPageContent());
        s.setStartDate(sco.getStartDate());
        s.setEndDate(sco.getEndDate());
        s.setCategories(getCategoriesFromIds(sco.getCategories()));

        s = sdao.create(s);

        for (Category c : s.getCategories()) {
            StaticPageCategory sc = new StaticPageCategory();
            sc.setCategory(c);
            sc.setStaticPage(s);
            scdao.create(sc);
        }

        return s;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String showStaticPage(@PathVariable("id") Integer staticPageId, ModelMap model) {
        StaticPage staticPage = sdao.read(staticPageId);
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        model.addAttribute("hashtags", hashtags);
        model.addAttribute("firstLetter", firstLetter);
        model.addAttribute("staticPage", staticPage);
        model.addAttribute("categories", categories);
        model.addAttribute("flCategories", flCategories);
        model.addAttribute("otherCategories", otherCategories);
        model.addAttribute("otherFlCategories", otherFlCategories);

        return "static";
    }

    @RequestMapping(value = "editstatic/{id}", method = RequestMethod.GET)
    public String editStaticPage(HttpServletRequest request,
            @PathVariable("id") Integer staticPageId, Map model, ModelMap m) {

        String referer = request.getHeader("Referer");

        StaticPage staticPage = sdao.read(staticPageId);
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<Category> allCategories = cdao.readAll();
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        m.addAttribute("hashtags", hashtags);
        m.addAttribute("firstLetter", firstLetter);
        m.addAttribute("categories", categories);
        m.addAttribute("flCategories", flCategories);
        m.addAttribute("otherCategories", otherCategories);
        m.addAttribute("otherFlCategories", otherFlCategories);

        for (Category c : staticPage.getCategories()) {
            for (Category d : allCategories) {
                if (Objects.equals(c.getId(), d.getId())) {
                    allCategories.remove(d);
                    break;
                }
            }
        }

        model.put("staticPage", staticPage);
        model.put("referer", referer);
        model.put("allCategories", allCategories);

        return "editstatic";

    }

    @RequestMapping(value = "editstatic/{id}", method = RequestMethod.POST)
    public String editSubmit(@Valid @ModelAttribute StaticCommandObject editedSCO, BindingResult result, Map model, @PathVariable("id") Integer staticPageId,
            @RequestParam(value = "referer", required = false) String referer) {

        StaticPage editedStaticPage = new StaticPage();
        editedStaticPage.setId(editedSCO.getId());
        editedStaticPage.setTitle(editedSCO.getTitle());
        editedStaticPage.setPageContent(editedSCO.getPageContent());
        editedStaticPage.setStartDate(editedSCO.getStartDate());
        editedStaticPage.setEndDate(editedSCO.getEndDate());
        editedStaticPage.setCategories(getCategoriesFromIds(editedSCO.getCategories()));

        if (result.hasErrors()) {
            List<Category> allCategories = cdao.readAll();
            model.put("allCategories", allCategories);
            model.put("staticPage", editedStaticPage);

            return "editstatic";
        }

        sdao.update(editedStaticPage);

        return "redirect:" + referer;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public StaticPage deleteStaticPage(@RequestBody StaticPage toBeDeleted) {

        scdao.deleteByStaticPageId(toBeDeleted.getId());
        sdao.delete(toBeDeleted);

        return toBeDeleted;
    }

    private List<Category> getCategoriesFromIds(List<Integer> categoryIds) {
        List<Category> categories = new ArrayList();
        if (categoryIds != null && !categoryIds.isEmpty()) {
            for (Integer i : categoryIds) {
                categories.add(cdao.read(i));
            }
        }
        return categories;
    }

    @RequestMapping(value = "category/{id}", method = RequestMethod.GET)
    public String getStaticContainingCategory(@PathVariable("id") Integer categoryId, ModelMap model) {

        List<StaticPage> modStatic = new ArrayList();
        List<StaticPage> getStaticList = sdao.getByCategory(categoryId, 1);
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        getStaticList.stream().map((s) -> {
            String content = s.getPageContent();
            if (content.contains("</p>")) {
                String[] firstParagraph = StringUtils.split(content, "</p>");
                String paragraph = firstParagraph[0];
                s.setPageContent(paragraph);
            } else if (content.contains("</div>")) {
                String[] firstParagraph = StringUtils.split(content, "</div>");
                String paragraph = firstParagraph[0];
                s.setPageContent(paragraph);
            } else if (content.contains("</pre>")) {
                String[] firstParagraph = StringUtils.split(content, "<br />");
                String paragraph = firstParagraph[0];
                s.setPageContent(paragraph);
            } else {
                s.setPageContent(content);
            }
            return s;
        }).forEach((s) -> {
            modStatic.add(s);
        });

        int staticSize = bdao.staticCategorySize(categoryId);

        int totalPages = (int) (staticSize % 10 > 0 ? (int) staticSize / 10 + 1 : (int) staticSize / 10);

        int startpage = 1;
        int endpage = (int) (5 > totalPages ? totalPages : 5);

        model.addAttribute("modStatic", modStatic);
        model.addAttribute("categories", categories);
        model.addAttribute("startpage", startpage);
        model.addAttribute("endpage", endpage);
        model.addAttribute("hashtags", hashtags);
        model.addAttribute("firstLetter", firstLetter);
        model.addAttribute("flCategories", flCategories);
        model.put("categoryId", categoryId);
        model.put("otherCategories", otherCategories);
        model.put("otherFlCategories", otherFlCategories);

        return "staticlist";
    }

    @RequestMapping(value = "{id}/list", method = RequestMethod.GET)
    public String getBlogsWithHash(@PathVariable("id") Integer categoryId, @RequestParam(value = "page", required = false) Integer page, ModelMap model) {

        List<StaticPage> modStatic = new ArrayList();
        List<StaticPage> getStaticList = sdao.getByCategory(categoryId, page);
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();
        Category c = cdao.read(categoryId);

        getStaticList.stream().map((s) -> {
            String content = s.getPageContent();
            if (content.contains("</p>")) {
                String[] firstParagraph = StringUtils.split(content, "</p>");
                String paragraph = firstParagraph[0];
                s.setPageContent(paragraph);
            } else if (content.contains("</div>")) {
                String[] firstParagraph = StringUtils.split(content, "</div>");
                String paragraph = firstParagraph[0];
                s.setPageContent(paragraph);
            } else if (content.contains("</pre>")) {
                String[] firstParagraph = StringUtils.split(content, "<br />");
                String paragraph = firstParagraph[0];
                s.setPageContent(paragraph);
            } else {
                s.setPageContent(content);
            }
            return s;
        }).forEach((s) -> {
            modStatic.add(s);
        });

        int staticSize = bdao.staticCategorySize(categoryId);

        int totalPages = (int) (staticSize % 10 > 0 ? (int) staticSize / 10 + 1 : (int) staticSize / 10);

        int startpage = (int) (page > 2 ? page - 2 : 1);
        int endpage = (int) (totalPages > 5 && page < totalPages - 2 ? page + 2 : totalPages);

        model.addAttribute("static", modStatic);

        model.addAttribute("hashtags", hashtags);
        model.addAttribute("firstLetter", firstLetter);
        model.put("categoryId", c);
        model.addAttribute("startpage", startpage);
        model.addAttribute("endpage", endpage);
        model.addAttribute("flCategories", flCategories);
        model.addAttribute("categories", categories);
        model.addAttribute("otherCategories", otherCategories);
        model.addAttribute("otherFlCategories", otherFlCategories);

        return "staticlist";
    }
}
