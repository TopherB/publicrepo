/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.controllers;



import com.mycompany.superbouncyballs.dao.AccessLevelDao;
import com.mycompany.superbouncyballs.dao.CategoryDao;
import com.mycompany.superbouncyballs.dao.HashtagDao;
import com.mycompany.superbouncyballs.dao.UserDao;
import com.mycompany.superbouncyballs.dto.AccessLevel;
import com.mycompany.superbouncyballs.dto.Category;
import com.mycompany.superbouncyballs.dto.Hashtag;
import com.mycompany.superbouncyballs.dto.User;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    private UserDao udao;
    private AccessLevelDao adao;
    private HashtagDao hdao;
    private PasswordEncoder encoder;
    private CategoryDao cdao;

    public UserController(UserDao ud, AccessLevelDao ad, HashtagDao h, PasswordEncoder pe, CategoryDao c) {
        this.udao = ud;
        this.adao = ad;
        this.hdao = h;
        this.encoder = pe;
        this.cdao = c;
    }

    @RequestMapping(value = "users", method = RequestMethod.GET)
    public String listUsers(ModelMap model) {
        List<User> users = udao.getUsers();
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        model.addAttribute("hashtags", hashtags);
        model.addAttribute("firstLetter", firstLetter);
        model.addAttribute("flCategories", flCategories);
        model.addAttribute("categories", categories);
        model.addAttribute("otherCategories", otherCategories);
        model.addAttribute("otherFlCategories", otherFlCategories);

        for (User u : users) {
            List<AccessLevel> al = adao.read(u.getUserName());
            String userAuthority = (String) (al.size() == 1 ? "Assistant" : "Administrator");
            u.setAuthority(userAuthority);
        }

        model.addAttribute("users", users);

        return "users";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public User addUser(@Valid @RequestBody User u) {
        u.setEnabled(1);
        String pw = u.getPassword();
        String hashPw = encoder.encode(pw);
        u.setPassword(hashPw); 
        u = udao.create(u);

        AccessLevel al = new AccessLevel();
        al.setAuthority(u.getAuthority());
        al.setUsername(u.getUserName());
        adao.create(al);

        return u;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public User deleteUser(@RequestBody User u) {

        adao.delete(udao.read(u.getId()).getUserName());

        udao.delete(u);

        return u;
    }

    @RequestMapping(value = "edituser/{id}", method = RequestMethod.GET)
    public String editUser(HttpServletRequest request, @PathVariable("id") Integer userId, Map model, ModelMap m) {

        String referer = request.getHeader("Referer");

        User u = udao.read(userId);

        List<AccessLevel> al = adao.read(u.getUserName());

        String userAuthority = (String) (al.size() == 1 ? "Assistant" : "Administrator");
        List<String> firstLetter = hdao.firstLetterOfHashtag();
        List<Hashtag> hashtags = hdao.currentList();
        List<Category> categories = cdao.currentList();
        List<String> flCategories = cdao.firstLetterOfCategory();
        List<Category> otherCategories = cdao.otherCurrentList();
        List<String> otherFlCategories = cdao.otherFirstLetterOfCategory();

        m.addAttribute("hashtags", hashtags);
        m.addAttribute("firstLetter", firstLetter);
        m.addAttribute("categories", categories);
        m.addAttribute("flCategoies", flCategories);
        m.addAttribute("otherCategories", otherCategories);
        m.addAttribute("otherFlCategories", otherFlCategories);
        model.put("user", u);
        model.put("authority", userAuthority);
        model.put("referer", referer);

        return "edituser";
    }

    @RequestMapping(value = "edituser/{id}", method = RequestMethod.POST)
    public String editSubmit(@Valid @ModelAttribute User u, BindingResult result, Map model, @PathVariable("id") Integer userId,
            @RequestParam(value = "referer", required = false) String referer) {

        u.setEnabled(1);
        if (result.hasErrors()) {

            model.put("user", u);
            return "edituser";
        }

        AccessLevel al = new AccessLevel();
        al.setAuthority(u.getAuthority());
        al.setUsername(u.getUserName());

        adao.delete(udao.read(userId).getUserName());
        udao.update(u);
        adao.create(al);

        return "redirect:" + referer;
    }
}
