/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.AccessLevel;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface AccessLevelDao {
    
    AccessLevel create(AccessLevel accessLevel);
    
    List<AccessLevel> read(String username);
    
    void update(AccessLevel accessLevel);
    
    void delete(String username);
    
}
