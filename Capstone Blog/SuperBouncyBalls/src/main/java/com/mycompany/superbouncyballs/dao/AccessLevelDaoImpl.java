/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.AccessLevel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class AccessLevelDaoImpl implements AccessLevelDao {

    private final String SQL_INSERT_ACCESS_LEVEL = "INSERT INTO authorities (username, authority) VALUES (?, ?);";
    private final String SQL_UPDATE_ACCESS_LEVEL = "UPDATE authorities SET authority=? WHERE username=?;";
    private final String SQL_SELECT_ACCESS_LEVEL = "SELECT * FROM authorities WHERE username=?;";
    private final String SQL_SELECT_ALL = "SELECT * FROM authorities;";
    private final String SQL_DELETE_ACCESS_LEVEL = "DELETE FROM authorities WHERE username=?;";

    private final JdbcTemplate jdbcTemplate;

    public AccessLevelDaoImpl(JdbcTemplate j) {
        this.jdbcTemplate = j;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public AccessLevel create(AccessLevel accessLevel) {

        if (accessLevel.getAuthority().equals("Administrator")) {

            accessLevel.setAuthority("ROLE_ADMIN");
            jdbcTemplate.update(SQL_INSERT_ACCESS_LEVEL,
                    accessLevel.getUsername(),
                    accessLevel.getAuthority());

            accessLevel.setAuthority("ROLE_USER");
            jdbcTemplate.update(SQL_INSERT_ACCESS_LEVEL,
                    accessLevel.getUsername(),
                    accessLevel.getAuthority());
        } else {

            accessLevel.setAuthority("ROLE_USER");
            jdbcTemplate.update(SQL_INSERT_ACCESS_LEVEL,
                    accessLevel.getUsername(),
                    accessLevel.getAuthority());
        }

        return accessLevel;
    }

    @Override
    public List<AccessLevel> read(String username) {

        List<AccessLevel> accessLevels = jdbcTemplate.query(SQL_SELECT_ACCESS_LEVEL, new AccessLevelMapper(), username);
        return accessLevels;
    }

    @Override
    public void update(AccessLevel accessLevel) {

        if (accessLevel.getAuthority().equals("Administrator")) {

            accessLevel.setAuthority("ROLE_ADMIN");
            jdbcTemplate.update(SQL_UPDATE_ACCESS_LEVEL,
                    accessLevel.getUsername(),
                    accessLevel.getAuthority());

            accessLevel.setAuthority("ROLE_USER");
            jdbcTemplate.update(SQL_UPDATE_ACCESS_LEVEL,
                    accessLevel.getUsername(),
                    accessLevel.getAuthority());
        } else {
            accessLevel.setAuthority("ROLE_USER");
            jdbcTemplate.update(SQL_UPDATE_ACCESS_LEVEL,
                    accessLevel.getUsername(),
                    accessLevel.getAuthority());
        }
    }

    @Override
    public void delete(String username) {

        jdbcTemplate.update(SQL_DELETE_ACCESS_LEVEL, username);
    }

    private static final class AccessLevelMapper implements RowMapper<AccessLevel> {

        @Override
        public AccessLevel mapRow(ResultSet rs, int i) throws SQLException {

            AccessLevel ac = new AccessLevel();
            ac.setUsername(rs.getString("username"));
            ac.setAuthority(rs.getString("authority"));

            return ac;
        }
    }
}
