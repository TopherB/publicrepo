/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.Blog;
import com.mycompany.superbouncyballs.dto.BlogCategory;
import com.mycompany.superbouncyballs.dto.Category;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface BlogCategoryDao {

    BlogCategory create(BlogCategory blogCategory);

    BlogCategory read(Integer blogCategoryId);
    
    List<BlogCategory> getBlogCategoriesById(Integer blogId);

    void update(BlogCategory blogCategory);

    void delete(BlogCategory blogCategory);
    
    void deleteByBlogId(Integer blogId);
    
    void deleteByCategoryId(Integer categoryId);

}
