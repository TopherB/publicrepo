/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.BlogCategory;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class BlogCategoryDaoImpl implements BlogCategoryDao {

    private final String SQL_INSERT_BLOG_CATEGORY = "INSERT INTO categories_blog (category_id, blog_id) VALUES (?, ?);";
    private final String SQL_UPDATE_BLOG_CATEGORY = "UPDATE categories_blog SET category_id=?, blog_id=? WHERE id=?;";
    private final String SQL_SELECT_BLOG_CATEGORY = "SELECT * FROM categories_blog WHERE id=?;";
    private final String SQL_SELECT_BLOG_CATEGORIES_BY_BLOG = "SELECT * FROM categories_blog WHERE blog_id=?;";
    private final String SQL_DELETE_BLOG_CATEGORY = "DELETE FROM categories_blog WHERE id = ?;";
    private final String SQL_DELETE_BLOG_CATEGORIES_BY_BLOG = "DELETE FROM categories_blog WHERE blog_id = ?;";
    private final String SQL_DELETE_BLOG_CATEGORIES_BY_CATEGORY = "DELETE FROM categories_blog WHERE category_id = ?;";

    private final JdbcTemplate jdbcTemplate;
    private static CategoryDao cdao;

    public BlogCategoryDaoImpl(JdbcTemplate j, CategoryDaoImpl cd) {
        this.jdbcTemplate = j;
//        this.bdao = bd;
        this.cdao = cd;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public BlogCategory create(BlogCategory blogCategory) {

        jdbcTemplate.update(SQL_INSERT_BLOG_CATEGORY,
                blogCategory.getCategory().getId(),
                blogCategory.getBlog().getId());

        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        blogCategory.setId(newId);

        return blogCategory;
    }

    @Override
    public BlogCategory read(Integer blogCategoryId) {

//        BlogCategory bc = jdbcTemplate.queryForObject(SQL_SELECT_BLOG_CATEGORY, new BlogCategoryMapper(), blogCategoryId);
//        return bc;
        return null;
    }

    @Override
    public List<BlogCategory> getBlogCategoriesById(Integer blogId) {
//        List<BlogCategory> lbc = jdbcTemplate.query(SQL_SELECT_BLOG_CATEGORIES_BY_BLOG, new BlogCategoryMapper(), blogId);
//        return lbc;
        return null;
    }

    @Override
    public void update(BlogCategory blogCategory) {

        jdbcTemplate.update(SQL_UPDATE_BLOG_CATEGORY,
                blogCategory.getCategory().getId(),
                blogCategory.getBlog().getId(),
                blogCategory.getId());
    }

    @Override
    public void delete(BlogCategory blogCategory) {

        jdbcTemplate.update(SQL_DELETE_BLOG_CATEGORY, blogCategory.getId());
    }

    @Override
    public void deleteByBlogId(Integer blogId) {
        jdbcTemplate.update(SQL_DELETE_BLOG_CATEGORIES_BY_BLOG, blogId);
    }
    
    @Override
    public void deleteByCategoryId(Integer categoryId){
        jdbcTemplate.update(SQL_DELETE_BLOG_CATEGORIES_BY_CATEGORY, categoryId);
    }




//    private static final class BlogCategoryMapper implements RowMapper<BlogCategory> {
//
//        @Override
//        public BlogCategory mapRow(ResultSet rs, int i) throws SQLException {
//            BlogCategory bc = new BlogCategory();
//            bc.setId(rs.getInt("id"));
//            int blogId = rs.getInt("blog_id");
//            Blog blog = bdao.read(blogId);
//            bc.setBlog(blog);
//            int categoryId = rs.getInt("category_id");
//            Category category = cdao.read(categoryId);
//            bc.setCategory(category);
//
//            return bc;
//        }
//    }
}
