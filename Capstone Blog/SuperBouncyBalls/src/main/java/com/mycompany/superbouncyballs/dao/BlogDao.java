    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.Blog;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface BlogDao {

    Blog create(Blog blog);

    Blog read(Integer blogId);

    Blog readPending(Integer blogId);
    
    void update(Blog blog);

    void delete(Blog blog);
    
    Integer blogSize();
    
    Integer blogHashtagSize(Integer hashId);
    
    Integer blogCategorySize(Integer categoryId);

    List<Blog> getByCategory(Integer categoryId, Integer offset);
    
    List<Blog> getBlogs(Integer offset);
    
    List<Blog> getPendingBlogs();
    
    List<Blog> getByHashtag(Integer hashtagId, Integer offset);

    Blog blogsByRecentDate();
    
    Integer staticCategorySize(Integer categoryId);
}
