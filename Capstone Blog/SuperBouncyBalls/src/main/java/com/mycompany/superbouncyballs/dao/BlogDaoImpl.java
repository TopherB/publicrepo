/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.Blog;
import com.mycompany.superbouncyballs.dto.BlogCategory;
import com.mycompany.superbouncyballs.dto.Category;
import com.mycompany.superbouncyballs.dto.Pending;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class BlogDaoImpl implements BlogDao {

    private final String SQL_INSERT_BLOG = "INSERT INTO blog (title, blog_content, start_date, end_date, enabled) VALUES (?, ?, ?, ?, ?)";
    private final String SQL_UPDATE_BLOG = "UPDATE blog SET title=?, blog_content=?, start_date=?, end_date=?, enabled=? WHERE id=?;";
    private final String SQL_SELECT_BLOG = "SELECT * FROM blog LEFT JOIN pending ON blog.id = pending.object_id WHERE blog.id= ?;";
    private final String SQL_SELECT_PENDING_BLOG = "SELECT * FROM blog WHERE id=? AND enabled=0;";
    private final String SQL_SELECT_PENDING_BLOGS = "SELECT * FROM blog INNER JOIN pending ON blog.id = pending.object_id WHERE enabled=0 OR change_type='delete';";
    private final String SQL_DELETE_BLOG = "DELETE FROM blog WHERE id = ?;";
    private final String SQL_SELECT_BLOG_BY_PAGE_BY_CATEGORY = "SELECT blog.id, blog.title, blog.blog_content, blog.start_date, blog.end_date "
            + "FROM blog blog INNER JOIN categories_blog cb ON blog.id=cb.blog_id WHERE category_id = ? AND blog.start_date <= CURRENT_DATE() "
            + "AND (blog.end_date > CURRENT_DATE() OR blog.end_date IS NULL) AND enabled = 1 ORDER BY blog.start_date DESC "
            + "LIMIT 10 OFFSET ?;";
    private final String SQL_SELECT_BLOG_BY_RECENT_DATE = "SELECT blog.id, blog.title, blog.blog_content, blog.start_date, blog.end_date"
            + " FROM blog blog WHERE blog.start_date <= CURRENT_DATE() "
            + "AND (blog.end_date > CURRENT_DATE OR blog.end_date IS NULL) AND enabled = 1 ORDER BY blog.start_date DESC LIMIT 1;";
    private final String SQL_SELECT_BLOG_BY_PAGE = "SELECT blog.id, blog.title, blog.blog_content, blog.start_date, blog.end_date "
            + "FROM blog blog WHERE blog.start_date <= CURRENT_DATE() "
            + "AND (blog.end_date > CURRENT_DATE OR blog.end_date IS NULL) AND enabled = 1 ORDER BY blog.start_date DESC LIMIT 10 OFFSET ?;";
    private final String SQL_SELECT_BLOG_BY_PAGE_BY_HASHTAG = "SELECT blog.id, blog.title, blog.blog_content, blog.start_date, blog.end_date "
            + "FROM blog blog INNER JOIN blog_hashtag bh ON blog.id = bh.blog_id WHERE hashtag_id = ? AND blog.start_date <= CURRENT_DATE() "
            + "AND (blog.end_date >= CURRENT_DATE() OR blog.end_date IS NULL) AND enabled = 1 ORDER BY blog.start_date DESC "
            + "LIMIT 10 OFFSET ?;";
    private final String SQL_COUNT_ENTRIES = "SELECT COUNT(*) FROM blog WHERE start_date <= CURRENT_DATE() "
            + "AND (end_date > CURRENT_DATE OR end_date IS NULL) AND enabled = 1;";
    private final String SQL_COUNT_HASHTAG_SPECIFIC_ENTRIES = "SELECT COUNT(*) FROM blog b INNER JOIN blog_hashtag bh "
            + "ON b.id = bh.blog_id WHERE hashtag_id = ? AND start_date <= CURRENT_DATE() "
            + "AND (end_date > CURRENT_DATE() OR end_date IS NULL) AND enabled = 1;";
    private final String SQL_COUNT_CATEGORY_SPECIFIC_ENTRIES = "SELECT COUNT(*) FROM blog b INNER JOIN categories_blog cb "
            + "ON b.id=cb.blog_id WHERE category_id = ? AND start_date <= CURRENT_DATE() "
            + "AND (end_date > CURRENT_DATE() OR end_date IS NULL) AND enabled = 1;";
    private final String SQL_COUNT_CATEGORY_FOR_STATIC_ENTRIES = "SELECT COUNT(*) FROM static_pages sp "
            + "INNER JOIN categories_staticpages csp ON sp.id=csp.static_page_id WHERE csp.category_id = ? "
            + "AND start_date <= CURRENT_DATE() AND (end_date >= CURRENT_DATE() OR end_date IS NULL);";

    private final JdbcTemplate jdbcTemplate;
    private final CategoryDao cdao;
    private final BlogCategoryDao bcdao;

    public BlogDaoImpl(JdbcTemplate j, CategoryDao cd, BlogCategoryDao bcd) {
        this.jdbcTemplate = j;
        this.cdao = cd;
        this.bcdao = bcd;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Blog create(Blog blog) {

        jdbcTemplate.update(SQL_INSERT_BLOG,
                blog.getTitle(),
                blog.getContent(),
                blog.getStartDate(),
                blog.getEndDate(),
                blog.getEnabled());

        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        blog.setId(newId);

        blog.getCategories().stream().map((c) -> {
            BlogCategory bc = new BlogCategory();
            bc.setBlog(blog);
            bc.setCategory(c);
            return bc;
        }).forEach((bc) -> {
            bcdao.create(bc);
        });
        return blog;
    }

    @Override
    public Blog read(Integer blogId) {

        Blog blog = jdbcTemplate.queryForObject(SQL_SELECT_BLOG, new PendingBlogMapper(), blogId);
        blog.setCategories(cdao.getCategoriesByBlogId(blogId));

        return blog;
    }

    @Override
    public Blog readPending(Integer blogId) {

        Blog blog = jdbcTemplate.queryForObject(SQL_SELECT_PENDING_BLOG, new BlogMapper(), blogId);
        blog.setCategories(cdao.getCategoriesByBlogId(blogId));

        return blog;
    }

    @Override
    public void update(Blog blog) {

        jdbcTemplate.update(SQL_UPDATE_BLOG,
                blog.getTitle(),
                blog.getContent(),
                blog.getStartDate(),
                blog.getEndDate(),
                blog.getEnabled(),
                blog.getId());

        bcdao.deleteByBlogId(blog.getId());

        if (blog.getCategories() != null) {
            blog.getCategories().stream().map((c) -> {
                BlogCategory bc = new BlogCategory();
                bc.setBlog(blog);
                bc.setCategory(c);
                return bc;
            }).forEach((bc) -> {
                bcdao.create(bc);
            });
        }
    }

    @Override
    public void delete(Blog blog) {

        bcdao.deleteByBlogId(blog.getId());
        jdbcTemplate.update(SQL_DELETE_BLOG, blog.getId());

    }

    @Override
    public List<Blog> getByCategory(Integer categoryId, Integer offset) {
        List<Blog> blogs = jdbcTemplate.query(SQL_SELECT_BLOG_BY_PAGE_BY_CATEGORY, new BlogMapper(), categoryId, (offset - 1) * 10);
        return blogs;
    }

    @Override
    public Blog blogsByRecentDate() {

        Blog blog = jdbcTemplate.queryForObject(SQL_SELECT_BLOG_BY_RECENT_DATE, new BlogMapper());
        blog.setCategories(cdao.getCategoriesByBlogId(blog.getId()));
        return blog;
    }

    @Override
    public Integer blogSize() {
        Integer blogSize = jdbcTemplate.queryForObject(SQL_COUNT_ENTRIES, Integer.class);
        return blogSize;
    }

    @Override
    public Integer blogHashtagSize(Integer hashId) {
        Integer blogSize = jdbcTemplate.queryForObject(SQL_COUNT_HASHTAG_SPECIFIC_ENTRIES, Integer.class, hashId);
        return blogSize;
    }
    
    @Override
    public Integer blogCategorySize(Integer categoryId) {
        Integer blogSize = jdbcTemplate.queryForObject(SQL_COUNT_CATEGORY_SPECIFIC_ENTRIES, Integer.class, categoryId);
        return blogSize;
    }
    
    @Override
    public Integer staticCategorySize(Integer categoryId) {
        
        Integer staticSize = jdbcTemplate.queryForObject(SQL_COUNT_CATEGORY_FOR_STATIC_ENTRIES, Integer.class, categoryId);
        
        return staticSize;
    }

    @Override
    public List<Blog> getBlogs(Integer offset) {
        List<Blog> blogs = jdbcTemplate.query(SQL_SELECT_BLOG_BY_PAGE, new BlogMapper(), (offset - 1) * 10);
        return blogs;
    }

    @Override
    public List<Blog> getPendingBlogs() {
        List<Blog> blogs = jdbcTemplate.query(SQL_SELECT_PENDING_BLOGS, new PendingBlogMapper());
        return blogs;
    }

    @Override
    public List<Blog> getByHashtag(Integer hashtagId, Integer offset) {
        List<Blog> blogs = jdbcTemplate.query(SQL_SELECT_BLOG_BY_PAGE_BY_HASHTAG, new BlogMapper(), hashtagId, (offset - 1) * 10);
        return blogs;
    }

    private static final class BlogMapper implements RowMapper<Blog> {

        @Override
        public Blog mapRow(ResultSet rs, int i) throws SQLException {
            Blog blog = new Blog();
            blog.setId(rs.getInt("blog.id"));
            blog.setTitle(rs.getString("blog.title"));
            blog.setContent(rs.getString("blog.blog_content"));
            blog.setStartDate(rs.getDate("blog.start_date"));
            blog.setEndDate(rs.getDate("blog.end_date"));

            return blog;
        }
    }

    private static final class PendingBlogMapper implements RowMapper<Blog> {

        @Override
        public Blog mapRow(ResultSet rs, int i) throws SQLException {
            Blog blog = new Blog();
            blog.setId(rs.getInt("blog.id"));
            blog.setTitle(rs.getString("blog.title"));
            blog.setContent(rs.getString("blog.blog_content"));
            blog.setStartDate(rs.getDate("blog.start_date"));
            blog.setEndDate(rs.getDate("blog.end_date"));
            Pending pending = new Pending();
            pending.setId(rs.getInt("pending.id"));
            pending.setChangeType(rs.getString("pending.change_type"));
            pending.setObjectId(rs.getInt("pending.object_id"));
            pending.setTableType(rs.getString("pending.table_type"));
            pending.setTempId(rs.getInt("pending.temp_id"));
            blog.setPending(pending);

            return blog;
        }
    }

}
