/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.BlogHashtag;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface BlogHashtagDao {

    BlogHashtag create(BlogHashtag blogHashtag);

    BlogHashtag read(Integer blogHashtagId);

    void update(BlogHashtag blogHashtag);

    void delete(Integer blogId);
    
    List<BlogHashtag> listByHashtag(Integer blogId);
}
