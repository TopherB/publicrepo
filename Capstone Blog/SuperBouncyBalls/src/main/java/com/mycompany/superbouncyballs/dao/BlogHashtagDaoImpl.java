/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.Blog;
import com.mycompany.superbouncyballs.dto.BlogHashtag;
import com.mycompany.superbouncyballs.dto.Hashtag;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class BlogHashtagDaoImpl implements BlogHashtagDao {

    private final String SQL_INSERT_BLOG_HASHTAG = "INSERT INTO blog_hashtag (blog_id, hashtag_id) VALUES (?, ?)";
    private final String SQL_UPDATE_BLOG_HASHTAG = "UPDATE blog_hashtag SET blog_id=?, hashtag_id=? WHERE id=?;";
    private final String SQL_SELECT_BLOG_HASHTAG = "SELECT * FROM blog_hashtag WHERE id=?;";
    private final String SQL_SELECT_ALL_BLOG_HASHTAG_BY_HASHTAG = "SELECT * FROM blog_hashtag WHERE hashtag_id = ?;";
    private final String SQL_DELETE_BLOG_HASHTAG = "DELETE FROM blog_hashtag WHERE blog_id = ?;";

    private final JdbcTemplate jdbcTemplate;
    private static BlogDaoImpl bdao;
    private static HashtagDaoImpl hdao;

    public BlogHashtagDaoImpl(JdbcTemplate j, BlogDaoImpl bd, HashtagDaoImpl hd) {
        this.jdbcTemplate = j;
        this.bdao = bd;
        this.hdao = hd;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public BlogHashtag create(BlogHashtag blogHashtag) {

        jdbcTemplate.update(SQL_INSERT_BLOG_HASHTAG,
                blogHashtag.getBlog().getId(),
                blogHashtag.getHashtag().getId());

        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        blogHashtag.setId(newId);

        return blogHashtag;
    }

    @Override
    public BlogHashtag read(Integer blogHashtagId) {

        BlogHashtag bh = jdbcTemplate.queryForObject(SQL_SELECT_BLOG_HASHTAG, new BlogHashtagMapper(), blogHashtagId);
        return bh;
    }

    @Override
    public void update(BlogHashtag blogHashtag) {
        
        jdbcTemplate.update(SQL_UPDATE_BLOG_HASHTAG,
                blogHashtag.getBlog().getId(),
                blogHashtag.getHashtag().getId(),
                blogHashtag.getId());
    }

    @Override
    public void delete(Integer blogId) {

        jdbcTemplate.update(SQL_DELETE_BLOG_HASHTAG, blogId);
    }

    @Override
    public List<BlogHashtag> listByHashtag(Integer hashId) {
        List<BlogHashtag> blogHash = jdbcTemplate.query(SQL_SELECT_ALL_BLOG_HASHTAG_BY_HASHTAG, new BlogHashtagMapper(), hashId);

        return blogHash;
    }
    
    private static final class BlogHashtagMapper implements RowMapper<BlogHashtag> {

        @Override
        public BlogHashtag mapRow(ResultSet rs, int i) throws SQLException {
            BlogHashtag bh = new BlogHashtag();
            bh.setId(rs.getInt("id"));
            int blogId = rs.getInt("blog_id");
            Blog blog = bdao.read(blogId);
            bh.setBlog(blog);
            int hashtagId = rs.getInt("hashtag_id");
            Hashtag hashtag = hdao.read(hashtagId);
            bh.setHashtag(hashtag);

            return bh;
        }
    }

}
