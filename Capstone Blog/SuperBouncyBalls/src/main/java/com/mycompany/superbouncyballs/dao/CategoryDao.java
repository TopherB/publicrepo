/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.Category;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface CategoryDao {

    Category create(Category category);

    Category read(Integer categoryId);

    void update(Category category);

    void delete(Category category);
    
    List<Category> readAll();
    
    List<Category> getCategoriesByBlogId(Integer blogId);
    
    List<Category> getCategoriesByStaticId(Integer staticId);
    
    List<String> firstLetterOfCategory();
    
    List<Category> currentList();
    
    List<Category> otherCurrentList();
    
    List<String> otherFirstLetterOfCategory();
}
