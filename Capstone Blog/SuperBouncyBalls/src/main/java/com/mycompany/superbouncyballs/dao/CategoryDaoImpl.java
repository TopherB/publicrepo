/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.Category;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class CategoryDaoImpl implements CategoryDao {

    private final String SQL_INSERT_CATEGORY = "INSERT INTO categories (category) VALUES (?)";
    private final String SQL_UPDATE_CATEGORY = "UPDATE categories SET category=? WHERE id=?;";
    private final String SQL_SELECT_CATEGORY = "SELECT * FROM categories WHERE id=?;";
    private final String SQL_SELECT_ALL_CATEGORIES = "SELECT * FROM categories ORDER BY category ASC;";
    private final String SQL_SELECT_CATEGORIES_BY_BLOG_ID = "SELECT c.id, c.category FROM tmcblog.categories c "
            + "JOIN categories_blog cb ON c.id = cb.category_id WHERE cb.blog_id=?;";
    private final String SQL_SELECT_CATEGORIES_BY_STATIC_ID = "SELECT c.id, c.category FROM tmcblog.categories c "
            + "JOIN categories_staticpages cs ON c.id = cs.category_id WHERE cs.static_page_id=?;";
    private final String SQL_DELETE_CATEGORY = "DELETE FROM categories WHERE id = ?;";
    private final String SQL_SELECT_FIRST_LETTER_OF_CATEGORY = "SELECT DISTINCT LEFT (UPPER(c.category), 1) ca FROM categories c "
            + "INNER JOIN categories_blog cb ON c.id=cb.category_id INNER JOIN blog blog ON blog.id=cb.blog_id "
            + "WHERE blog.start_date <= CURRENT_DATE() AND (blog.end_date >= CURRENT_DATE() OR blog.end_date IS NULL) "
            + "AND blog.enabled=1 ORDER BY ca ASC;";
    private final String SQL_SELECT_ALL_CURRENT_CATEGORIES = "SELECT DISTINCT c.category, c.id FROM categories_blog cb "
            + "INNER JOIN categories c ON c.id=cb.category_id INNER JOIN blog blog ON blog.id=cb.blog_id "
            + "WHERE blog.start_date <= CURRENT_DATE() AND (blog.end_date >= CURRENT_DATE() OR blog.end_date IS NULL) "
            + "AND blog.enabled=1 ORDER BY category ASC;";
    private final String SQL_SELECT_ALL_CURRENT_STATIC_CATEGORIES = "SELECT DISTINCT c.category, c.id FROM categories_staticpages csp "
            + "INNER JOIN categories c ON c.id=csp.category_id INNER JOIN static_pages sp ON sp.id=csp.static_page_id "
            + "WHERE sp.start_date <= CURRENT_DATE() AND (sp.end_date >= CURRENT_DATE() OR sp.end_date IS NULL) ORDER BY c.category ASC;";
    private final String SQL_SELECT_FIRST_LETTER_OF_CATEGORY_FOR_STATIC = "SELECT DISTINCT LEFT (UPPER(c.category), 1) ca "
            + "FROM categories c INNER JOIN categories_staticpages csp ON c.id=csp.category_id INNER JOIN static_pages sp "
            + "ON sp.id=csp.static_page_id WHERE sp.start_date <= CURRENT_DATE() AND (sp.end_date >= CURRENT_DATE() "
            + "OR sp.end_date IS NULL) ORDER BY ca ASC;";

    private final JdbcTemplate jdbcTemplate;

    public CategoryDaoImpl(JdbcTemplate j) {
        this.jdbcTemplate = j;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Category create(Category category) {

        jdbcTemplate.update(SQL_INSERT_CATEGORY,
                category.getCategoryName());

        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        category.setId(newId);

        return category;
    }

    @Override
    public Category read(Integer categoryId) {
        
        Category c = jdbcTemplate.queryForObject(SQL_SELECT_CATEGORY, new CategoryMapper(), categoryId);
        return c;
    }

    @Override
    public void update(Category category) {
        
        jdbcTemplate.update(SQL_UPDATE_CATEGORY,
                category.getCategoryName(),
                category.getId());
    }

    @Override
    public void delete(Category category) {

        jdbcTemplate.update(SQL_DELETE_CATEGORY, category.getId());
    }
    
    @Override
    public List<Category> readAll(){
        List<Category> allCategories = jdbcTemplate.query(SQL_SELECT_ALL_CATEGORIES, new CategoryMapper());
        return allCategories;
    }
    
    @Override
    public List<Category> getCategoriesByBlogId(Integer blogId){
        List<Category> categories = jdbcTemplate.query(SQL_SELECT_CATEGORIES_BY_BLOG_ID, new CategoryMapper(), blogId);
        return categories;
    }
    
    @Override
    public List<Category> getCategoriesByStaticId(Integer staticId){
        List<Category> categories = jdbcTemplate.query(SQL_SELECT_CATEGORIES_BY_STATIC_ID, new CategoryMapper(), staticId);
        return categories;
    }
    
    @Override
    public List<String> firstLetterOfCategory() {

        List<String> firstLetter = jdbcTemplate.queryForList(SQL_SELECT_FIRST_LETTER_OF_CATEGORY, String.class);

        return firstLetter;
    }
    
    @Override
    public List<String> otherFirstLetterOfCategory() {

        List<String> firstLetter = jdbcTemplate.queryForList(SQL_SELECT_FIRST_LETTER_OF_CATEGORY_FOR_STATIC, String.class);

        return firstLetter;
    }
    
    @Override
    public List<Category> currentList() {

        List<Category> categories = jdbcTemplate.query(SQL_SELECT_ALL_CURRENT_CATEGORIES, new CategoryMapper());

        return categories;
    }
    
    @Override
    public List<Category> otherCurrentList() {
        
        List<Category> categories = jdbcTemplate.query(SQL_SELECT_ALL_CURRENT_STATIC_CATEGORIES, new CategoryMapper());
        
        return categories;
    }

    private static final class CategoryMapper implements RowMapper<Category> {

        @Override
        public Category mapRow(ResultSet rs, int i) throws SQLException {
            Category c = new Category();
            c.setId(rs.getInt("id"));
            c.setCategoryName(rs.getString("category"));

            return c;
        }
    }

}
