/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.Comment;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class CommentDaoImpl implements CommentDao {

    private final String SQL_INSERT_COMMENT = "INSERT INTO comments (title, author, comment, email, date, blog_id) VALUES (?,?,?,?,?,?)";
    private final String SQL_DELETE_COMMENT = "DELETE FROM comments WHERE id = ?;";
    private final String SQL_DELETE_COMMENT_BY_BLOG = "DELETE FROM comments WHERE blog_id = ?;";
    private final String SQL_SELECT_LIST_COMMENTS = "SELECT * FROM comments WHERE blog_id=?;";

    private final JdbcTemplate jdbcTemplate;

    public CommentDaoImpl(JdbcTemplate j) {
        this.jdbcTemplate = j;
    }

    @Override
    public Comment create(Comment comment) {

        jdbcTemplate.update(SQL_INSERT_COMMENT,
                comment.getTitle(),
                comment.getAuthor(),
                comment.getComment(),
                comment.getEmail(),
                comment.getDate(),
                comment.getBlogId());

        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        comment.setId(newId);

        return comment;
    }

    @Override
    public void delete(Comment comment) {
        jdbcTemplate.update(SQL_DELETE_COMMENT, comment.getId());
    }
    
        @Override
    public void deleteByBlog(Integer blogId) {
        jdbcTemplate.update(SQL_DELETE_COMMENT_BY_BLOG, blogId);
    }

    @Override
    public List<Comment> getComments(Integer blogId) {
        List<Comment> commment = jdbcTemplate.query(SQL_SELECT_LIST_COMMENTS, new CommentMapper(), blogId);
        return commment;
    }

    private static final class CommentMapper implements RowMapper<Comment> {

        @Override
        public Comment mapRow(ResultSet rs, int i) throws SQLException {

            Comment comment = new Comment();
            comment.setId(rs.getInt("id"));
            comment.setTitle(rs.getString("title"));
            comment.setAuthor(rs.getString("author"));
            comment.setComment(rs.getString("comment"));
            comment.setEmail(rs.getString("email"));
            comment.setDate(rs.getDate("date"));
            comment.setBlogId(rs.getInt("blog_id"));

            return comment;
        }

    }

}
