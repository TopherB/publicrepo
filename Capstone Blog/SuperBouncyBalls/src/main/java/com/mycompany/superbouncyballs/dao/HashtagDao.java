/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.Hashtag;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface HashtagDao {

    Hashtag create(Hashtag hashtag);

    Hashtag read(Integer hashtagId);
    
    Hashtag readString(String hashtag);

    void update(Hashtag hashtag);

    void delete(Hashtag hashtag);
    
    List<Hashtag> list();
    
    List<Hashtag> currentList();
    
    List<Hashtag> hashtagsInBlog(Integer blogId);
    
    List<Hashtag> hashtagsByLetter(String alph);
    
    List<String> firstLetterOfHashtag();
}
