/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.Hashtag;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class HashtagDaoImpl implements HashtagDao {

    private final String SQL_INSERT_HASHTAG = "INSERT INTO hashtags (hashtag) VALUES (?)";
    private final String SQL_UPDATE_HASHTAG = "UPDATE hashtags SET hashtag=? WHERE id=?;";
    private final String SQL_SELECT_HASHTAG = "SELECT * FROM hashtags WHERE id=?;";
    private final String SQL_DELETE_HASHTAG = "DELETE FROM hashtags WHERE id = ?;";
    private final String SQL_SELECT_ALL_CURRENT_HASHTAGS = "SELECT DISTINCT h.hashtag, h.id FROM blog_hashtag bh INNER JOIN hashtags h "
            + "ON h.id=bh.hashtag_id INNER JOIN blog blog ON blog.id=bh.blog_id WHERE blog.start_date < CURRENT_DATE() "
            + "AND (blog.end_date > CURRENT_DATE() OR blog.end_date IS NULL) AND blog.enabled=1 ORDER BY hashtag ASC;";
    private final String SQL_SELECT_ALL_HASHTAGS = "SELECT id, LCASE(hashtag) FROM hashtags ORDER BY LCASE(hashtag) ASC;";
    private final String SQL_SELECT_HASHTAG_BY_NAME = "SELECT * FROM hashtags WHERE hashtag = ?;";
    private final String SQL_SELECT_HASHTAGS_IN_BLOG = "SELECT h.id, h.hashtag FROM blog_hashtag b "
            + "INNER JOIN hashtags h ON b.hashtag_id=h.id WHERE blog_id=?;";
    private final String SQL_SELECT_HASHTAGS_BY_LETTER = "SELECT * FROM hashtags WHERE hashtag LIKE ? ORDER BY hashtag ASC;";
    private final String SQL_SELECT_FIRST_LETTER_OF_HASHTAG = "SELECT DISTINCT LEFT (UPPER(h.hashtag), 1) ha FROM hashtags h "
            + "INNER JOIN blog_hashtag bh ON h.id=bh.hashtag_id INNER JOIN blog blog ON blog.id=bh.blog_id "
            + "WHERE blog.start_date < CURRENT_DATE() AND (blog.end_date > CURRENT_DATE() OR blog.end_date IS NULL) "
            + "AND blog.enabled=1 ORDER BY ha ASC;";

    private final JdbcTemplate jdbcTemplate;

    public HashtagDaoImpl(JdbcTemplate j) {
        this.jdbcTemplate = j;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Hashtag create(Hashtag hashtag) {

        jdbcTemplate.update(SQL_INSERT_HASHTAG,
                hashtag.getHashtagName());

        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        hashtag.setId(newId);

        return hashtag;
    }

    @Override
    public Hashtag read(Integer hashtagId) {

        Hashtag h = jdbcTemplate.queryForObject(SQL_SELECT_HASHTAG, new HashtagMapper(), hashtagId);
        return h;
    }

    @Override
    public Hashtag readString(String hashtagName) {

        Hashtag h = jdbcTemplate.queryForObject(SQL_SELECT_HASHTAG_BY_NAME, new HashtagMapper(), hashtagName);
        return h;
    }

    @Override
    public void update(Hashtag hashtag) {

        jdbcTemplate.update(SQL_UPDATE_HASHTAG,
                hashtag.getHashtagName(),
                hashtag.getId());
    }

    @Override
    public void delete(Hashtag hashtag) {

        jdbcTemplate.update(SQL_DELETE_HASHTAG, hashtag.getId());
    }

    @Override
    public List<Hashtag> currentList() {

        List<Hashtag> hashtags = jdbcTemplate.query(SQL_SELECT_ALL_CURRENT_HASHTAGS, new HashtagMapper());

        return hashtags;
    }

    @Override
    public List<Hashtag> list() {

        List<Hashtag> hashtags = jdbcTemplate.query(SQL_SELECT_ALL_HASHTAGS, new LCaseHashtagMapper());

        return hashtags;
    }

    @Override
    public List<Hashtag> hashtagsInBlog(Integer blogId) {

        List<Hashtag> hashtags = jdbcTemplate.query(SQL_SELECT_HASHTAGS_IN_BLOG, new HashtagMapper(), blogId);

        return hashtags;
    }

    @Override
    public List<String> firstLetterOfHashtag() {

        List<String> firstLetter = jdbcTemplate.queryForList(SQL_SELECT_FIRST_LETTER_OF_HASHTAG, String.class);

        return firstLetter;
    }

    @Override
    public List<Hashtag> hashtagsByLetter(String a) {

        List<Hashtag> hashtags = jdbcTemplate.query(SQL_SELECT_HASHTAGS_BY_LETTER, new HashtagMapper(), a + "%");

        return hashtags;
    }
    
    private static final class LCaseHashtagMapper implements RowMapper<Hashtag> {
        
        @Override
        public Hashtag mapRow(ResultSet rs, int i) throws SQLException {
            
            Hashtag h = new Hashtag();
            h.setId(rs.getInt("id"));
            h.setHashtagName(rs.getString("LCASE(hashtag)"));
            
            return h;
        }
     }

    private static final class HashtagMapper implements RowMapper<Hashtag> {

        @Override
        public Hashtag mapRow(ResultSet rs, int i) throws SQLException {

            Hashtag h = new Hashtag();
            h.setId(rs.getInt("id"));
            h.setHashtagName(rs.getString("hashtag"));

            return h;
        }

    }
}
