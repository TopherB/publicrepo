/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.Pending;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface PendingDao {

    Pending create(Pending pending);

    Pending read(Integer pendingId);

    void update(Pending pending);

    void delete(Pending pending);

    List<Pending> getPending();

}
