/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.Pending;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class PendingDaoImpl implements PendingDao {

    private final String SQL_INSERT_PENDING = "INSERT INTO pending (change_type, object_id, table_type, temp_id) "
            + "VALUES (?, ?, ?, ?)";
    private final String SQL_SELECT_PENDING = "SELECT * FROM pending WHERE id=?;";
    private final String SQL_SELECT_PENDING_BY_BLOG = "SELECT * FROM pending WHERE temp_id=?;";
    private final String SQL_DELETE_PENDING = "DELETE FROM pending WHERE id = ?;";
    private final String SQL_LIST_PENDING = "SELECT * FROM pending;";

    private final JdbcTemplate jdbcTemplate;

    public PendingDaoImpl(JdbcTemplate j) {
        this.jdbcTemplate = j;

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Pending create(Pending pending) {

        jdbcTemplate.update(SQL_INSERT_PENDING,
                pending.getChangeType(),
                pending.getObjectId(),
                pending.getTableType(),
                pending.getTempId());

        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        pending.setId(newId);

        return pending;
    }

    @Override
    public Pending read(Integer pendingId) {
        Pending pending = jdbcTemplate.queryForObject(SQL_SELECT_PENDING, new PendingMapper(), pendingId);
        return pending;
    }

    @Override
    public void update(Pending pending) {

    }

    @Override
    public void delete(Pending pending) {
        jdbcTemplate.update(SQL_DELETE_PENDING, pending.getId());
    }

    @Override
    public List<Pending> getPending() {
        List<Pending> pending = jdbcTemplate.query(SQL_LIST_PENDING, new PendingMapper());
        return pending;
        
    }
    
    private static final class PendingMapper implements RowMapper<Pending> {

        @Override
        public Pending mapRow(ResultSet rs, int i) throws SQLException {
            Pending pending = new Pending();
            pending.setId(rs.getInt("id"));
            pending.setChangeType(rs.getString("change_type"));
            pending.setObjectId(rs.getInt("object_id"));
            pending.setTableType(rs.getString("table_type"));
            pending.setTempId(rs.getInt("temp_id"));

            return pending;
        }
    }

}
