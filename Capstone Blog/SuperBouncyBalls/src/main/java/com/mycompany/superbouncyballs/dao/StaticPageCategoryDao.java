/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.StaticPageCategory;

/**
 *
 * @author apprentice
 */
public interface StaticPageCategoryDao {

    StaticPageCategory create(StaticPageCategory staticPageeCategory);

    StaticPageCategory read(Integer staticPageCategoryId);

    void update(StaticPageCategory staticPageCategory);

    void delete(StaticPageCategory staticPageCategory);
    
    void deleteByStaticPageId(Integer staticPageId);
    
    void deleteByCategoryId(Integer categoryId);
}
