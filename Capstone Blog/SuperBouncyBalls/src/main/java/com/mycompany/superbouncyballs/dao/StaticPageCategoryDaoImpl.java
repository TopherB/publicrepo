/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.StaticPageCategory;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class StaticPageCategoryDaoImpl implements StaticPageCategoryDao {

    private final String SQL_INSERT_STATIC_CATEGORY = "INSERT INTO categories_staticpages (category_id, static_page_id) VALUES (?, ?)";
    private final String SQL_UPDATE_STATIC_CATEGORY = "UPDATE categories_staticpages SET category_id=?, static_page_id=? WHERE id=?;";
    private final String SQL_SELECT_STATIC_CATEGORY = "SELECT * FROM categories_staticpages WHERE id=?;";
    private final String SQL_DELETE_STATIC_CATEGORY = "DELETE FROM categories_staticpages WHERE id = ?;";
    private final String SQL_DELETE_STATIC_CATEGORY_BY_PAGE_ID = "DELETE FROM categories_staticpages WHERE static_page_id = ?;";
    private final String SQL_DELETE_STATIC_CATEGORY_BY_CATEGORY_ID = "DELETE FROM categories_staticpages WHERE category_id = ?;";

    private final JdbcTemplate jdbcTemplate;

    public StaticPageCategoryDaoImpl(JdbcTemplate j ) {
        this.jdbcTemplate = j;
        
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public StaticPageCategory create(StaticPageCategory staticPageCategory) {

        jdbcTemplate.update(SQL_INSERT_STATIC_CATEGORY,
                staticPageCategory.getCategory().getId(),
                staticPageCategory.getStaticPage().getId());

        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        staticPageCategory.setId(newId);

        return staticPageCategory;
    }

    @Override
    public StaticPageCategory read(Integer staticPageCategoryId) {
        
        StaticPageCategory c = jdbcTemplate.queryForObject(SQL_SELECT_STATIC_CATEGORY, new StaticPageCategoryMapper(), staticPageCategoryId);
        return c;
    }

    @Override
    public void update(StaticPageCategory staticPageCategory) {
        
        jdbcTemplate.update(SQL_UPDATE_STATIC_CATEGORY,
                staticPageCategory.getCategory().getId(),
                staticPageCategory.getStaticPage().getId(),
                staticPageCategory.getId());
    }

    @Override
    public void delete(StaticPageCategory staticPageCategory) {

        jdbcTemplate.update(SQL_DELETE_STATIC_CATEGORY, staticPageCategory.getId());
    }
    
    @Override
    public void deleteByStaticPageId(Integer staticPageId){
        jdbcTemplate.update(SQL_DELETE_STATIC_CATEGORY_BY_PAGE_ID, staticPageId);
    }
    
    @Override
    public void deleteByCategoryId(Integer categoryId){
        jdbcTemplate.update(SQL_DELETE_STATIC_CATEGORY_BY_CATEGORY_ID, categoryId);
    }
            
    //Totally broken. Needs a new StaticPageCategory object with only integers.
    private static final class StaticPageCategoryMapper implements RowMapper<StaticPageCategory> {

        @Override
        public StaticPageCategory mapRow(ResultSet rs, int i) throws SQLException {
            StaticPageCategory spc = new StaticPageCategory();
            spc.setId(rs.getInt("id"));
            int categoryId = rs.getInt("category_id");
//            Category c = cdao.read(categoryId);
//            spc.setCategory(c);
            int pageId = rs.getInt("static_page_id");
//            StaticPage sp = spdao.read(pageId);
//            spc.setStaticPage(sp);

            return spc;
        }
    }

}
