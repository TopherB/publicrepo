/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.StaticPage;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface StaticPageDao {

    StaticPage create(StaticPage staticPage);

    StaticPage read(Integer staticPageId);

    void update(StaticPage staticPage);

    void delete(StaticPage staticPage);
    
    public List<StaticPage> getStaticPages();
    
    List<StaticPage> getByCategory(Integer categoryId, Integer offset);
}
