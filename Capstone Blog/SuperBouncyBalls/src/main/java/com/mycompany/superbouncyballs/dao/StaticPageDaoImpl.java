/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.StaticPage;
import com.mycompany.superbouncyballs.dto.StaticPageCategory;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class StaticPageDaoImpl implements StaticPageDao {

    private final String SQL_INSERT_STATIC_PAGE = "INSERT INTO static_pages (title, page_content, start_date, end_date) "
            + "VALUES (?, ?, ?, ?)";
    private final String SQL_UPDATE_STATIC_PAGE = "UPDATE static_pages SET title=?, page_content=?, start_date=?, end_date=? "
            + "WHERE id=?;";
    private final String SQL_SELECT_STATIC_PAGE = "SELECT * FROM static_pages WHERE id=?;";
    private final String SQL_DELETE_STATIC_PAGE = "DELETE FROM static_pages WHERE id = ?;";
    private final String SQL_LIST_STATIC_PAGES = "SELECT * FROM static_pages WHERE start_date <= CURRENT_DATE() "
            + "AND (end_date >= CURRENT_DATE OR end_date IS NULL) ORDER BY start_date DESC;";
    private final String SQL_SELECT_STATIC_BY_PAGE_BY_CATEGORY = "SELECT s.id, s.title, s.page_content, s.start_date, s.end_date "
            + "FROM static_pages s INNER JOIN categories_staticpages cs ON s.id=cs.static_page_id WHERE category_id = ? "
            + "AND s.start_date <= CURRENT_DATE() AND (s.end_date >= CURRENT_DATE() OR s.end_date IS NULL) ORDER BY s.start_date DESC "
            + "LIMIT 10 OFFSET ?;";

    private final JdbcTemplate jdbcTemplate;
    private final CategoryDao cdao;
    private final StaticPageCategoryDao scdao;

    public StaticPageDaoImpl(JdbcTemplate j, CategoryDao cd, StaticPageCategoryDao scd) {
        this.jdbcTemplate = j;
        this.cdao = cd;
        this.scdao = scd;

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public StaticPage create(StaticPage staticPage) {

        jdbcTemplate.update(SQL_INSERT_STATIC_PAGE,
                staticPage.getTitle(),
                staticPage.getPageContent(),
                staticPage.getStartDate(),
                staticPage.getEndDate());

        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        staticPage.setId(newId);

        return staticPage;
    }

    @Override
    public StaticPage read(Integer staticPageId) {

        StaticPage sp = jdbcTemplate.queryForObject(SQL_SELECT_STATIC_PAGE, new StaticPageMapper(), staticPageId);
        sp.setCategories(cdao.getCategoriesByStaticId(staticPageId));
        return sp;
    }

    @Override
    public void update(StaticPage staticPage) {

        jdbcTemplate.update(SQL_UPDATE_STATIC_PAGE,
                staticPage.getTitle(),
                staticPage.getPageContent(),
                staticPage.getStartDate(),
                staticPage.getEndDate(),
                staticPage.getId());

        scdao.deleteByStaticPageId(staticPage.getId());

        if (staticPage.getCategories() != null) {
            staticPage.getCategories().stream().map((c) -> {
                StaticPageCategory sc = new StaticPageCategory();
                sc.setStaticPage(staticPage);
                sc.setCategory(c);
                return sc;
            }).forEach((sc) -> {
                scdao.create(sc);
            });
        }
    }

    @Override
    public void delete(StaticPage staticPage) {

        jdbcTemplate.update(SQL_DELETE_STATIC_PAGE, staticPage.getId());
    }

    @Override
    public List<StaticPage> getStaticPages() {
        List<StaticPage> staticPages = jdbcTemplate.query(SQL_LIST_STATIC_PAGES, new StaticPageMapper());
        return staticPages;
    }
    
    @Override
    public List<StaticPage> getByCategory(Integer categoryId, Integer offset) {
        List<StaticPage> statPages = jdbcTemplate.query(SQL_SELECT_STATIC_BY_PAGE_BY_CATEGORY, new StaticPageMapper(), categoryId, (offset - 1) * 10);
        return statPages;
    }

    private static final class StaticPageMapper implements RowMapper<StaticPage> {

        @Override
        public StaticPage mapRow(ResultSet rs, int i) throws SQLException {
            StaticPage sp = new StaticPage();
            sp.setId(rs.getInt("id"));
            sp.setTitle(rs.getString("title"));
            sp.setPageContent(rs.getString("page_content"));
            sp.setStartDate(rs.getDate("start_date"));
            sp.setEndDate(rs.getDate("end_date"));

            return sp;
        }
    }

}
