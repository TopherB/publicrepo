/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.User;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface UserDao {

    User create(User user);

    User read(Integer userId);

    void update(User user);

    void delete(User user);
    
    List<User> getUsers();
}
