/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superbouncyballs.dao;

import com.mycompany.superbouncyballs.dto.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class UserDaoImpl implements UserDao {

    private final String SQL_INSERT_USER = "INSERT INTO users (username, password, enabled) VALUES (?, ?, ?)";
    private final String SQL_UPDATE_USER = "UPDATE users SET username=?, password=?, enabled=? WHERE user_id=?;";
    private final String SQL_SELECT_USER = "SELECT * FROM users WHERE user_id=?;";
    private final String SQL_DELETE_USER = "DELETE FROM users WHERE user_id = ?;";
    private final String SQL_SELECT_LIST_USERS = "SELECT * FROM users;";

    private final JdbcTemplate jdbcTemplate;

    public UserDaoImpl(JdbcTemplate j) {
        this.jdbcTemplate = j;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public User create(User user) {

        jdbcTemplate.update(SQL_INSERT_USER,
                user.getUserName(),
                user.getPassword(),
                user.getEnabled());

        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        user.setId(newId);

        return user;
    }

    @Override
    public User read(Integer userId) {

        User user = jdbcTemplate.queryForObject(SQL_SELECT_USER, new UserMapper(), userId);
        return user;
    }

    @Override
    public void update(User user) {

        jdbcTemplate.update(SQL_UPDATE_USER,
                user.getUserName(),
                user.getPassword(),
                user.getEnabled(),
                user.getId());
    }

    @Override
    public void delete(User user) {

        jdbcTemplate.update(SQL_DELETE_USER, user.getId());
    }

    @Override
    public List<User> getUsers() {
        List<User> users = jdbcTemplate.query(SQL_SELECT_LIST_USERS, new UserMapper());
        return users;
    }

    private static final class UserMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {

            User user = new User();
            user.setId(rs.getInt("user_id"));
            user.setUserName(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setEnabled(rs.getInt("enabled"));

            return user;
        }

    }

}
