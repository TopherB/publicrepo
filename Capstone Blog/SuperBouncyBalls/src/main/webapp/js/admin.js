/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $('#deleteCommentModal').on('shown.bs.modal', function (e) {

        var link = $(e.relatedTarget);
        var commentId = link.data('comment-id');
        $('#deleteCommentId').val(commentId);
        console.log(commentId);
    });

    $('#deleteCommentButton').on('click', function (e) {

        e.preventDefault();
        var comment = {
            id: $('#deleteCommentId').val(),
        };
        var commentData = JSON.stringify(comment);
        $.ajax({
            url: contextRoot + "/comment/" + comment.id,
            type: "DELETE",
            data: commentData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                location.reload();
            },
            error: function (data, status) {
                console.log("ERROR");
            }
        });
    });

    $('#showBlogEntryButton').on('click', function (e) {
        document.getElementById('addBlogDiv').style.display = "block";
        document.getElementById('showBlogEntryDiv').style.display = "none";
    });

    $('#cancelBlogButton').on('click', function (e) {
        document.getElementById('addBlogDiv').style.display = "none";
        document.getElementById('showBlogEntryDiv').style.display = "block";

        $('#titleCreate').val('');
        tinyMCE.activeEditor.setContent('');
        $('#startDateCreate').val('');
        $('#endDateCreate').val('');
    });

    $('#addBlogButton').on('click', function (e) {

        e.preventDefault();
        tinymce.triggerSave();

        var myCategories = [];
        $('#categorySelect option').each(function (i) {
            if (true == this.selected) {
                myCategories.push(this.value);
            }
        });

        var myBlogCommandObject = {
            title: $('#titleCreate').val(),
            content: $('#contentCreate').val(),
            startDate: $('#startDateCreate').val(),
            endDate: $('#endDateCreate').val(),
            enabled: $('#enabledCreate').val(),
            categories: myCategories
        };

        var fieldNames = Object.keys(myBlogCommandObject);
        $.each(fieldNames, function (index, fieldName) {
            $('#add-' + fieldName + '-validation-errors').text('');
        });
        var myBlogCommandObjectData = JSON.stringify(myBlogCommandObject);
        console.log("myBlogData" + myBlogCommandObjectData);
        $.ajax({
            url: contextRoot + "/blog",
            type: "POST",
            data: myBlogCommandObjectData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                $('#titleCreate').val('');
                tinyMCE.activeEditor.setContent('');
                $('#startDateCreate').val('');
                $('#endDateCreate').val('');
                $('#categorySelect option').each(function (i) {
                    this.selected = false;
                });

                document.getElementById('addBlogDiv').style.display = "none";

                location.reload();
            },
            error: function (data, status) {

                var errors = data.responseJSON.errors;

                $.each(errors, function (index, error) {
                    var fieldNames = Object.keys(myBlogCommandObject);
                    $.each(fieldNames, function (index, fieldName) {

                        if (error.fieldName == fieldName) {
                            $('#add-' + fieldName + '-validation-errors').append(error.fieldName + ": " + error.message + "<br />");
                        }
                    });
                });
            }
        });
    });

    $('#cancelEditBlogButton').on('click', function (e) {
        history.go(-1);
    });

    $('#deleteBlogModal').on('shown.bs.modal', function (e) {

        var link = $(e.relatedTarget);
        var blogId = link.data('blog-id');
        $('#deleteBlogId').val(blogId);
    });

    $('#deleteBlogModal').on('hidden.bs.modal', function () {
        $('#deleteTitle').text('');
        $('#deleteStartDate').text('');
    });

    $('#deleteBlogButton').on('click', function (e) {

        e.preventDefault();
        var blog = {
            id: $('#deleteBlogId').val(),
            enabled: $('#deleteBlogEnabled').val()
        };
        var blogData = JSON.stringify(blog);
        $.ajax({
            url: contextRoot + "/blog/" + blog.id,
            type: "DELETE",
            data: blogData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                location.reload();
            },
            error: function (data, status) {
                console.log("ERROR");
            }
        });
    });

    $('#cancelEditStaticButton').on('click', function (e) {
        history.go(-1);
    });

    $('#deleteStaticPageModal').on('shown.bs.modal', function (e) {

        var link = $(e.relatedTarget);
        var staticPageId = link.data('staticpage-id');
        console.log(staticPageId);
        $('#deleteStaticPageId').val(staticPageId);
    });

    $('#deleteStaticPageButton').on('click', function (e) {

        e.preventDefault();
        var staticPage = {
            id: $('#deleteStaticPageId').val()
        };
        var staticPageData = JSON.stringify(staticPage);
        $.ajax({
            url: contextRoot + "/static/" + staticPage.id,
            type: "DELETE",
            data: staticPageData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                window.location.replace(contextRoot);
                console.log("OK");
            },
            error: function (data, status) {
                console.log("ERROR");
            }
        });
    });

    $('#addPageButton').on('click', function (e) {

        e.preventDefault();
        tinymce.triggerSave();
        var myCategories = [];
        $('#categorySelect option').each(function (i) {
            if (true == this.selected) {
                myCategories.push(this.value);
            }
        });
        console.log(myCategories);
        var myPageCommand = {
            title: $('#titleCreate').val(),
            pageContent: $('#pageContentCreate').val(),
            startDate: $('#startDateCreate').val(),
            endDate: $('#endDateCreate').val(),
            categories: myCategories
        };
        var fieldNames = Object.keys(myPageCommand);
        $.each(fieldNames, function (index, fieldName) {
            $('#add-' + fieldName + '-validation-errors').text('');
        });
        var myPageCommandData = JSON.stringify(myPageCommand);
        console.log(myPageCommandData);
        $.ajax({
            url: contextRoot + "/static",
            type: "POST",
            data: myPageCommandData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                $('#titleCreate').val('');
                tinyMCE.activeEditor.setContent('');
                $('#startDateCreate').val('');
                $('#endDateCreate').val('');
                $('#categorySelect option').each(function (i) {
                    this.selected = false;
                });

            },
            error: function (data, status) {

                var errors = data.responseJSON.errors;

                $.each(errors, function (index, error) {
                    var fieldNames = Object.keys(myPageCommand);
                    $.each(fieldNames, function (index, fieldName) {

                        if (error.fieldName == fieldName) {
                            $('#add-' + fieldName + '-validation-errors').append(error.fieldName + ": " + error.message + "<br />");
                        }
                    });
                });
            }
        });
    });

    $('#addCategoryButton').on('click', function (e) {
        e.preventDefault();

        var myCategory = {
            categoryName: $('#newCategory').val()
        };
        var myCategoryData = JSON.stringify(myCategory);
        console.log(myCategory);
        $.ajax({
            url: contextRoot + "/category",
            type: "POST",
            data: myCategoryData,
            datatype: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                $('#newCategory').val('');

                var select = document.getElementById("categorySelect");
                select.options[select.options.length] = new Option(data.categoryName, data.id);
            },
            error: function (data, status) {
                alert($('#newCategory').val() + " already exists");
                $('#newCategory').val('');
            }
        });

    });

    $('#deleteCategoryModalButton').on('click', function (e) {
        e.preventDefault();
    });

    $('#deleteCategoryButton').on('click', function (e) {
        e.preventDefault();

        var myCategoryIds = [];
        $('#categorySelect option').each(function (i) {
            if (true == this.selected) {
                myCategoryIds.push(this.value);
            }
        });

        var myCategoryIdsData = JSON.stringify(myCategoryIds);
        console.log(myCategoryIds);
        $.ajax({
            url: contextRoot + "/category",
            type: "DELETE",
            data: myCategoryIdsData,
            datatype: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                var selectobject = document.getElementById("categorySelect");
                for (var j = 0; j < myCategoryIds.length; j++) {
                    for (var i = 0; i < selectobject.length; i++) {
                        if (selectobject.options[i].value == myCategoryIds[j])
                            selectobject.remove(i);
                    }
                }
            },
            error: function (data, status) {
                console.log("Something went wrong with deleting categories!");

            }
        });
    });

    $('#showCommentEntryButton').on('click', function (e) {
        console.log("OK");
        document.getElementById('addCommentDiv').style.display = "block";
        document.getElementById('showCommentEntryDiv').style.display = "none";
    });

    $('#cancelCommentButton').on('click', function (e) {
        document.getElementById('addCommentDiv').style.display = "none";
        document.getElementById('showCommentEntryDiv').style.display = "block";

        $('#titleCreate').val('');
        $('#authorCreate').val('');
        $('#titleCreate').val('');
        $('#commentCreate').val('');
        $('#emailCreate').val('');
    });

    $('#addCommentButton').on('click', function (e) {

        e.preventDefault();

        var d = new Date();

        var myComment = {
            blogId: $('#blogId').val(),
            author: $('#authorCreate').val(),
            title: $('#titleCreate').val(),
            comment: $('#commentCreate').val(),
            email: $('#emailCreate').val(),
        };

        var fieldNames = Object.keys(myComment);
        $.each(fieldNames, function (index, fieldName) {
            $('#add-' + fieldName + '-validation-errors').text('');
        });
        var myCommentData = JSON.stringify(myComment);
        $.ajax({
            url: contextRoot + "/comment",
            type: "POST",
            data: myCommentData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                $('#titleCreate').val('');
                $('#authorCreate').val('');
                $('#titleCreate').val('');
                $('#commentCreate').val('');
                $('#emailCreate').val('');

                document.getElementById('addCommentDiv').style.display = "none";

                location.reload();
            },
            error: function (data, status) {

                var errors = data.responseJSON.errors;

                $.each(errors, function (index, error) {
                    var fieldNames = Object.keys(myComment);
                    $.each(fieldNames, function (index, fieldName) {

                        if (error.fieldName == fieldName) {
                            $('#add-' + fieldName + '-validation-errors').append(error.fieldName + ": " + error.message + "<br />");
                        }
                    });
                });
            }

        });


    });
});
