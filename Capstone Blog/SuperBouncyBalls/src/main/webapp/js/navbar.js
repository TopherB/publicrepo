/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    window.onclick = function (event) {

        if (event.target.matches('.dropbtn')) {

            $.ajax({
                url: contextRoot + "/static/list",
                type: "GET",
                dataType: "json",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Accept", "application/json")
                },
                success: function (data, status) {

                    $.each(data, function (key, value) {
                        var dropdownMenu = '<a href="' + contextRoot + '/static/' + value.id + '">' + value.title + '</a>';

                        $('#myDropdown').append($(dropdownMenu));
                    });
                },
                error: function (data, status) {

                    alert("STATIC PAGES NOT FOUND");
                }
            }).done(function () {
                document.getElementById("myDropdown").classList.toggle("slow");
            });
            
            $('#myDropdown').empty();
        }

        if (!event.target.matches('.dropbtn')) {

            $('#myDropdown').empty();

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
});