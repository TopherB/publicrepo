/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $('#showUserEntryButton').on('click', function (e) {
        document.getElementById('addUserDiv').style.display = "block";
        document.getElementById('showUserEntryDiv').style.display = "none";
    });

    $('#cancelUserButton').on('click', function (e) {
        document.getElementById('addUserDiv').style.display = "none";
        document.getElementById('showUserEntryDiv').style.display = "block";

        $('#userNameCreate').val('');
        $('#authorityCreate').val('Administrator');
    });

    $('#addUserButton').on('click', function (e) {

        e.preventDefault();
        var myUser = {
            userName: $('#userNameCreate').val(),
            authority: $('#authorityCreate').val(),
            password: $('#passwordCreate').val()
        };

        var fieldNames = Object.keys(myUser);
        $.each(fieldNames, function (index, fieldName) {
            $('#add-' + fieldName + '-validation-errors').text('');
        });
        var myUserData = JSON.stringify(myUser);
        console.log(myUserData);
        $.ajax({
            url: contextRoot + "/user",
            type: "POST",
            data: myUserData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                $('#userNameCreate').val('');
                $('#authorityCreate').val('Administrator');
                $('#passwordCreate').val('');

                document.getElementById('addUserDiv').style.display = "none";

                location.reload();
            },
            error: function (data, status) {

                var errors = data.responseJSON.errors;

                $.each(errors, function (index, error) {
                    var fieldNames = Object.keys(myUser);
                    $.each(fieldNames, function (index, fieldName) {

                        if (error.fieldName == fieldName) {
                            $('#add-' + fieldName + '-validation-errors').append(error.fieldName + ": " + error.message + "<br />");
                        }
                    });
                });
            }
        });
    });

    $('#cancelEditUserButton').on('click', function (e) {
        history.go(-1);
    });

    $('#deleteUserModal').on('shown.bs.modal', function (e) {

        var link = $(e.relatedTarget);
        var userId = link.data('user-id');
        $('#deleteUserId').val(userId);
    });

    $('#deleteUserButton').on('click', function (e) {

        e.preventDefault();
        var user = {
            id: $('#deleteUserId').val()
        };
        var userData = JSON.stringify(user);
        $.ajax({
            url: contextRoot + "/user/" + user.id,
            type: "DELETE",
            data: userData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                location.reload();
            },
            error: function (data, status) {
                console.log("ERROR");
            }
        });
    });
});