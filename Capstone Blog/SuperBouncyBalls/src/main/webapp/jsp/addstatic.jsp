<%-- 
    Document   : addstatic
    Created on : Oct 12, 2016, 6:02:14 PM
    Author     : apprentice
--%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://paginationtag.miin.com" prefix="pagination-tag"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({
                selector: 'textarea',
                height: 400,
                theme: 'modern',
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'table contextmenu directionality emoticons template paste textcolor'
                ],
                toolbar: [
                    'undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | outdent indent',
                    'bullist numlist | print preview forecolor backcolor emoticons | nonbreaking'
                ],
                nonbreaking_force_tab:true
                
            });
        </script>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <title>Add new Page</title>
    </head>
    <body>
        <jsp:include page="navbar.jsp" />   
        <div class="container" >
            <div class="col-sm-9" style="background-color:lightgray;">

                <h3>Add New Static Page</h3>
                <sf:form>
                    <table width="100%">
                        <tr>
                            <td>Page Name:  </td><td><input class="form-control" type="text" path="title" id="titleCreate" placeholder="Title" /><div id="add-title-validation-errors"></div><br/></td>
                        </tr>
                        <tr>
                            <td valign="top">Page Content: </td><td><textarea class="form-control" path="pageContent" id="pageContentCreate" name="pageContent" cols="50" rows="15" ></textarea><div id="add-pageContent-validation-errors"></div><br/></td>
                        </tr>
                        <tr>
                            <td>Please choose category/categories</td>
                            <td><select class="form-control selectpicker" multiple id="categorySelect">
                                    <c:forEach items="${allCategories}" var="category">
                                        <option value="${category.id}">${category.categoryName}</option>
                                    </c:forEach>
                                </select></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div class="col-sm-7"><input type="text" class="form-control" id="newCategory" placeholder="New Category"></input></div>
                                <div class="col-sm-2"><button class="btn btn-default" id="addCategoryButton">Add</button></div>
                                <div class="col-sm-3"><button class="btn" id="deleteCategoryModalButton" data-toggle="modal" data-target="#deleteCategoryModal">Delete Selected</button></div>
                            </td>
                        </tr>
                        <tr>
                            <td>Please choose post date: </td><td><input class="form-control" type="date" path="startDate" id="startDateCreate" placeholder="mm-dd-yyyy" /><div id="add-startDate-validation-errors"></div><br/></td>
                        </tr>
                        <tr>
                            <td>Please choose expiry date (optional): </td><td><input class="form-control" type="date" path="endDate" id="endDateCreate" placeholder="mm-dd-yyyy" /><div id="add-endDate-validation-errors"></div><br/></td>
                        </tr>
                        <tr>
                            <td><input id="addPageButton" type="button" value="Save" /></td><td align="right"><input id="cancelPageButton" type="button" value="Cancel" /></td>
                        </tr>
                    </table>
                </sf:form>
                </br>
                </br>
                <div class="modal fade" id="deleteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel2">Delete Categories</h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" id="deleteBlogId"/>
                                <p>Are you sure you want to delete the currently selected category/categories? This will remove them from all currently associated blogs.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteCategoryButton">Delete Categories</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="sidebar.jsp" />
        </div> 
        <script> var contextRoot = "${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/js/admin.js" ></script>   
    </body>
</html>
