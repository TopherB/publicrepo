<%-- 
    Document   : admin
    Created on : Oct 6, 2016, 10:03:47 AM
    Author     : crystal
--%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://paginationtag.miin.com" prefix="pagination-tag"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({
                selector: 'textarea',
                height: 400,
                theme: 'modern',
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'table contextmenu directionality emoticons template paste textcolor'
                ],
                toolbar: [
                    'undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | outdent indent',
                    'bullist numlist | print preview forecolor backcolor emoticons'
                ],
                nonbreaking_force_tab: true

            });
        </script>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <title>Admin Page</title>
    </head>
    <body>
        <jsp:include page="navbar.jsp" />   
        <div class="container">
            <div class="col-sm-9" style="background-color:lightgray;">
                <div id="addBlogDiv"  style="display:none;">                    
                    <h3>Add New Blog Post</h3>
                    <sf:form commandName="order">
                        <table width="100%">
                            <c:set var="enableVar" value="0" />
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <c:set var="enableVar" value="1" />
                            </sec:authorize>
                            <input type="hidden" id="enabledCreate" value="${enableVar}" />

                            <tr>
                                <td>Title:  </td><td><input class="form-control" type="text" path="title" id="titleCreate" placeholder="Title" /><div id="add-title-validation-errors"></div><br/></td>
                            </tr>
                            <tr>
                                <td valign="top">Content: </td><td><textarea class="form-control" path="content" id="contentCreate" name="content" cols="50" rows="15" ></textarea><div id="add-content-validation-errors"></div><br/></td>
                            </tr>
                            <tr>
                                <td>Please choose category/categories</td>
                                <td><select class="form-control selectpicker" multiple id="categorySelect">
                                        <c:forEach items="${allCategories}" var="category">
                                            <option value="${category.id}">${category.categoryName}</option>
                                        </c:forEach>
                                    </select></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div class="col-sm-7"><input type="text" class="form-control" id="newCategory" placeholder="New Category"></input></div>
                                    <div class="col-sm-2"><button class="btn btn-default" id="addCategoryButton">Add</button></div>
                                    <div class="col-sm-3"><button class="btn" id="deleteCategoryModalButton" data-toggle="modal" data-target="#deleteCategoryModal">Delete Selected</button></div>
                                </td>
                            </tr>
                            <tr>
                                <td>Please choose post date: </td><td><input class="form-control" type="date" path="startDate" id="startDateCreate" value="" /><div id="add-startDate-validation-errors"></div><br/></td>
                            </tr>
                            <tr>
                                <td>Please choose expiry date (optional): </td><td><input class="form-control" type="date" path="endDate" id="endDateCreate" placeholder="mm-dd-yyyy" /><div id="add-endDate-validation-errors"></div><br/></td>
                            </tr>
                            <tr>
                                <td><input id="addBlogButton" type="button" value="Save" /></td><td align="right"><input id="cancelBlogButton" type="button" value="Cancel" /></td>
                            </tr>

                        </table>
                    </sf:form>
                </div>
                </br>
                <div id="showBlogEntryDiv"  style="display:block;"> 
                    <button id="showBlogEntryButton" type="button" class="btn btn-default" >Add a new blog entry</button>
                </div>
                </br>
                <c:forEach items="${blogs}" var="blog">
                    <div id="blog-row-${blog.id}">
                        <div>
                            <a href="${pageContext.request.contextPath}/blog/${blog.id}">${blog.title}</a> ${blog.startDate}
                            <a href="${pageContext.request.contextPath}/blog/editblog/${blog.id}">Edit</a>
                            <a data-toggle="modal" data-target="#deleteBlogModal" data-blog-id="${blog.id}">Delete</a>
                        </div>
                        <div id="blog_content_${blog.id}">
                            ${blog.content}
                        </div>
                    </div>
                    </br>
                </c:forEach>      
                <div class="pagination">
                    <ul>
                        <li><c:forEach begin="${startpage}" end="${endpage}" var="p">
                                <a href="<c:url value="blog/list" ><c:param name="page" value="${p}"/>${p}</c:url>">${p}</a>
                            </c:forEach></li>
                    </ul>
                </div>
                <div class="modal fade" id="deleteBlogModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Delete Blog</h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" id="deleteBlogId"/>
                                <c:set var="enableDelVar" value="0" />
                                <sec:authorize access="hasRole('ROLE_ADMIN')">
                                    <c:set var="enableDelVar" value="1" />
                                </sec:authorize>
                                <input type="hidden" id="deleteBlogEnabled" value="${enableDelVar}" />
                                <p>Are you sure you want to delete this blog?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteBlogButton">Delete Blog</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="deleteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel2">Delete Categories</h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" id="deleteBlogId"/>
                                <p>Are you sure you want to delete the currently selected category/categories? This will remove them from all currently associated blogs.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteCategoryButton">Delete Categories</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="sidebar.jsp" />
        </div>
        <script> var contextRoot = "${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/js/admin.js" ></script>    
    </body>
</html>
