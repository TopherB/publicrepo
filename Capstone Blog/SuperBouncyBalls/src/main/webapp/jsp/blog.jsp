<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <title>Blog Post</title>
    </head>
    <body>
        <jsp:include page="navbar.jsp" />
        <div class="container">
            <div class="col-sm-9" style="background-color:lightgray;">
                <div class="row">
                    <!--<div class="col-sm-9" style="background-color:lightgray;">-->
                        <h1 id="blogTitle">${blog.title}</h1>
                        <hr>
                        <p id="blogStartDate">Posted on ${blog.startDate}</p>
                        <hr>
                        <div id="blogContent">
                            ${blog.content}
                        </div>
                        <hr>
                        <p id="blogCategories">
                            Categories: <c:forEach items="${blog.categories}" var="category">${category.categoryName} </c:forEach>
                            </p>
                            <hr>
                            </br></br></br>
                            <div id="commments">
                                <p>
                                    <b>Comments:</b>
                                </p>
                            <sf:form commandName="comment">
                                <div id="addCommentDiv"  style="display:none;">  
                                    <table width="100%">
                                        Add New Comment
                                        <input type="hidden" id="blogId" value="${blog.id}" />
                                        <tr>
                                            <td>Your name: </td><td><input class="form-control" type="text" path="author" id="authorCreate" placeholder="Name" /><div id="add-author-validation-errors"></div><br/></td>
                                        </tr>
                                        <tr>
                                            <td>Headline: </td><td><input class="form-control" type="text" path="title" id="titleCreate" placeholder="Headline" /><div id="add-title-validation-errors"></div><br/></td>
                                        </tr>
                                        <tr>
                                            <td>Comment: </td><td><input class="form-control" type="text" path="comment" id="commentCreate" placeholder="Comment" /><div id="add-comment-validation-errors"></div><br/></td>
                                        </tr>
                                        <tr>
                                            <td>Email: </td><td><input class="form-control" type="text" path="email" id="emailCreate" placeholder="Email" /><div id="add-email-validation-errors"></div><br/></td>
                                        </tr>
                                        <tr>
                                            <td><input id="addCommentButton" type="button" value="Save" /></td><td align="right"><input id="cancelCommentButton" type="button" value="Cancel" /></td>
                                        </tr>
                                    </table>
                                </div>
                                </br>
                                <div id="showCommentEntryDiv"  style="display:block;"> 
                                    <button id="showCommentEntryButton" type="button" class="btn btn-default" >Add a new comment</button>
                                </div>
                                </br>
                            </sf:form>
                        </div>
                        <c:forEach items="${comments}" var="comment">
                            <div id="comment-row-${comment.id}">
                                <p>
                                    ${comment.author}, ${comment.date}, <sec:authorize access="hasRole('ROLE_USER')">${comment.email}, 
                                        <a data-toggle="modal" data-target="#deleteCommentModal" data-comment-id="${comment.id}">Delete</a></sec:authorize>
                                    </p>
                                    <p>
                                    ${comment.title}
                                </p>
                                <p>
                                    ${comment.comment}
                                </p>
                            </div>
                            </br></br>
                        </c:forEach>
<!--                    </div>-->
                </div>
                <div class="modal fade" id="deleteCommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Delete Blog</h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" id="deleteCommentId"/>
                                <p>Are you sure you want to delete this comment?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteCommentButton">Delete Comment</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <jsp:include page="sidebar.jsp" />
    </div> <!--container div-->
    <script> var contextRoot = "${pageContext.request.contextPath}";</script>
    <script src="${pageContext.request.contextPath}/js/admin.js"></script>
</body>
</html>
