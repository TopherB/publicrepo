<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({
                selector: 'textarea',
                height: 400,
                theme: 'modern',
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'table contextmenu directionality emoticons template paste textcolor'
                ],
                toolbar: [
                    'undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | outdent indent',
                    'bullist numlist | print preview forecolor backcolor emoticons'
                ],
                nonbreaking_force_tab: true

            });
        </script>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <title>Edit Blog</title>
    </head>
    <body>
        <jsp:include page="navbar.jsp" /> 

        <div class="container">

            <div class="row">
                <div class="col-sm-9" style="background-color:lightgray;" >
                    <sf:form commandName="blog" action="${pageContext.request.contextPath}/blog/editblog/${blog.id}" method="POST">
                        <table width="100%">
                            <input type="hidden" name="referer" value="${referer}"/>
                            <input type="hidden" name="id" value="${blog.id}"/>
                            <input type="hidden" name="pending.id" value="${blog.pending.id}"/>
                            <input type="hidden" name="pending.changeType" value="${blog.pending.changeType}"/>
                            <input type="hidden" name="pending.tableType" value="${blog.pending.tableType}"/>
                            <input type="hidden" name="pending.tempId" value="${blog.pending.tempId}"/>
                            <input type="hidden" name="pending.objectId" value="${blog.pending.objectId}"/>

                            <c:set var="enableVar" value="0" />
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <c:set var="enableVar" value="1" />
                            </sec:authorize>
                            <input type="hidden" name="enabled" value="${enableVar}" />
                            <tr>
                                <td>Title:  </td><td><sf:input class="form-control" type="text" path="title" id="editTitle" value="${blog.title}" /><sf:errors path="title" class="alert-danger"/><br/></td>
                            </tr>
                            <tr>
                                <td valign="top">Content: </td><td><sf:textarea path="content" id="contentEdit" cols="50" rows="15" ></sf:textarea><sf:errors path="content" class="alert-danger"/> <br/></td>
                                </tr>
                                <tr>
                                    <td>Please choose category/categories</td>
                                    <td><sf:select class="form-control selectpicker" path="categories" multiple="multiple" id="categorySelect">
                                        <c:forEach items="${blog.categories}" var="blogCategory">
                                            <sf:option value="${blogCategory.id}" selected="selected" >${blogCategory.categoryName}</sf:option>
                                        </c:forEach>
                                        <c:forEach items="${allCategories}" var="category">
                                            <sf:option value="${category.id}">${category.categoryName}</sf:option>
                                        </c:forEach>
                                    </sf:select></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div class="col-sm-7"><input type="text" class="form-control" id="newCategory" placeholder="New Category"></input></div>
                                    <div class="col-sm-2"><button class="btn btn-default" id="addCategoryButton">Add</button></div>
                                    <div class="col-sm-3"><button class="btn" id="deleteCategoryModalButton" data-toggle="modal" data-target="#deleteCategoryModal">Delete Selected</button></div>

                                </td>
                            </tr>
                            <tr>
                                <td>Please choose post date: </td><td><fmt:formatDate value="${blog.startDate}" pattern="yyyy-MM-dd" var="startDate" /><sf:input class="form-control" type="date" path="startDate" id="editStartDate" value="${startDate}" name="startDate"/><sf:errors path="startDate" class="alert-danger"/><br/></td>
                            </tr>
                            <tr>
                                <td>Please choose expiry date (optional): </td><td><fmt:formatDate value="${blog.endDate}" pattern="yyyy-MM-dd" var="endDate" /><sf:input class="form-control" type="date" path="endDate" id="editEndDate" value="${blog.endDate}" name="endDate" /><sf:errors path="endDate" class="alert-danger"/><br/></td>
                            </tr>
                            <tr>
                                <td><input id="editBlogButton" type="submit" value="Save" /></td>
                                <td><input id="cancelEditBlogButton" type="button" value="Cancel" /></td>
                            </tr>
                        </table>
                    </sf:form>
                </div>

                <jsp:include page="sidebar.jsp" />
            </div>






        </div> <!--container div-->

        <script src="${pageContext.request.contextPath}/js/admin.js"></script>
    </body>
</html>
