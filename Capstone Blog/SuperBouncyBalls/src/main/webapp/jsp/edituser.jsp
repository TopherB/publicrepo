<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <title>Edit User</title>
    </head>
    <body>
        <jsp:include page="navbar.jsp" />
        <div class="container">
            <div class="row">
                <div class="col-sm-9" style="background-color:lightgray;" >
                    <sf:form commandName="user" action="${pageContext.request.contextPath}/user/edituser/${user.id}" method="POST">
                        <table width="100%">
                            <input type="hidden" name="referer" value="${referer}"/>
                            <input type="hidden" name="id" value="${user.id}"/>
                            <tr>
                                <td>Name: </td><td><sf:input class="form-control" type="text" path="userName" id="editUserName" value="${user.userName}" /><sf:errors path="userName" class="alert-danger"/><br/></td>
                            </tr>
                            <tr>
                                <td>Authority: </td><td><select name="authority" id="editAuthority">
                                        <option value="Administrator">Administrator</option>
                                        <option value="User">User</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td>Password: </td><td><sf:input class="form-control" type="text" path="password" id="editPassword" value="${user.password}" /><sf:errors path="password" class="alert-danger"/><br/></td>                            
                            </tr>
                            <tr>
                                <td><input id="editUserButton" type="submit" value="Save" /></td>
                                <td><input id="cancelEditUserButton" type="button" value="Cancel" /></td>
                            </tr>
                        </table>
                    </sf:form>
                </div>
                <jsp:include page="sidebar.jsp" />
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/user.js"></script>
    </body>
</html>
