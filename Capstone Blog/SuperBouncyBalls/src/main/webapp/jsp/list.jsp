<%-- 
    Document   : list
    Created on : Oct 8, 2016, 4:04:02 PM
    Author     : apprentice
--%>

<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://paginationtag.miin.com" prefix="pagination-tag"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({selector: 'textarea'});</script>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <title>Admin Page</title>
        <style>
            hr{
                margin: 0px;
            }
            h2 a:hover{
                text-decoration: none;
            }
            h2 a:link{
                color:black;
            }
            h2 a:visited{
                color:black;
            }
        </style>
    </head>

    <body>
        <jsp:include page="navbar.jsp" />   

        <div class="container">

            <div class="col-sm-9" style="background-color:lightgray;">

                <c:forEach items="${blogs}" var="blog">
                    <div id="blog-row-${blog.id}">
                        <br/><hr/><br/>
                        <div>                            
                            <sec:authorize access="hasRole('ROLE_USER')">
                                <a href="${pageContext.request.contextPath}/blog/editblog/${blog.id}">Edit</a>
                                <a data-toggle="modal" data-target="#deleteBlogModal" data-blog-id="${blog.id}">Delete</a>
                            </sec:authorize>
                            <h2><a href="${pageContext.request.contextPath}/blog/${blog.id}">${blog.title}</a></h2> 
                                ${blog.startDate}
                            <br/><br/>
                        </div>
                        <div id="blog_content_${blog.id}">
                            ${blog.content}
                        </div>
                        <a href="${pageContext.request.contextPath}/blog/${blog.id}">More...</a>
                    </div>
                    </br>
                </c:forEach>       
                <div class="pagination">
                    <ul>
                        <li><c:forEach begin="${startpage}" end="${endpage}" var="p">
                                <c:if test="${p == 1}">
                                    <a href="${pageContext.request.contextPath}/admin">${p}</a>
                                </c:if>
                                <c:if test="${p != 1}">    
                                    <a href="<c:url value="list" ><c:param name="page" value="${p}"/>${p}</c:url>">${p}</a>
                                </c:if>
                            </c:forEach></li>
                    </ul>
                </div>

                <div class="modal fade" id="deleteBlogModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Delete Blog</h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" id="deleteBlogId"/>
                                <c:set var="enableDelVar" value="0" />
                                <sec:authorize access="hasRole('ROLE_ADMIN')">
                                    <c:set var="enableDelVar" value="1" />
                                </sec:authorize>
                                <input type="hidden" id="deleteBlogEnabled" value="${enableDelVar}" />
                                <p>Are you sure you want to delete this blog?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteBlogButton">Delete Blog</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="sidebar.jsp" />
        </div>
        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>
        <script src="${pageContext.request.contextPath}/js/admin.js" ></script>
    </body>
</html>
