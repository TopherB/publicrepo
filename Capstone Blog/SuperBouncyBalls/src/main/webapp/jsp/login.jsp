<%-- 
    Document   : login
    Created on : Oct 15, 2016, 12:02:09 PM
    Author     : apprentice
--%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://paginationtag.miin.com" prefix="pagination-tag"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({selector: 'textarea'});</script>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <title>Login Page</title>
    </head>
    <body>
        <jsp:include page="navbar.jsp" />   
        <div class="container">
            <div class="col-sm-9" style="background-color:lightgray;">
                <c:if test="${param.login_error == 1}">
                    <h2>Error logging in. Please retry.</h2>
                </c:if>
                <form action="${pageContext.request.contextPath}/j_spring_security_check" method="POST">
                    <div>
                        Username: <input type="text" name="username" />
                    </div>
                    </br>
                    <div>
                        Password: <input type="password" name="password" />
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </br>
                    <div>
                        <input type="submit" value="Login" />
                    </div>
                </form>
            </div>
            <jsp:include page="sidebar.jsp" />
        </div>
        <script> var contextRoot = "${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/js/admin.js" ></script>   
    </body>
</html>

