<%-- 
    Document   : navbar
    Created on : Oct 7, 2016, 9:26:45 AM
    Author     : apprentice
--%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            .dropbtn {
                background-color: #4CAF50;
                color: white;
                padding: 16px;
                border: none;
                cursor: pointer;
            }

            .dropbtn:hover, .dropbtn:focus {
                background-color: #3e8e41;
            }

            .dropdown {
                position: relative;
                display: inline-block;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                z-index:1;
                background-color: #f9f9f9;
                min-width: 160px;
                overflow: auto;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

            .dropdown a:hover {background-color: #f1f1f1}

            .show {display:block;}
            
            
        </style>
    </head>
    <script>
        function myFunction() {
            document.getElementById("myDropdown").classList.toggle("show");
        }

        window.onclick = function (event) {
            if (!event.target.matches('.dropbtn')) {

                var dropdowns = document.getElementsByClassName("dropdown-content");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains('show')) {
                        openDropdown.classList.remove('show');
                    }
                }
            }
        }
    </script>
    <body>
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="${pageContext.request.contextPath}"><b>Super Bouncy Balls</b></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="${pageContext.request.contextPath}">Home</a>
                        </li>
                        <li>
                            <a href="mailto:owner@superbouncyballs.com?Subject=Hello%20" target="_top">Contact Us</a>
                        </li>
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <li>
                                <a href="${pageContext.request.contextPath}/admin">Add/Edit Blogs</a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li>
                                <a href="${pageContext.request.contextPath}/pending">Pending</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/static/addstatic">Add a new page</a>
                            </li>
                        </sec:authorize>

                        <li>
                            <div class="dropdown">
                                <button onclick="myFunction()" class="dropbtn" id="staticPagesButton">Static Pages</button>
                                <div id="myDropdown" class="dropdown-content">                                                     
                                </div>
                            </div>
                        </li>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li>
                                <a href="${pageContext.request.contextPath}/user/users">Users</a>
                            </li>
                        </sec:authorize>
                        <li>
                            <sec:authorize access="hasRole('ROLE_USER')">
                                <a href="${pageContext.request.contextPath}/logout">Log out (logged in as <sec:authentication property="principal.username" />)</a>
                            </sec:authorize>
                            <sec:authorize access="!hasRole('ROLE_USER')">
                                <a href="${pageContext.request.contextPath}/login">Log in</a>
                            </sec:authorize>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <script> var contextRoot = "${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js" ></script>
        <script src="${pageContext.request.contextPath}/js/navbar.js" ></script>   
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
