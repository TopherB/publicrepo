<%-- 
    Document   : pending
    Created on : Oct 19, 2016, 10:21:43 AM
    Author     : apprentice
--%>

<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://paginationtag.miin.com" prefix="pagination-tag"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home</title>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({selector: 'textarea'});</script>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <style>
            hr{
                margin: 0px;
            }
            h1 a:hover{
                text-decoration: none;
            }
            h1 a:link{
                color:black;
            }
            h1 a:visited{
                color:black;
            }
        </style>
    </head>
    <body>
        <jsp:include page="navbar.jsp" />
        <div class="container">
            <div class="col-sm-9" style="background-color:lightgray;">
                <h3>Pending Changes</h3>
                <div class="table-responsive">
                    <table class="table table-hover " style="width: 90%" id="pendingTable">
                        <tr>
                            <th>Title</th>
                            <th></th>
                            <th>Change Type</th>
                        </tr>
                        <c:forEach items="${pendingList}" var="blog">
                            <tr id="blog-row-${blog.id}">
                                <td>${blog.title}</td>
                                <c:set var="changeVar" value="${blog.pending.changeType}"/>
                                <c:if test="${changeVar == 'add'}">
                                    <td><a href="${pageContext.request.contextPath}/blog/editblog/${blog.pending.objectId}">Approve Added Blog</a></td>
                                </c:if>
                                <c:if test="${changeVar == 'edit'}">
                                    <td><a href="${pageContext.request.contextPath}/blog/editblog/${blog.pending.objectId}">Approve Edited Blog</a></td>
                                </c:if>
                                <c:if test="${changeVar == 'delete'}">
                                    <td><a data-toggle="modal" data-target="#deleteBlogModal" data-blog-id="${blog.id}">Approve Deletion</a></td>
                                </c:if>
                                <td>${blog.pending.changeType}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal fade" id="deleteBlogModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Delete Blog</h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" id="deleteBlogId"/>
                                <c:set var="enableDelVar" value="1" />
                                <sec:authorize access="hasRole('ROLE_ADMIN')">
                                    <c:set var="enableDelVar" value="1" />
                                </sec:authorize>
                                <input type="hidden" id="deleteBlogEnabled" value="${enableDelVar}" />
                                <p>Are you sure you want to delete this blog?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteBlogButton">Delete Blog</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="sidebar.jsp" />
        </div>
        <script> var contextRoot = "${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/js/admin.js" ></script> 
    </body>
</html>
