<%-- 
    Document   : sidebar
    Created on : Oct 7, 2016, 9:26:56 AM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">

        <title>SideBar</title>
    </head>
    <body>
        <div class="col-sm-2" style="float:right;padding:0px;">
            <nav class="navbar navbar-default">
                <hr>
                <h2 style="text-align: center">Hashtags</h2>
                <c:forEach items="${firstLetter}" var="fl">

                    <ul class="nav nav-pills nav-stacked">
                        <li class="dropdown text-center">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span> ${fl}</a>

                            <ul class="dropdown-menu">
                                <c:set var="xyz" value="${fl}"/>
                                <c:forEach items="${hashtags}" var="hashtag">
                                    <c:if test="${hashtag.hashtagName.substring(0, 1).equalsIgnoreCase(xyz)}">
                                        <li class="text-center">
                                            <a href="${pageContext.request.contextPath}/hashtag/${hashtag.id}">${hashtag.hashtagName}</a>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </ul>

                        </li>
                    </ul>
                </c:forEach>
                <h2 style="text-align: center">Blog Categories</h2>
                <c:forEach items="${flCategories}" var="flc">
                    
                    <ul class="nav nav-pills nav-stacked">
                        <li class="dropdown text-center">
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span> ${flc}</a> 
                           <ul class="dropdown-menu">
                                <c:set var="abc" value="${flc}"/>
                                <c:forEach items="${categories}" var="category">
                                    <c:if test="${category.categoryName.substring(0, 1).equalsIgnoreCase(abc)}">
                                        <li class="text-center">
                                            <a href="${pageContext.request.contextPath}/category/${category.id}">${category.categoryName}</a>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </li>                       
                    </ul> 
                </c:forEach>
                <h2 style="text-align: center">Other Categories</h2>
                <c:forEach items="${otherFlCategories}" var="ofl">

                    <ul class="nav nav-pills nav-stacked">
                        <li class="dropdown text-center">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span> ${ofl}</a>

                            <ul class="dropdown-menu">
                                <c:set var="tuv" value="${ofl}"/>
                                <c:forEach items="${otherCategories}" var="oc">
                                    <c:if test="${oc.categoryName.substring(0, 1).equalsIgnoreCase(tuv)}">
                                        <li class="text-center">
                                            <a href="${pageContext.request.contextPath}/static/category/${oc.id}">${oc.categoryName}</a>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </ul>

                        </li>
                    </ul>
                </c:forEach>
            </nav>
        </div>
    </body>
</html>

