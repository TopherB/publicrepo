<%-- 
    Document   : static
    Created on : Oct 7, 2016, 9:25:54 AM
    Author     : apprentice
--%>

<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://paginationtag.miin.com" prefix="pagination-tag"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({selector: 'textarea'});</script>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <title>${staticPage.title}</title>
    </head>
    <body>
        <jsp:include page="navbar.jsp" />
        <div class="container">
            <div class="col-sm-9" style="background-color:lightgray;">
                <div id="staticPage_content_${staticPage.id}">
                    <h1 id="staticPageTitle">${staticPage.title}</h1>
                        <hr>
                        <p id="staticPageStartDate">Posted on ${staticPage.startDate}</p>
                        <hr>
                        <div id="staticPageContent">
                            ${staticPage.pageContent}
                        </div>
                        <hr>
                        <p id="staticPageCategories">
                            Categories: <c:forEach items="${staticPage.categories}" var="category">${category.categoryName} </c:forEach>
                        </p>
                </div>
                <div>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <a href="${pageContext.request.contextPath}/static/editstatic/${staticPage.id}">Edit</a>
                        <a data-toggle="modal" data-target="#deleteStaticPageModal" data-staticPage-id="${staticPage.id}">Delete</a>
                    </sec:authorize>
                </div>
            </div>
            <div class="modal fade" id="deleteStaticPageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete Static Page</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="deleteStaticPageId"/>
                            <p>Are you sure you want to delete this page?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteStaticPageButton">Delete Static Page</button>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="sidebar.jsp" />
        </div>
        <script> var contextRoot = "${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/js/admin.js" ></script>   
    </body>
</html>

