<%-- 
    Document   : staticlist
    Created on : Oct 19, 2016, 11:49:39 PM
    Author     : crystal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://paginationtag.miin.com" prefix="pagination-tag"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({selector: 'textarea'});</script>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <title>List Page</title>
    </head>

    <body>
        <jsp:include page="navbar.jsp" />   

        <div class="container">

            <div class="col-sm-9" style="background-color:lightgray;">

                <c:forEach items="${modStatic}" var="staticPage">
                    <div id="staticPage_content_${staticPage.id}">
                        <h1 id="staticPageTitle">${staticPage.title}</h1>
                        <hr>
                        <p id="staticPageStartDate">Posted on ${staticPage.startDate}</p>
                        <hr>
                        <div id="staticPageContent">
                            ${staticPage.pageContent}                            
                        </div>
                        <a href="${pageContext.request.contextPath}/static/${staticPage.id}">More...</a>
                        <hr>
                    </div>
                    <div>
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <a href="${pageContext.request.contextPath}/static/editstatic/${staticPage.id}">Edit</a>
                            <a data-toggle="modal" data-target="#deleteStaticPageModal" data-staticPage-id="${staticPage.id}">Delete</a>
                        </sec:authorize>
                    </div>
                    </br>
                </c:forEach>       
                <div class="pagination">
                    <ul>
                        <li><c:forEach begin="${startpage}" end="${endpage}" var="p">
                                <a href="<c:url value="/static/${categoryId}/list" ><c:param name="page" value="${p}"/>${p}</c:url>">${p}</a>
                            </c:forEach></li>
                    </ul>                    
                </div>
            </div>
            <div class="modal fade" id="deleteStaticPageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete Static Page</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="deleteStaticPageId"/>
                            <p>Are you sure you want to delete this page?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteStaticPageButton">Delete Static Page</button>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="sidebar.jsp" />
        </div>
        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>
        <script src="${pageContext.request.contextPath}/js/admin.js" ></script>
    </body>
</html>
