<%-- 
    Document   : users
    Created on : Oct 16, 2016, 11:07:26 AM
    Author     : apprentice
--%>

<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://paginationtag.miin.com" prefix="pagination-tag"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <title>Users</title>
    </head>
    <body>
        <jsp:include page="navbar.jsp" />   
        <div class="container">
            <div class="col-sm-9" style="background-color:lightgray;">
                <div id="addUserDiv"  style="display:none;">                    
                    <sf:form commandName="user">
                        <table width="100%">
                            <tr>
                                <td>Name:  </td><td valign="bottom"><input class="form-control" type="text" path="userName" id="userNameCreate" placeholder="Name" /><div id="add-userName-validation-errors"></div><br/></td>
                            </tr>
                            <tr>
                                <td>Password:  </td><td valign="bottom"><input class="form-control" type="text" path="password" id="passwordCreate" placeholder="password" /><div id="add-password-validation-errors"></div><br/></td>
                            </tr>
                            <tr>
                                <td >Authority: </td><td valign="bottom"><select name="authority" id="authorityCreate">
                                        <option value="Administrator">Administrator</option>
                                        <option value="User">User</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td><input id="addUserButton" type="button" value="Save" /></td><td align="right"><input id="cancelUserButton" type="button" value="Cancel" /></td>
                            </tr>
                        </table>
                    </sf:form>
                </div>
                </br>
                <div id="showUserEntryDiv"  style="display:block;"> 
                    <button id="showUserEntryButton" type="button" class="btn btn-default" >Add a new User</button>
                </div>
                </br>
                <div class="table-responsive">
                    <table class="table table-hover " style="width: 90%" id="userTable">
                        <tr>
                            <th>User name</th>
                            <th>User authority</th>
                            <th></th>
                            <th></th>
                        </tr>
                        <c:forEach items="${users}" var="user">

                            <tr id="user-row-${user.id}">
                                <td>${user.userName}</td>
                                <td>${user.authority}</td>
                                <td><a href="${pageContext.request.contextPath}/user/edituser/${user.id}">Edit</a></td>
                                <td><a data-toggle="modal" data-target="#deleteUserModal" data-user-id="${user.id}">Delete</a></td>
                            </tr>
                        </c:forEach> 
                    </table>
                </div>
                <div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit User Info</h4>
                            </div>
                            <form:form method="PUT" commandName="user">
                                <div class="modal-body">
                                    <input type="hidden" id="editId" />
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <table>
                                                <tr>
                                                    <td></br>Name: </td>
                                                    <td><div id="errorEditName" class="red"></div></br>
                                                        <input type="text" id="editName" path="userName"></td>
                                                </tr>
                                                <tr>
                                                    <td>Authority: </td><td><select name="authority" id="editAuthority">
                                                            <option value="Administrator">Administrator</option>
                                                            <option value="User">User</option>
                                                        </select></td>
                                                </tr>
                                                <tr>
                                                    <td></br>Name: </td>
                                                    <td><div id="errorEditPassword" class="red"></div></br>
                                                        <input type="text" id="editPassword" path="password"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="editUserButton">Save changes</button>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete User</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="deleteUserId"/>
                            <p>Are you sure you want to delete this user?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteUserButton">Delete User</button>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="sidebar.jsp" />
        </div>
        <script> var contextRoot = "${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/js/user.js" ></script>   
    </body>
</html>

