/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daotests;

import com.mycompany.superbouncyballs.dao.BlogDao;
import com.mycompany.superbouncyballs.dto.Blog;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class BlogDaoTests {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    BlogDao bd;
    Blog blog1 = new Blog();
    Blog blog2 = new Blog();

    Date startDate = new Date(2016, 05, 06);
    Date startDate2 = new Date(2014, 04, 24);
    Date endDate = new Date(2016, 10, 05);

    public BlogDaoTests() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");
        this.bd = ctx.getBean("blogDao", BlogDao.class);
        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        cleaner.execute("DELETE FROM blog");
    }

    @Before
    public void setUp() {
        blog1.setTitle("Test Title");
        blog1.setContent("This is a bunch of content or something like that");
        blog1.setStartDate(startDate);
        blog1.setEndDate(endDate);
        
        blog2.setTitle("title");
        blog2.setContent("More stuff");
        blog2.setStartDate(startDate2);
        blog2.setEndDate(endDate);

    }

    @After
    public void tearDown() {
 
    }

    @Test
    public void testCreateBlog() {
        Blog addBlog = bd.create(blog1);

        try {
            Blog addedBlog = bd.read(addBlog.getId());
            Assert.assertNotNull(addedBlog);
        } catch (EmptyResultDataAccessException ex) {

        }

    }

    @Test
    public void testReadBlog(){
        Blog expected = bd.create(blog1);
        Blog result = bd.read(expected.getId());
        
        Assert.assertEquals(expected.getContent(), result.getContent());
        
    }
    
    @Test
    public void testUpdateBlog() {
        Blog editBlog = bd.create(blog1);
        editBlog.setContent("Updated content");

        bd.update(editBlog);

        Blog result = bd.read(editBlog.getId());
        Assert.assertEquals("Updated content", result.getContent());
    }

    @Test
    public void testDeleteBlog() {

        Blog addBlog = bd.create(blog1);

        bd.delete(addBlog);

        try {
            Blog deletedBlog = bd.read(addBlog.getId());
            Assert.assertNull(deletedBlog);
        } catch (EmptyResultDataAccessException ex) {

        }
    }
    
    @Test
    public void testGetBlogByRecentDate() {
        
        Blog addBlog1 = bd.create(blog1);
        Blog addBlog2 = bd.create(blog2);
        
        Blog mostRecentBlog = bd.blogsByRecentDate();
        
        Assert.assertEquals(mostRecentBlog.getTitle(), addBlog1.getTitle());
    }
    
    @Test
    public void testBlogSize() {
        
        Blog addBlog1 = bd.create(blog1);
        Blog addBlog2 = bd.create(blog2);
        
        int blogSize = bd.blogSize();
        
        Assert.assertEquals(2, blogSize);
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
