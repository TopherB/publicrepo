/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daotests;

import com.mycompany.superbouncyballs.dao.CategoryDao;
import com.mycompany.superbouncyballs.dto.Category;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class CategoryDaoTests {
    
    CategoryDao cd;
    
    Category c = new Category();
    
    public CategoryDaoTests() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");
        this.cd = ctx.getBean("categoryDao", CategoryDao.class);
    }
    
    @Before
    public void setUp() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");

        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");

        cleaner.execute("DELETE FROM categories");
        
        c.setCategoryName("Marketing");
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testAddUser() {

        Category addCat = cd.create(c);

        Assert.assertNotNull(addCat);

        Assert.assertEquals("Marketing", addCat.getCategoryName());

    }
    
    @Test
    public void testDeleteUser() {

        Category addCat = cd.create(c);
        
        cd.delete(addCat);

        try {
            Category deletedCat = cd.read(addCat.getId());
            Assert.assertNull(deletedCat);
        } catch (EmptyResultDataAccessException ex) {

        }
    }
    
    @Test
    public void testEditUser() {

        Category addCat = cd.create(c);

        addCat.setCategoryName("Personal");

        cd.update(addCat);

        Assert.assertEquals("Personal", addCat.getCategoryName());
    }
    
    @Test
    public void testReadUser() {

        Category addCat = cd.create(c);

        Assert.assertNotNull(addCat);
        
        Assert.assertEquals("Marketing", addCat.getCategoryName());
                
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
