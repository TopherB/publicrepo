/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daotests;

import com.mycompany.superbouncyballs.dao.HashtagDao;
import com.mycompany.superbouncyballs.dto.Hashtag;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class HashtagDaoTests {
    
    HashtagDao h;
    
    Hashtag hash = new Hashtag();
    
    public HashtagDaoTests() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");
        this.h = ctx.getBean("hashtagDao", HashtagDao.class);
    }
    
    @Before
    public void setUp() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");

        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");

        cleaner.execute("DELETE FROM hashtags");
        
        hash.setHashtagName("#thuglifeprobz");
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testAddUser() {

        Hashtag addHash = h.create(hash);

        Assert.assertNotNull(addHash);

        Assert.assertEquals("#thuglifeprobz", addHash.getHashtagName());

    }
    
    @Test
    public void testDeleteUser() {

        Hashtag addHash = h.create(hash);

        h.delete(addHash);

        try {
            Hashtag deletedHash = h.read(addHash.getId());
            Assert.assertNull(deletedHash);
        } catch (EmptyResultDataAccessException ex) {

        }
    }
    
    @Test
    public void testEditUser() {

        Hashtag addHash = h.create(hash);

        addHash.setHashtagName("!!!");

        h.update(addHash);

        Assert.assertEquals("!!!", addHash.getHashtagName());
    }
    
    @Test
    public void testReadUser() {

        Hashtag addHash = h.create(hash);

        Assert.assertNotNull(addHash);
        
        Assert.assertEquals("#thuglifeprobz", addHash.getHashtagName());
                
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
