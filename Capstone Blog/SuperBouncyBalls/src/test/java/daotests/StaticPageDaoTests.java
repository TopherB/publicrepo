/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daotests;

import com.mycompany.superbouncyballs.dao.StaticPageDao;
import com.mycompany.superbouncyballs.dto.StaticPage;
import java.util.Date;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class StaticPageDaoTests {
    
    StaticPageDao s;
    
    StaticPage sp = new StaticPage();
    
    Date sdate = new Date(1999, 12, 30);
    Date edate = new Date(2004, 12, 30);
    
    public StaticPageDaoTests() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");
        this.s = ctx.getBean("staticPageDao", StaticPageDao.class);
    }
    
    @Before
    public void setUp() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");

        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");

        cleaner.execute("DELETE FROM static_pages");
        
        sp.setTitle("title");
        sp.setPageContent("page content and stuff");
        sp.setStartDate(sdate);
        sp.setEndDate(edate);
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testAddUser() {

        StaticPage addPage = s.create(sp);

        Assert.assertNotNull(addPage);

        Assert.assertEquals("title", addPage.getTitle());
        Assert.assertEquals("page content and stuff", addPage.getPageContent());
        Assert.assertEquals(sdate, addPage.getStartDate());
        Assert.assertEquals(edate, addPage.getEndDate());

    }
    
    @Test
    public void testDeleteUser() {

        StaticPage addPage = s.create(sp);

        s.delete(addPage);

        try {
            StaticPage deletedPage = s.read(addPage.getId());
            Assert.assertNull(deletedPage);
        } catch (EmptyResultDataAccessException ex) {

        }
    }
    
    @Test
    public void testEditUser() {

        StaticPage addPage = s.create(sp);

        addPage.setTitle("!!!");

        s.update(addPage);

        Assert.assertEquals("!!!", addPage.getTitle());
        Assert.assertEquals("page content and stuff", addPage.getPageContent());
    }
    
    @Test
    public void testReadUser() {

        StaticPage addPage = s.create(sp);

        Assert.assertNotNull(addPage);
        
        Assert.assertEquals(sdate, addPage.getStartDate());
                
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
