/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daotests;

import com.mycompany.superbouncyballs.dao.AccessLevelDao;
import com.mycompany.superbouncyballs.dao.UserDao;
import com.mycompany.superbouncyballs.dto.AccessLevel;
import com.mycompany.superbouncyballs.dto.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class UserDaoTests {
    
    UserDao u;
    AccessLevelDao al;
    
    User user = new User();
    AccessLevel a = new AccessLevel();
    
    public UserDaoTests() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");
        this.u = ctx.getBean("userDao", UserDao.class);
        this.al = ctx.getBean("accessLevelDao", AccessLevelDao.class);
    }
    
//    @Before
//    public void setUp() {
//        
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");
//
//        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
//
//        cleaner.execute("DELETE FROM users");
//        cleaner.execute("DELETE FROM access_codes");
//        
//        a.setAccessLevel(19);
//        
//        user.setUserName("admin123");
//        user.setPassword("123456");
//        user.setAccessLevel(a);
//    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testAddUser() {

        AccessLevel addAL = al.create(a);
        User addUser = u.create(user);

        Assert.assertNotNull(addUser);

        Assert.assertEquals("admin123", addUser.getUserName());
        Assert.assertEquals("123456", addUser.getPassword());
//        Assert.assertEquals(addAL, addUser.getAccessLevel());

    }
    
    @Test
    public void testDeleteUser() {

        AccessLevel addAL = al.create(a);
        User addUser = u.create(user);

        u.delete(addUser);

        try {
            User deletedUser = u.read(addUser.getId());
            Assert.assertNull(deletedUser);
        } catch (EmptyResultDataAccessException ex) {

        }
    }
    
    @Test
    public void testEditUser() {

        AccessLevel addAL = al.create(a);
        User addUser = u.create(user);

        addUser.setPassword("!!!");

        u.update(addUser);

        Assert.assertEquals("!!!", addUser.getPassword());
//        Assert.assertEquals(addAL, addUser.getAccessLevel());
    }
    
    @Test
    public void testReadUser() {

        AccessLevel addAL = al.create(a);
        User addUser = u.create(user);

        Assert.assertNotNull(addUser);
        
        Assert.assertEquals("admin123", addUser.getUserName());
                
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
