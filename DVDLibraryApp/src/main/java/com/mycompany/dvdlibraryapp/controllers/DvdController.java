/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.controllers;

import com.mycompany.dvdlibraryapp.dao.DvdLibraryDao;
import com.mycompany.dvdlibraryapp.dao.NoteDao;
import com.mycompany.dvdlibraryapp.dto.AddDVDCommand;
import com.mycompany.dvdlibraryapp.dto.Dvd;
import com.mycompany.dvdlibraryapp.dto.Note;
import com.mycompany.dvdlibraryapp.dto.SearchData;
import com.mycompany.dvdlibraryapp.dto.ShowDvdViewModel;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value="/dvd")
public class DvdController {
    
    private DvdLibraryDao dvdDao;
    private NoteDao noteDao;
    
    @Inject
    public DvdController(DvdLibraryDao dvdDao, NoteDao noteDao){
        this.dvdDao = dvdDao;
        this.noteDao = noteDao;
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    @ResponseBody
    public ShowDvdViewModel show(@PathVariable("id") Integer dvdId){
        
        Dvd dvd = dvdDao.read(dvdId); 
        List<Note> notes = noteDao.dvdNotes(dvdId);
        
        ShowDvdViewModel sdvm = new ShowDvdViewModel();
        sdvm.setDvd(dvd);
        sdvm.setNotes(notes);
        
        return sdvm;
        
    }
    
    @RequestMapping(value="", method=RequestMethod.POST)
    @ResponseBody
    public Dvd add(@Valid @RequestBody AddDVDCommand addDVDCommand){
        
        Dvd d = new Dvd();
        Note n = new Note();
        
        d.setTitle(addDVDCommand.getDvdTitle());
        d.setRelease(addDVDCommand.getDvdRelease());
        d.setMpaa(addDVDCommand.getDvdMpaa());
        d.setDirector(addDVDCommand.getDvdDirector());
        d.setStudio(addDVDCommand.getDvdStudio());
        
        Dvd temp = dvdDao.create(d);
        
        if(!addDVDCommand.getDvdNote().equals(null)&&!addDVDCommand.getDvdNote().equals("")){
            n.setDvdId(temp.getId());
            n.setNote(addDVDCommand.getDvdNote());
        
            noteDao.create(n); 
        }
        
        
        return temp;
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    @ResponseBody
    public Dvd editSubmit(@Valid @RequestBody Dvd dvd){
        
        dvdDao.update(dvd); 
        return dvd;
        
    }
    

    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseBody
    public void deleteSubmit(@PathVariable("id")     Integer dvdId){
        
        Dvd toBeRemoved = dvdDao.read(dvdId);
        
        noteDao.deleteDvdNotes(dvdId);
        dvdDao.delete(toBeRemoved);  

    }
    
    @RequestMapping(value="search", method=RequestMethod.GET)
    public String searchPage(){
        return "search";
    }
    
//    @RequestMapping(value="search", method=RequestMethod.POST)
//    @ResponseBody
//    public List<Dvd> searchRequest(@RequestBody SearchData searchData){     
//        List<Dvd> results = new ArrayList();
//        
//        switch(searchData.getSearchType()){
//            case "title":
//                results=dvdDao.findTitle(searchData.getQuery());
//                break;
//            case "rating":
//                results=dvdDao.findByMpaa(searchData.getQuery());
//                break;
//            case "studio":
//                results=dvdDao.findByStudio(searchData.getQuery());
//                break;
//            case "nYears":
//                Integer years = Integer.parseInt(searchData.getQuery());
//                results=dvdDao.findLastNYears(years);
//                break;
//            case "director":
//                results = dvdDao.findByDirector(searchData.getQuery());
//                break;
//                
//        }
//        
//        
//        return results;       
//    }
    
    
    
            
    
}
