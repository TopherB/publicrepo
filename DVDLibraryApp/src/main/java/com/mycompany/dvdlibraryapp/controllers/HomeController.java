/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.controllers;

import com.mycompany.dvdlibraryapp.dao.DvdLibraryDao;
import com.mycompany.dvdlibraryapp.dto.AddDVDCommand;
import com.mycompany.dvdlibraryapp.dto.Dvd;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */

@Controller
public class HomeController {
    
    private DvdLibraryDao dvdDao;
    
    @Inject
    public HomeController(DvdLibraryDao dvdDao) {
        this.dvdDao = dvdDao;
        
    }
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(Map model){
        List<Dvd> dvds = dvdDao.list();
        
        
        model.put("dvdList", dvds);
        model.put("addDVDCommand", new AddDVDCommand());
        
        return "home";
    }
    
}
