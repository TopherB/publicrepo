/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.controllers;

import com.mycompany.dvdlibraryapp.dao.NoteDao;
import com.mycompany.dvdlibraryapp.dto.Note;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value="/note")
public class NoteController {
    
    private NoteDao noteDao;
    
    @Inject
    public NoteController(NoteDao noteDao){
        this.noteDao = noteDao;
    }
    
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    @ResponseBody
    public Note show(@PathVariable("id") Integer noteId){
        
        Note note = noteDao.read(noteId);

        return note;
        
    }
    
    @RequestMapping(value="", method=RequestMethod.POST)
    @ResponseBody
    public Note add(@Valid @RequestBody Note note){
        
        Note n = noteDao.create(note);
        
        return n;
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    @ResponseBody
    public Note editNote(@Valid @RequestBody Note note){
        
        noteDao.update(note); 
        return note;
        
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseBody
    public void deleteNote(@PathVariable("id") Integer id){
        
        noteDao.delete(noteDao.read(id));
        
    }
}
