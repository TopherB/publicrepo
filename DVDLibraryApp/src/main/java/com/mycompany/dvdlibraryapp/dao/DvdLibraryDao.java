/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.dao;

import com.mycompany.dvdlibraryapp.dto.Dvd;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface DvdLibraryDao {

    Dvd create(Dvd newDvd);
    void delete(Dvd toBeRemoved);
    Dvd read(int id);
    void update(Dvd editedDvd);
    List<Dvd> list();
    
//    List<Dvd> findTitle(String title);
//    List<Dvd> findLastNYears (Integer n);
//    List<Dvd> findByMpaa (String mpaa);
//    List<Dvd> findByDirector (String director);
//    //Map<String, List<Dvd>> firndByDirector (String director);
//    List<Dvd> findByStudio (String studio);
//    
//    Double averageAge ();
//    List<Dvd> findNewest();
//    List<Dvd> findOldest();
//    long countDvds();
}
