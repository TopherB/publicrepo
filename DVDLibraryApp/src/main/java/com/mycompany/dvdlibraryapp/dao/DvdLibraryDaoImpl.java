/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.dao;

import com.mycompany.dvdlibraryapp.dto.Dvd;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;


/**
 *
 * @author apprentice
 */
public class DvdLibraryDaoImpl implements DvdLibraryDao {
    
    
    private static String FILENAME = "DvdLibraryData.txt";
    private static final String ENCODETOKEN = ",";
    private static final String DECODETOKEN = ",(?!\\\\)";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private List<Dvd> dvdLibrary = new ArrayList();
    private Integer nextId = 1;

    public DvdLibraryDaoImpl() {
        
        dvdLibrary = decode();

        for (Dvd d : dvdLibrary) {
            if (d.getId() >= nextId) {
                nextId = d.getId()+1;
            }
        }
    }



    @Override
    public Dvd create(Dvd newDvd) {

        newDvd.setId(nextId);

        dvdLibrary.add(newDvd);

        nextId++;

        encode();

        return newDvd;
    }

    @Override
    public void update(Dvd editedDvd) {
        for (Dvd d : dvdLibrary) {
            if (d.getId() == editedDvd.getId()) {
                dvdLibrary.set(dvdLibrary.indexOf(d), editedDvd);
                break;
            }
        }
        encode();
    }

    @Override
    public Dvd read(int id) {
        for (Dvd d : dvdLibrary) {
            if (d.getId() == id) {
                return d;
            }
        }

        return null;
    }

    @Override
    public void delete(Dvd toBeRemoved) {
        for (Dvd d : dvdLibrary) {
            if (d.getId() == toBeRemoved.getId()) {
                dvdLibrary.remove(toBeRemoved);
                break;
            }
        }
        encode();
    }

    @Override
    public List<Dvd> list() {
        return new ArrayList(dvdLibrary);
    }



    private void encode() {
        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter(FILENAME));

            for (Dvd d : dvdLibrary) {

                out.print(d.getId());
                out.print(ENCODETOKEN);

                out.print(sanitize(d.getTitle()));
                out.print(ENCODETOKEN);

                out.print(d.getRelease().format(formatter));
                out.print(ENCODETOKEN);

                out.print(sanitize(d.getMpaa()));
                out.print(ENCODETOKEN);

                out.print(sanitize(d.getDirector()));
                out.print(ENCODETOKEN);

                out.print(sanitize(d.getStudio()));
                out.println("");               

            }

            out.flush();

        } catch (IOException ex) {

        } finally {
            out.close();
        }
    }
    
    private List<Dvd> decode() {
        List<Dvd> tempDvdLibrary = new ArrayList();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split(DECODETOKEN);
                Dvd myDvd = new Dvd();

                stringParts= unsanitizeAll(stringParts);
                
                int dvdId = Integer.parseInt(stringParts[0]);
                myDvd.setId(dvdId);

                myDvd.setTitle(stringParts[1]);
                
                LocalDate dvdRelease = LocalDate.parse(stringParts[2], formatter);
                myDvd.setRelease(dvdRelease);
                
                myDvd.setMpaa(stringParts[3]);
                myDvd.setDirector(stringParts[4]);
                myDvd.setStudio(stringParts[5]);

                tempDvdLibrary.add(myDvd);
            }

        } catch (FileNotFoundException ex) {
            
        }

        return tempDvdLibrary;
    }
    
    private String[] unsanitizeAll(String[] stringParts){
        for(int i=1; i<stringParts.length; i++){
            stringParts[i]= unsanitize(stringParts[i]);
        }
        
        return stringParts;
    }
    
    private String sanitize(String s){
        
        s= s.replaceAll("\\\\n", "n");
        s= s.replaceAll(",", ",\\\\");
        
        return s;
    }
    private String unsanitize(String s){
  
            s= s.replaceAll(",\\\\", ",");
        
        return s;
    }
    
//    @Override
//    public List<Dvd> findTitle(String title){
//        List<Dvd> results = dvdLibrary
//                .stream()
//                .filter(d -> d.getTitle().equalsIgnoreCase(title))
//                .collect(Collectors.toList());
//        
//        return results;
//    }
//    @Override
//    public List<Dvd> findLastNYears(Integer n) {
//        List<Dvd> results = dvdLibrary
//            .stream()
//            .filter(a -> a.getRelease()>=((Calendar.getInstance().get(Calendar.YEAR))-n))
//            .collect(Collectors.toList());
//        
//        return results;
//    }
//
//    @Override
//    public List<Dvd> findByMpaa(String mpaa) {
//        List<Dvd> results = dvdLibrary
//                .stream()
//                .filter(a -> a.getMpaa().equalsIgnoreCase(mpaa))
//                .collect(Collectors.toList());
//        
//        return results;
//    }
//
//    @Override
//    public List<Dvd> findByDirector(String director) {
//        List<Dvd> results = dvdLibrary
//                 .stream()
//                 .filter(d -> d.getDirector().equalsIgnoreCase(director))
//                 .collect(Collectors.toList());
//
////The Map way
////         Map<String, List<Dvd>> results = dvdLibrary
////                 .stream()
////                 .filter(d -> d.getDirector().equalsIgnoreCase(director))
////                 .collect(
////                         Collectors.groupingBy(Dvd::getMpaa));
//         
//         return results;
//             
//   }
//
//    @Override
//    public List<Dvd> findByStudio(String studio) {
//        List<Dvd> results = dvdLibrary
//                .stream()
//                .filter(a -> a.getStudio().equalsIgnoreCase(studio))
//                .collect(Collectors.toList());
//        
//        return results;
//    }
//
//    @Override
//    public Double averageAge() {
//        Double theAverage = dvdLibrary
//                .stream()
//                .mapToDouble( d -> (Calendar.getInstance().get(Calendar.YEAR))-d.getRelease() )
//                .average()
//                .getAsDouble();
//        
//        return theAverage;
//        
//    }
//
//    @Override
//    public List<Dvd> findNewest() {
//
//        int newFind = dvdLibrary
//                .stream()
//                .mapToInt(Dvd::getRelease)
//                .max()
//                .orElse(-1);
//                
//        List <Dvd> newest = dvdLibrary
//                .stream()
//                .filter(d -> d.getRelease() == newFind)
//                .collect(toList());
//    
//        return newest;
//    }
//                
//
//
//    @Override
//    public List<Dvd> findOldest() {
//        
//        int oldFind = dvdLibrary
//                .stream()
//                .mapToInt(Dvd::getRelease)
//                .min()
//                .orElse(-1);
//                
//        List <Dvd> oldest = dvdLibrary
//                .stream()
//                .filter(d -> d.getRelease() == oldFind)
//                .collect(toList());
//    
//        return oldest;
//    }
//    
//
//    @Override
//    public long countDvds() {
//
//        long total = dvdLibrary
//                .stream()
//                .count();
//                
//                
//        return total;
//        
//    }

}
