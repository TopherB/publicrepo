/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.dao;

import com.mycompany.dvdlibraryapp.dto.Dvd;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 *
 * @author apprentice
 */
public class DvdLibraryDaoInMemoryImpl implements DvdLibraryDao {
    
    

    private List<Dvd> dvdLibrary = new ArrayList();
    private Integer nextId = 1;


    @Override
    public Dvd create(Dvd newDvd) {

        newDvd.setId(nextId);

        dvdLibrary.add(newDvd);

        nextId++;

        return newDvd;
    }

    @Override
    public void update(Dvd editedDvd) {
        for (Dvd d : dvdLibrary) {
            if (d.getId() == editedDvd.getId()) {
                dvdLibrary.set(dvdLibrary.indexOf(d), editedDvd);
                break;
            }
        }
    }

    @Override
    public Dvd read(int id) {
        for (Dvd d : dvdLibrary) {
            if (d.getId() == id) {
                return d;
            }
        }
        //io.displayString("No Dvd found with id #" + id);
        return null;
    }

    @Override
    public void delete(Dvd toBeRemoved) {
        for (Dvd d : dvdLibrary) {
            if (d.getId() == toBeRemoved.getId()) {
                dvdLibrary.remove(toBeRemoved);
                break;
            }
        }
    }

    @Override
    public List<Dvd> list() {
        return new ArrayList(dvdLibrary);
    }

//    @Override
//    public List<Dvd> findTitle(String title) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//    
//    @Override
//    public List<Dvd> findLastNYears(Integer n) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public List<Dvd> findByMpaa(String mpaa) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public List<Dvd> findByDirector(String director) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public List<Dvd> findByStudio(String studio) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public Double averageAge() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public List<Dvd> findNewest() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public List<Dvd> findOldest() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public long countDvds() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }




}
