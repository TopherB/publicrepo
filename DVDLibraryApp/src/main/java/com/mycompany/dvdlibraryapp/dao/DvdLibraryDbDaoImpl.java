/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.dao;

import com.mycompany.dvdlibraryapp.dto.Dvd;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class DvdLibraryDbDaoImpl implements DvdLibraryDao{

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    
    private static final String SQL_INSERT_DVD = "INSERT INTO `dvd` (`title`, `release_date`, `mpaa`, `director`, `studio`) VALUES (?, ?, ?, ?, ?);";
    private static final String SQL_DELETE_DVD = "DELETE FROM `dvd` WHERE `id`=?;";
    private static final String SQL_UPDATE_DVD = "UPDATE `dvd` SET `title`=?, `release_date`=?, `mpaa`=?, `director`=?, `studio`=? WHERE `id`=?;";
    private static final String SQL_SELECT_DVD = "SELECT * FROM dvd_library.dvd WHERE id=?;";
    private static final String SQL_SELECT_ALL_DVDS = "SELECT * FROM dvd_library.dvd";
    
    private JdbcTemplate jdbcTemplate;

    public DvdLibraryDbDaoImpl(JdbcTemplate j) {
        this.jdbcTemplate = j;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Dvd create(Dvd dvd) {
        jdbcTemplate.update(SQL_INSERT_DVD,
                dvd.getTitle(),
                dvd.getRelease().format(formatter),
                dvd.getMpaa(),
                dvd.getDirector(),
                dvd.getStudio());
        
        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
    
        dvd.setId(newId);
        
        return dvd;
    }

    @Override
    public void delete(Dvd toBeRemoved) {
        jdbcTemplate.update(SQL_DELETE_DVD, toBeRemoved.getId());
    }

    @Override
    public Dvd read(int id) {
        Dvd d = jdbcTemplate.queryForObject(SQL_SELECT_DVD, new DvdMapper(), id);
        return d;
    }

    @Override
    public void update(Dvd dvd) {
        jdbcTemplate.update(SQL_UPDATE_DVD,
                dvd.getTitle(),
                dvd.getRelease().format(formatter),
                dvd.getMpaa(),
                dvd.getDirector(),
                dvd.getStudio(),
                dvd.getId());
    }

    @Override
    public List<Dvd> list() {
        List<Dvd> dvds = jdbcTemplate.query(SQL_SELECT_ALL_DVDS, new DvdMapper());
        return dvds;
    }
    
    
    private static final class DvdMapper implements RowMapper<Dvd> {
        
        @Override
        public Dvd mapRow(ResultSet rs, int i) throws SQLException{
            Dvd dvd = new Dvd();
            dvd.setId(rs.getInt("id"));
            dvd.setTitle(rs.getString("title"));
            dvd.setRelease(LocalDate.parse(rs.getString("release_date"), formatter));
            dvd.setMpaa(rs.getString("mpaa"));
            dvd.setDirector(rs.getString("director"));
            dvd.setStudio(rs.getString("studio"));
            
            return dvd;
        }
    }
    
}
