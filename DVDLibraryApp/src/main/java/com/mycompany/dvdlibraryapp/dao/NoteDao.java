/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.dao;

import com.mycompany.dvdlibraryapp.dto.Note;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface NoteDao {
    
    Note create(Note newNote);
    Note read(int id);
    void update(Note editNote);
    void delete(Note toBeRemoved);
    void deleteDvdNotes(int dvdId);
    List<Note> dvdNotes(int dvdId);
    long noteCount();


    
}
