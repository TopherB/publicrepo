/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.dao;

import com.mycompany.dvdlibraryapp.dto.Note;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class NoteDaoImpl implements NoteDao {
    
    private static final String FILENAME = ("NoteData.txt");
    private static final String ENCODETOKEN = ",";
    private static final String DECODETOKEN = ",(?!\\\\)";
    
    private List<Note> noteList = new ArrayList();
    private Integer nextId =1;
    
    public NoteDaoImpl(){
        noteList = decode(); 
        
        for (Note a : noteList) {
            if (a.getId() >= nextId) {
                nextId = a.getId() + 1;
            }

        }
    }
    
    @Override
    public Note create(Note newNote){

        newNote.setId(nextId);
        
        noteList.add(newNote);
        
        nextId++;
        
        encode();
        
        return newNote;
    }
    
    @Override
    public Note read (int id){
        
        for(Note n: noteList){
            if(n.getId()==id){
                return n;
            }
        }
        return null;
    }
    
    @Override
    public void update(Note editNote){
        
        for(Note n: noteList){
            if(n.getId()==editNote.getId()){
                noteList.set(noteList.indexOf(n), editNote);
                break;
            }
        }       
        encode();    
        
    }
    
    @Override
    public void delete(Note toBeRemoved){
        
        for(Note n: noteList){
            if(n.getId()==toBeRemoved.getId()){
                noteList.remove(n);
                break;
            }
        }
        encode();
    }
    
    @Override
    public void deleteDvdNotes(int dvdId) {
        List<Note> toBeDeleted = dvdNotes(dvdId);

        for (Note n : toBeDeleted) {
            delete(n);
        }

    }
    
    @Override
    public List<Note> dvdNotes(int dvdId){
        List<Note> tempList = new ArrayList();
        
        for(Note n: noteList){
            if(n.getDvdId()==dvdId){
                tempList.add(n);
            }
        }
        return tempList;
    }
    
    @Override
    public long noteCount(){
        long count = noteList
                .stream()
                .count();
                
        return count;
    }    

    private void encode(){
        PrintWriter out = null;
        try{
            out = new PrintWriter(new FileWriter(FILENAME));
            
            for (Note n : noteList){
                
                out.print(n.getId());
                out.print(ENCODETOKEN);
                
                out.print(n.getDvdId());
                out.print(ENCODETOKEN);
                
                out.print(sanitize(n.getNote()));
                out.println("");
                
            }
            
        out.flush();
            
        }catch (IOException ex){
            
        }finally {
            out.close();
        }
    }
    
    private List<Note> decode(){
        
        List<Note> tempNoteList = new ArrayList();
        
        try{
           Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine(); //read
                String[] stringParts = currentLine.split(DECODETOKEN);//split into String array by TOKEN
                Note myNote = new Note(); //create new Address object

                int noteId = Integer.parseInt(stringParts[0]); //parse first section into an int id
                myNote.setId(noteId);

                int dvdId = Integer.parseInt(stringParts[1]);
                myNote.setDvdId(dvdId);
                
                stringParts[2] = unsanitize(stringParts[2]);
                myNote.setNote(stringParts[2]);

                tempNoteList.add(myNote);//add the completed object to the addressBook

            }

        } catch (FileNotFoundException ex) {

        }

        return tempNoteList;
        
        
    }
    
    private String sanitize(String s){
        
        s= s.replaceAll("\\\\n", "n");
        s= s.replaceAll(",", ",\\\\");
        
        return s;
    }
    private String unsanitize(String s){
  
            s= s.replaceAll(",\\\\", ",");
        
        return s;
    }
    
}
