/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.dao;

import com.mycompany.dvdlibraryapp.dto.Note;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class NoteDaoInMemoryImpl implements NoteDao {
    
    
    private List<Note> noteList = new ArrayList();
    private Integer nextId =1;
    
    public NoteDaoInMemoryImpl(){
        
        for (Note a : noteList) {
            if (a.getId() >= nextId) {
                nextId = a.getId() + 1;
            }

        }
    }
    
    @Override
    public Note create(Note newNote){

        newNote.setId(nextId);
        
        noteList.add(newNote);
        
        nextId++;
                
        return newNote;
    }
    
    @Override
    public Note read (int id){
        
        for(Note n: noteList){
            if(n.getId()==id){
                return n;
            }
        }
        return null;
    }
    
    @Override
    public void update(Note editNote){
        
        for(Note n: noteList){
            if(n.getId()==editNote.getId()){
                n=editNote;
                break;
            }
        }       
        
    }
    
    @Override
    public void delete(Note toBeRemoved){
        
        for(Note n: noteList){
            if(n.getId()==toBeRemoved.getId()){
                noteList.remove(n);
                break;
            }
        }
    }
    
    @Override
    public List<Note> dvdNotes(int dvdId){
        List<Note> tempList = new ArrayList();
        
        for(Note n: noteList){
            if(n.getDvdId()==dvdId){
                tempList.add(n);
            }
        }
        return tempList;
    }
    
    @Override
    public long noteCount(){
        long count = noteList
                .stream()
                .count();
                
        return count;
    }    

    @Override
    public void deleteDvdNotes(int dvdId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
