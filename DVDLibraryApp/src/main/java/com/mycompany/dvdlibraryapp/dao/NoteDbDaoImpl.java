/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.dao;

import com.mycompany.dvdlibraryapp.dto.Note;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class NoteDbDaoImpl implements NoteDao {

    private static final String SQL_INSERT_NOTE = "INSERT INTO `dvd_library`.`note` (`dvd_id`, `note`) VALUES (?, ?);";
    private static final String SQL_DELETE_NOTE = "DELETE FROM `dvd_library`.`note` WHERE `id`=?;";
    private static final String SQL_UPDATE_NOTE = "UPDATE `dvd_library`.`note` SET `dvd_id`=?, `note`=? WHERE `id`=?;";
    private static final String SQL_SELECT_NOTE = "SELECT * FROM dvd_library.note WHERE id=?;";
    private static final String SQL_SELECT_DVD_NOTES = "SELECT * FROM dvd_library.note  WHERE `dvd_id`=?";
    private static final String SQL_DELETE_DVD_NOTES = "DELETE FROM `dvd_library`.`note` WHERE `dvd_id`=?;";
    private static final String SQL_COUNT_NOTES = "SELECT COUNT(*) FROM `dvd_library`.`note`;";
    
    private JdbcTemplate jdbcTemplate;

    public NoteDbDaoImpl(JdbcTemplate j) {
        this.jdbcTemplate = j;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Note create(Note note) {
        jdbcTemplate.update(SQL_INSERT_NOTE, 
                note.getDvdId(), 
                note.getNote()); 
                
        
        Integer newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        
        note.setId(newId);
        
        return note;
    }

    @Override
    public Note read(int id) {
        Note n = jdbcTemplate.queryForObject(SQL_SELECT_NOTE, new NoteMapper(), id);
        return n;    
    }

    @Override
    public void update(Note note) {
        jdbcTemplate.update(SQL_UPDATE_NOTE, 
                note.getDvdId(), 
                note.getNote(),
                note.getId()); 
    }

    @Override
    public void delete(Note toBeRemoved) {
        jdbcTemplate.update(SQL_DELETE_NOTE, toBeRemoved.getId());
    }

    @Override
    public void deleteDvdNotes(int dvdId) {
        jdbcTemplate.update(SQL_DELETE_DVD_NOTES, dvdId);
    }

    @Override
    public List<Note> dvdNotes(int dvdId) {
        List<Note> notes = jdbcTemplate.query(SQL_SELECT_DVD_NOTES, new NoteMapper(), dvdId);
        return notes;
    }

    @Override
    public long noteCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private static final class NoteMapper implements RowMapper<Note> {

        @Override
        public Note mapRow(ResultSet rs, int i) throws SQLException {  // ORM: takes one row of data and returns an object
            Note note = new Note();
            note.setId(rs.getInt("id"));
            note.setDvdId(rs.getInt("dvd_id"));
            note.setNote(rs.getString("note"));
            
            return note;
        }
        
    }
    
}
