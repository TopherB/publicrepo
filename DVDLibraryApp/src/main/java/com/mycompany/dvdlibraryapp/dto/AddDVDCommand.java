/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import java.util.Date;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;


public class AddDVDCommand {
    
    @NotEmpty(message="Please enter a title")
    @Length(max=75, message="Title can't be more than 25 characters long")
    private String dvdTitle;
    
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "America/NewYork")
    private LocalDate dvdRelease;
    
    @NotEmpty(message="Please enter a rating")
    @Length(max=5, message="Rating can't be more than 5 characters long")
    private String dvdMpaa;
    
    @NotEmpty(message="Please enter a director")
    @Length(max=75, message="Director name can't be more than 25 characters long")
    private String dvdDirector;
    
    @NotEmpty(message="Please enter a studio")
    @Length(max=75, message="Studio can't be more than 25 characters long")
    private String dvdStudio;
    
    @Length(max=200, message="Note cannot be more than 200 characters long")
    private String dvdNote;

    public String getDvdTitle() {
        return dvdTitle;
    }

    public void setDvdTitle(String dvdTitle) {
        this.dvdTitle = dvdTitle;
    }

    public LocalDate getDvdRelease() {
        return dvdRelease;
    }

    public void setDvdRelease(LocalDate dvdRelease) {
        this.dvdRelease = dvdRelease;
    }

    public String getDvdMpaa() {
        return dvdMpaa;
    }

    public void setDvdMpaa(String dvdMpaa) {
        this.dvdMpaa = dvdMpaa;
    }

    public String getDvdDirector() {
        return dvdDirector;
    }

    public void setDvdDirector(String dvdDirector) {
        this.dvdDirector = dvdDirector;
    }

    public String getDvdStudio() {
        return dvdStudio;
    }

    public void setDvdStudio(String dvdStudio) {
        this.dvdStudio = dvdStudio;
    }

    public String getDvdNote() {
        return dvdNote;
    }

    public void setDvdNote(String dvdNote) {
        this.dvdNote = dvdNote;
    }


    
    
}
