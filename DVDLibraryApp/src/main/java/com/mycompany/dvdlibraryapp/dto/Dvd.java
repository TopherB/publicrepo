/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibraryapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;


public class Dvd {
    
    private int id;
    
    @NotEmpty(message="Please enter a title")
    @Length(max=75, message="Title can't be more than 25 characters long")
    private String title;
    
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "America/NewYork")
    private LocalDate release;
    
    private String mpaa;
    
    @NotEmpty(message="Please enter a director")
    @Length(max=75, message="Director can't be more than 25 characters long")
    private String director;
    
    @NotEmpty(message="Please enter a studio")
    @Length(max=75, message="Studio can't be more than 25 characters long")
    private String studio;

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getRelease() {
        return release;
    }

    public void setRelease(LocalDate release) {
        this.release = release;
    }

    public String getMpaa() {
        return mpaa;
    }

    public void setMpaa(String mpaa) {
        this.mpaa = mpaa;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getStudio() {
        return studio;
    }

    public void setStudio(String studio) {
        this.studio = studio;
    }


    
   
    
}
