$(document).ready(function () {

    $('#createDvdButton').on('click', function (e) {
        e.preventDefault();
        $('#add-dvd-validation-errors').text('');

        var myDvdCommand = {
            dvdTitle: $('#titleCreate').val(),
            dvdRelease: $('#releaseCreate').val(),
            dvdMpaa: $('#mpaaCreate').val(),
            dvdDirector: $('#directorCreate').val(),
            dvdStudio: $('#studioCreate').val(),
            dvdNote: $('#noteCreate').val()
        };

        var myDvdCommandData = JSON.stringify(myDvdCommand);
     
        $.ajax({
            url: contextRoot + "/dvd",
            type: "POST",
            data: myDvdCommandData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                var tableRow = buildDvdRow(data);

                $('#dvdTable').append($(tableRow)),
                $('#titleCreate').val(''),
                $('#releaseCreate').val(''),
                $('#mpaaCreate').val(''),
                $('#directorCreate').val(''),
                $('#studioCreate').val(''),
                $('#noteCreate').val('');
            },
            error: function (data, status) {
                var errors = data.responseJSON.errors;
                $.each(errors, function (index, error) {
                    $('#add-dvd-validation-errors').append(error.message + "<br/>");
                });
            }

        });

    });

    $('#showDvdModal').on('shown.bs.modal', function (e) {

        var link = $(e.relatedTarget);
        var dvdId = link.data('dvd-id');

        $.ajax({
            url: contextRoot + "/dvd/" + dvdId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function (data, status) {
                $('#showTitle').text(data.dvd.title);
                $('#showRelease').text(data.dvd.release);
                $('#showMpaa').text(data.dvd.mpaa);
                $('#showDirector').text(data.dvd.director);
                $('#showStudio').text(data.dvd.studio);

                $(data.notes).each(function (key, value) {
                    var noteRow = '\
                        <tr class="noteRow">\n\
                            <th>User Note ' + (key + 1) + '</th>\n\
                            <td>' + value.note + '</td>\n\
                        </tr>'
                    $('#showDvdTable').append(noteRow);
                });
            },
            error: function (data, staus) {
                alert("DVD not found");
            }

        });

    });

    $('#editDvdModal').on('shown.bs.modal', function (e) {

        var link = $(e.relatedTarget);
        var dvdId = link.data('dvd-id');

        $.ajax({
            url: contextRoot + "/dvd/" + dvdId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function (data, status) {
                $('#editId').val(data.dvd.id);
                $('#editTitle').val(data.dvd.title);
                $('#editRelease').val(data.dvd.release);
                $('#editMpaa').val(data.dvd.mpaa);
                $('#editDirector').val(data.dvd.director);
                $('#editStudio').val(data.dvd.studio);

                $(data.notes).each(function (key, value) {
                    var noteRow = buildNoteRow(key, value);
                    $('#editDvdTable').append(noteRow);
                });
            },
            error: function (data, staus) {
                alert("DVD not found");
            }

        });

    });

    $('#editDvdButton').on('click', function (e) {
        e.preventDefault();
        $('#edit-dvd-validation-errors').text('');
        
        var myDvd = {
            id: $('#editId').val(),
            title: $('#editTitle').val(),
            release: $('#editRelease').val(),
            mpaa: $('#editMpaa').val(),
            director: $('#editDirector').val(),
            studio: $('#editStudio').val()

        };

        var myDvdData = JSON.stringify(myDvd);

        $.ajax({
            url: contextRoot + "/dvd/" + myDvd.id,
            type: "PUT",
            data: myDvdData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                $("#editDvdModal").modal('hide');
                var tableRow = buildDvdRow(data);

                $('#dvd-row-' + data.id).replaceWith($(tableRow));
            },
            error: function (data, staus) {
                var errors = data.responseJSON.errors;
                $.each(errors, function (index, error) {
                    $('#edit-dvd-validation-errors').append(error.message + "<br/>");
                });
            }

        });

    });


    $('#deleteDvdModal').on('shown.bs.modal', function (e) {

        var link = $(e.relatedTarget);
        var dvdId = link.data('dvd-id');

        $.ajax({
            url: contextRoot + "/dvd/" + dvdId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function (data, status) {
                $('#deleteId').val(data.dvd.id);
                $('#deleteTitle').text(data.dvd.title);
                $('#deleteRelease').text(data.dvd.release);
                $('#deleteMpaa').text(data.dvd.mpaa);
                $('#deleteDirector').text(data.dvd.director);
                $('#deleteStudio').text(data.dvd.studio);

            },
            error: function (data, staus) {
                alert("DVD not found");
            }

        });

    });

    $('#deleteDvdButton').on('click', function (e) {
        e.preventDefault();
        var link = $(e.target);
        var dvdId = $('#deleteId').val();

        $.ajax({
            url: contextRoot + "/dvd/" + dvdId,
            type: "DELETE",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                $('#dvd-row-' + dvdId).remove();
            },
            error: function (data, staus) {
                alert("DVD not found");
            }

        });

    });

//<!-- Note Functions ->>
    $('#addNoteModal').on('shown.bs.modal', function (e) {
        
        var link = $(e.relatedTarget);
        var dvdId = $('#editId').val();
        $('#createNoteDvdId').val(dvdId);

    });

    $('#createNoteButton').on('click', function (e) {
        e.preventDefault();
      
        $('#add-note-validation-error').text('');

        var myNote= {
            note: $('#createNote').val(),
            dvdId: $('#createNoteDvdId').val()
        };
        
        var myNoteData = JSON.stringify(myNote);
        
        $.ajax({
            url: contextRoot + "/note",
            type: "POST",
            data: myNoteData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                var noteRowLength = (document.getElementById("editDvdTable").rows.length)-5;
                var noteRow = buildNoteRow(noteRowLength,data);
                $('#editDvdTable').append(noteRow);
                
                $("#addNoteModal").modal('hide');

            },
            error: function (data, status) {
                var errors = data.responseJSON.errors;
                $.each(errors, function(index, error){
                    $('#add-note-validation-error').append(error.message+"<br/>"); 
                });
                
            }

        });

    });

    $('#editNoteModal').on('shown.bs.modal', function (e) {
        
        var link = $(e.relatedTarget);
        var noteId = link.data('note-id');
        var notePosition = link.data('note-position');

        $.ajax({
            url: contextRoot + "/note/" + noteId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function (data, status) {
                $('#editNoteId').val(data.id);
                $('#editNoteDvdId').val(data.dvdId);
                $('#editNote').val(data.note);
                $('#notePosition').val(notePosition);

            },
            error: function (data, staus) {
                alert("Note not found");
            }

        });
    });
    
    $('body').on('click','#editNoteButton', function (e) {
        e.preventDefault();

        $('#edit-note-validation-error').text('');
        
        var link = $(e.target);
        var notePosition = $('#notePosition').val();
        console.log(notePosition);
        var myNote = {
            id: $('#editNoteId').val(),
            dvdId: $('#editNoteDvdId').val(),
            note: $('#editNote').val()
        };

        var myNoteData = JSON.stringify(myNote);

        $.ajax({
            url: contextRoot + "/note/" + myNote.id,
            type: "PUT",
            data: myNoteData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                $("#editNoteModal").modal('hide');
                
                var noteRow = buildNoteRow(notePosition-1, data);

                $('#note-row-' + data.id).replaceWith($(noteRow));
            },
            error: function (data, staus) {
                var errors = data.responseJSON.errors;
                $.each(errors, function(index, error){
                    $('#edit-note-validation-error').append(error.message+"<br/>"); 
                });
            }

        });

    });
    
    $('#deleteNoteModal').on('shown.bs.modal', function (e) {

        var link = $(e.relatedTarget);
        var noteId = link.data('note-id');

        $.ajax({
            url: contextRoot + "/note/" + noteId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function (data, status) {
                $('#deleteNoteId').val(data.id);
                $('#deleteNote').text(data.note);
                
            },
            error: function (data, staus) {
                alert("Note not found");
            }

        });

    });

    $('#deleteNoteButton').on('click', function (e) {
        e.preventDefault();
        var link = $(e.target);
        var noteId = $('#deleteNoteId').val();

        $.ajax({
            url: contextRoot + "/note/" + noteId,
            type: "DELETE",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                $('#note-row-' + noteId).remove();
            },
            error: function (data, staus) {
                alert("Note not found");
            }

        });

    });
    
//<!--Search Methods-->
    $('#searchButton').on('click', function (e) {
        e.preventDefault();
        $('#searchResults').empty();
        var link = $(e.target);
        
        var mySearch = {
            searchType: $('#searchType').val(),
            query: $('#query').val()
        };
        
        var mySearchData = JSON.stringify(mySearch);
        
        $.ajax({
            url: contextRoot + "/dvd/search",
            type: "POST",
            data: mySearchData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
//                var dvdRow = buildDvdRow(data);
                console.log('the data is'+data);
                console.log(data);
                $(data).each(function (dvd) {
                    var dvdRow = buildDvdRow(dvd);
                    console.log(dvd);
                    $('#searchResults').append(dvdRow);
                });


                
                $('#searchType').val(''),
                $('#query').val('');
            },
            error: function (data, staus) {
                alert("Something went wrong with the search");
            }

        });

    });
    $('#showDvdModal').on('hidden.bs.modal', function () {

        $('#showTitle').text('');
        $('#showRelease').text('');
        $('#showMpaa').text('');
        $('#showDirector').text('');
        $('#showStudio').text('');
        $('.noteRow').remove();
    });

    $('#editDvdModal').on('hidden.bs.modal', function () {

        $('#editTitle').val('');
        $('#editRelease').val('');
        $('#editDirector').val('');
        $('#editStudio').val('');
        $('.noteRow').remove();
    });

    $('#deleteDvdModal').on('hidden.bs.modal', function () {

        $('#deleteTitle').text('');
        $('#deleteRelease').text('');
        $('#deleteMpaa').text('');
        $('#deleteDirector').text('');
        $('#deleteStudio').text('');
        $('.noteRow').remove();
    });

    $('#addNoteModal').on('hidden.bs.modal', function(){
        $('#add-note-validation-error').text('');
        $('#createNote').val('');
    });
    
    $('#editNoteModal').on('hidden.bs.modal', function(){
        
        $('#editNote').val('');
    });
    
    $('#deleteNoteModal').on('hidden.bs.modal', function(){
        
        $('#deleteNote').text('');
    });
    
    

    function buildDvdRow(data) {

        var tableRow = '\
            <tr id="dvd-row-' + data.id + '"> \n\
                <td><a data-toggle="modal" data-target="#showDvdModal" data-dvd-id="' + data.id + '">' + data.title + '</a></td> \n\
                <td>' + data.release + '</td> \n\
                <td><a data-toggle="modal" data-target="#editDvdModal" data-dvd-id="' + data.id + '"> Edit </a></td> \n\
                <td><a data-toggle="modal" data-target="#deleteDvdModal" data-dvd-id="' + data.id + '"> Delete </a></td> \n\
            </tr>';
        return tableRow;
    }

    function buildNoteRow(key, value) {
        key++;
        var noteRow = '\
            <tr class="noteRow" id="note-row-' + value.id + '">\n\
                <th>User Note ' + key + '</th>\n\
                <td>' + value.note + '</td>\n\
                <td><a data-toggle="modal" data-target="#editNoteModal" data-note-id="' + value.id + '" data-note-position="' + key + '"> Edit </a></td>\n\
                <td><a data-toggle="modal" data-target="#deleteNoteModal" data-note-id="' + value.id + '"> Delete </a></td>\n\
           </tr>';
        return noteRow;
    }

});
