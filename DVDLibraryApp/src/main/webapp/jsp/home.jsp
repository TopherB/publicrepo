<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <h1>DVD Library 3000</h1>
            <jsp:include page="navbar.jsp" />

            <div class="row">
                <div class ="col-sm-6">
                    <h2>My DVD Library</h2>
                    <table class="table table-responsive" id="dvdTable">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Year</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <c:forEach items="${dvdList}" var="dvd">
                            <tr id="dvd-row-${dvd.id}">
                                <td><a data-toggle="modal" data-target="#showDvdModal" data-dvd-id="${dvd.id}">${dvd.title}</a></td>
                                <td>${dvd.release}</td>
                                <td><a data-toggle="modal" data-target="#editDvdModal" data-dvd-id="${dvd.id}">Edit</a></td>
                                <td><a data-toggle="modal" data-target="#deleteDvdModal" data-dvd-id="${dvd.id}">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </table>

                </div>

                <div class="col-sm-6">
                    <h2>Add New DVD</h2>

                    <sf:form method="POST" action="${pageContext.request.contextPath}/dvd/create" commandName="addDVDCommand" class="form-horizontal" >

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="dvdTitle">Title:</label>
                            <div class="col-sm-9">
                                <sf:input type="text" path="dvdTitle" id="titleCreate" class="form-control"/>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="dvdRelease">Release Date:</label>
                            <div class="col-sm-9">
                                <sf:input type="date" path="dvdRelease" id="releaseCreate" class="form-control"/>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="dvdMpaa">MPAA Rating:</label>
                            <div class="col-sm-9">
                                <sf:select class="form-control" path="dvdMpaa" id="mpaaCreate">
                                    <sf:option value="G">G</sf:option>
                                    <sf:option value="PG">PG</sf:option>
                                    <sf:option value="PG-13">PG-13</sf:option>
                                    <sf:option value="R">R</sf:option>
                                    <sf:option value="NC-17">NC-17</sf:option>
                                </sf:select>

                                
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="dvdDirector">Director:</label>
                            <div class="col-sm-9">
                                <sf:input type="text" path="dvdDirector"  id="directorCreate" class="form-control"/>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="dvdStudio">Studio:</label>
                            <div class="col-sm-9">
                                <sf:input type="text" path="dvdStudio" id="studioCreate" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="dvdNote">User Note:</label>
                            <div class="col-sm-9">
                                <sf:textarea path="dvdNote" class="form-control" id="noteCreate" ></sf:textarea>
                                </div>
                            </div> 
                            <div class="form-group"> 
                                <div class="col-sm-offset-3 col-sm-3">
                                    <button type="submit" class="btn btn-default" id="createDvdButton">Create DVD</button>
                                </div>
                                <div class="col-sm-6 alert-danger" id="add-dvd-validation-errors"/>
                            </div>


                    </sf:form>


                </div>
            </div>

            <!-- Show Dvd Modal -->
            <div class="modal fade" id="showDvdModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">


                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Dvd Info</h4>
                        </div>
                        <div class="modal-body">

                            <table class="table" >

                                <tbody id="showDvdTable">
                                    <tr>
                                        <th>Title:</th>
                                        <td id="showTitle">${dvd.title}</td>
                                    </tr>
                                    <tr>
                                        <th>Release:</th>
                                        <td id="showRelease">${dvd.release}</td>
                                    </tr>
                                    <tr>
                                        <th>MPAA Rating:</th>
                                        <td id="showMpaa">${dvd.mpaa}</td>
                                    </tr>
                                    <tr>
                                        <th>Director:</th>
                                        <td id="showDirector">${dvd.director}</td>
                                    </tr>
                                    <tr>
                                        <th>Studio:</th>
                                        <td id="showStudio">${dvd.studio}</td>
                                    </tr>


                                </tbody>


                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!--Edit Dvd Modal -->
            <div class="modal" id="editDvdModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content"> 
                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Edit DVD</h4>
                        </div>

                        <form>

                            <div class="modal-body">

                                <table class="table table-responsive">
                                    <input type="hidden" id="editId" />
                                    <tbody id="editDvdTable">
                                        <tr>
                                            <th>Title:</th>
                                            <td><input type="text" id="editTitle" class="form-control"/></td>
                                        </tr>
                                        <tr>
                                            <th>Release:</th>
                                            <td><input type="date" id="editRelease" class="form-control"/></td>
                                        </tr>
                                        <tr>
                                            <th>MPAA Rating:</th>
                                            <td><select class="form-control" id="editMpaa">
                                                    <option value="G" ${dvd.mpaa=="G"? "selected" : ""} >G</option>
                                                    <option value="PG" ${dvd.mpaa=="PG"? "selected" : ""} >PG</option>
                                                    <option value="PG-13" ${dvd.mpaa=="PG-13"? "selected" : ""} >PG-13</option>
                                                    <option value="R" ${dvd.mpaa=="R"? "selected" : ""} >R</option>
                                                    <option value="NC-17" ${dvd.mpaa=="NC-17"? "selected" : ""} >NC-17</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Director:</th>
                                            <td><input type="text" id="editDirector" class="form-control"/></td>
                                        </tr>
                                        <tr>
                                            <th>Studio:</th>
                                            <td><input type="text" id="editStudio" class="form-control"/><div class="alert-danger" id="edit-dvd-validation-errors"/></td>
                                        </tr>
                                    </tbody>


                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary"  data-toggle="modal" data-target="#addNoteModal" data-show="true">Add User Note</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-secondary" id="editDvdButton">Submit</button>
                                
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            <!--Delete Dvd Modal -->
            <div class="modal fade" id="deleteDvdModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
                        </div>
                        <div class="modal-body">

                            <table class="table">

                                <tbody id="deleteDvdTable">
                                    <tr>
                                        <th>Title:</th>
                                        <td id="deleteTitle">${dvd.title}</td>
                                    </tr>
                                    <tr>
                                        <th>Release Date:</th>
                                        <td id="deleteRelease">${dvd.release}</td>
                                    </tr>
                                    <tr>
                                        <th>MPAA Rating:</th>
                                        <td id="deleteMpaa">${dvd.mpaa}</td>
                                    </tr>
                                    <tr>
                                        <th>Director:</th>
                                        <td id="deleteDirector">${dvd.director}</td>
                                    </tr>
                                    <tr>
                                        <th>Studio:</th>
                                        <td id="deleteStudio">${dvd.studio}</td>
                                    </tr>
                                <input type="hidden" id="deleteId"/>
                                </tbody>


                            </table>
                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="deleteDvdButton" data-dvd-id="${dvd.id}">Delete</button>
                        </div>
                    </div>
                </div>
            </div>

            <!--Add Note Modal-->
            <div class="modal" id="addNoteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Add Note</h4>
                        </div>

                        <form>
                            <div class="modal-body">
                                <table class="table table-responsive">
                                    <tbody>
                                        <tr>
                                            <th>Enter Note:</th>
                                            <td><input type="text" id="createNote" class="form-control"/><div class="alert-danger" id="add-note-validation-error"/></td>
                                            
                                        </tr>
                                    <input type="hidden" id="createNoteDvdId" />
                                    
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-secondary" id="createNoteButton">Create Note</button>
                                
                            </div>
                        </form>

                    </div>
                </div>
            </div>

            <!--Edit Note Modal -->
            <div class="modal" id="editNoteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Edit Note</h4>
                        </div>

                        <form>
                            <div class="modal-body">
                                <table class="table table-responsive">
                                    <input type="hidden" id="editNoteId" />
                                    <tbody>
                                        <tr>
                                            <th>User Note ${notePosition + 1}:</th>
                                            <td><input type="text" id="editNote" class="form-control"/><div class="alert-danger" id="edit-note-validation-error"/></td>
                                        </tr>
                                    <input type="hidden" id="editNoteDvdId" />
                                    <input type="hidden" id="editNoteId" />
                                    <input type="hidden" id="notePosition" />
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-secondary"  id="editNoteButton">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            
            <!--Delete Note Modal -->
            <div class="modal" id="deleteNoteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Delete Note</h4>
                        </div>
                        <div class="modal-body">

                            <table class="table">

                                <tbody>
                                    <tr>
                                        <th>User Note:</th>
                                        <td id="deleteNote">${note.note}</td>
                                    </tr>
            
                                <input type="hidden" id="deleteNoteId"/>
                                </tbody>


                            </table>
                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="deleteNoteButton" data-note-id="${note.id}">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!--container div-->
                            
        <script> var contextRoot = "${pageContext.request.contextPath}";</script>

        <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/app.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    </body>
</html>