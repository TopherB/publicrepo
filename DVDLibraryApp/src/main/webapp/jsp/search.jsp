<%-- 
    Document   : search
    Created on : Sep 20, 2016, 12:19:26 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <h1>Search DVD Library</h1>
            <jsp:include page="navbar.jsp" />

            <div class="col-sm-6">
                <br/>
                <form method="POST" action="${pageContext.request.contextPath}/dvd/search"  class="form-horizontal" >
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="searchType">Search type:</label>
                        <div class="col-sm-9">
                            <select id="searchType" class="form-control">
                                <option value="title">Title</option>
                                <option value="rating">Rating</option>
                                <option value="director">Director</option>
                                <option value="studio">Studio</option>
                                <option value="nYears">All For Last X Years</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="query">Search Query:</label>
                            <div class="col-sm-6">
                                <input type="text" id="query" class="form-control"/>
                            </div>
                            
                        </div>
                        <button type="submit" id="searchButton" class="btn btn-default">Search</button>
                    </div>
                </form>



            </div>
            <div class="col-sm-6">
                <h2>Results</h2>
                <table class="table" >
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Release</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="searchResults">
                        <tr>
                            <td>Test</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
                    
        <script> var contextRoot = "${pageContext.request.contextPath}";</script>

        <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/app.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
