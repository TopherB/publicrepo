/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daotests;

import com.mycompany.dvdlibraryapp.dao.DvdLibraryDao;
import com.mycompany.dvdlibraryapp.dto.Dvd;
import java.time.LocalDate;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class DvdDaoTest {
    
    DvdLibraryDao dao;
    Dvd testDvd = new Dvd();
    
    public DvdDaoTest() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");
        this.dao = ctx.getBean("dvdDao" , DvdLibraryDao.class);
    }

    
    @Before
    public void setUp() {
        
        testDvd.setTitle("The Matrix");
        testDvd.setRelease(LocalDate.now());
        testDvd.setMpaa("R");
        testDvd.setDirector("The Wachowski Brothers");
        testDvd.setStudio("Warner Bros.");
        
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void addDvd(){
        Dvd result = new Dvd();
        result = dao.create(testDvd);
        
        assertNotNull(result.getId());
        
    }
    
    @Test
    public void removeDvd(){

        Dvd toBeRemoved = dao.create(testDvd);
        
        List<Dvd> testList1 = dao.list();
        
        int sizeInitial = testList1.size();
        

        dao.delete(toBeRemoved);
        List<Dvd> testList2 = dao.list();
        int sizeAfterRemoval = testList2.size();

        assertEquals(sizeInitial - 1, sizeAfterRemoval);

    }
    
    @Test
    public void testReadDvd() {
        Dvd toBeRead = dao.create(testDvd);

        Dvd checkedDvd = dao.read(toBeRead.getId());

        assertEquals("The Matrix", checkedDvd.getTitle());
    }

    @Test
    public void testListDvd() {

        //Arrange
        Dvd actual = dao.create(testDvd);

        //Act
        List<Dvd> testList = dao.list();

        //Assert
        assertNotEquals(0, testList.size());
    }

    public void testUpdateDvd() {
        Dvd original = dao.create(testDvd);
        original.setTitle("Different");
        
        dao.update(original);
        
        Dvd edited = dao.read(original.getId());
        assertNotEquals(edited.getTitle(), original.getTitle());
        
    }
}
