/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daotests;

import com.mycompany.dvdlibraryapp.dao.NoteDao;
import com.mycompany.dvdlibraryapp.dto.Note;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class NoteDaoTest {
    
    NoteDao dao;
    
    Note testNote = new Note();
    
    public NoteDaoTest() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");
        this.dao = ctx.getBean("noteDao" , NoteDao.class);
    }

    
    @Before
    public void setUp() {
        
        testNote.setDvdId(1);
        testNote.setNote("This is a test note.");
        
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void addNote(){
        Note result = new Note();
        result = dao.create(testNote);
        
        assertNotNull(result.getId());
        
    }
    
    @Test
    public void removeNote(){

        Note toBeRemoved = dao.create(testNote);
        
        List<Note> testList1 = dao.dvdNotes(1);
        
        int sizeInitial = testList1.size();
        

        dao.delete(toBeRemoved);
        List<Note> testList2 = dao.dvdNotes(1);
        int sizeAfterRemoval = testList2.size();

        assertEquals(sizeInitial - 1, sizeAfterRemoval);

    }
    
    @Test
    public void testReadNote() {
        Note toBeRead = dao.create(testNote);

        Note checkedNote = dao.read(toBeRead.getId());

        assertEquals("This is a test note.", checkedNote.getNote());
    }

    @Test
    public void testListNote() {

        //Arrange
        Note actual = dao.create(testNote);

        //Act
        List<Note> testList = dao.dvdNotes(1);

        //Assert
        assertNotEquals(0, testList.size());
    }

    public void testUpdateNote() {
        Note original = dao.create(testNote);
        original.setNote("Different");
        
        dao.update(original);
        
        Note edited = dao.read(original.getId());
        assertNotEquals(edited.getNote(), original.getNote());
        
    }
}
