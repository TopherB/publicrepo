/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.aop;

import com.mycompany.flooringmastery.dao.AuditDao;
import com.mycompany.flooringmastery.dto.Audit;
import com.mycompany.flooringmastery.dto.Order;
import java.time.LocalDateTime;
import org.aspectj.lang.JoinPoint;

/**
 *
 * @author apprentice
 */
public class AuditAspect {
    
    private AuditDao aDao;
    
    public AuditAspect(AuditDao auditDao){
        aDao = auditDao;
    }
    
    public void logActions(JoinPoint jp){
        Audit newAudit = new Audit();
        
        Order tempOrder = (Order) jp.getArgs()[0];
        
        newAudit.setOrderId(tempOrder.getOrderNumber());
        newAudit.setOperation(jp.getSignature().getName());
        newAudit.setOrderDate(tempOrder.getOrderDate());
        newAudit.setAccessTime(LocalDateTime.now());
        
        aDao.logCreate(newAudit);
        
    }
    
}
