/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.controller;

import com.mycompany.flooringmastery.dao.OrderDao;
import com.mycompany.flooringmastery.dao.ProductDao;
import com.mycompany.flooringmastery.dao.TaxDao;
import com.mycompany.flooringmastery.dto.Order;
import com.mycompany.flooringmastery.dto.Product;
import com.mycompany.flooringmastery.dto.Tax;
import com.mycompany.projectconsoleio.ConsoleIO;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class FMController {

    private ConsoleIO io;
    private OrderDao oDao;
    private ProductDao pDao;
    private TaxDao tDao;

    private final String format = "MMddyyyy";
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);

    public FMController(OrderDao orderDao, ProductDao prodDao, TaxDao taxDao, ConsoleIO cIO) {
        this.oDao = orderDao;
        this.pDao = prodDao;
        this.tDao = taxDao;
        this.io = cIO;

    }

    public void run() {
        boolean playAgain = true;
        int choice;

        io.displayString("!!!Flooring Mastery!!!\n");

        while (playAgain) {
            io.displayString("\n===================================================");

            io.displayString("\nMain Menu\n");
            io.displayString("1. Display Orders");
            io.displayString("2. Add an Order");
            io.displayString("3. Edit an Order");
            io.displayString("4. Remove an Order");
            io.displayString("5. Admin Options");
            io.displayString("0. Quit");

            choice = io.getIntRange("What would you like to do?", 5, 0);

            switch (choice) {
                case 1:
                    displayOrders("\nEnter the date you would like to see orders from (mmddyyy)");
                    break;
                case 2:
                    addOrder();
                    break;
                case 3:
                    editOrder();
                    break;
                case 4:
                    removeOrder();
                    break;
                case 5:
                    adminOptions();
                    break;
                default:
                    io.displayString("\nBye bye, now");
                    playAgain = false;
                    break;

            }

        }

    }

    private void displayOrders(String prompt) {

        LocalDate trydate = io.getLocalDate(prompt, formatter);

        List<Order> tempList = oDao.getListByDate(trydate);

        while (tempList.isEmpty()) {
            io.displayString("\nNo orders found for " + trydate.format(formatter));
            trydate = io.getLocalDate(prompt, formatter);
            tempList = oDao.getListByDate(trydate);
        }

        io.displayString(String.format("%n%20s %s", "Orders from: ", trydate.format(formatter)));

        //display list
        displayOrderList(tempList);
    }

    private void addOrder() {
        Order newOrder = new Order();
        
        //ask for order date
        LocalDate date = io.getLocalDate("Which date will this order be taking place on? (Press enter for today)", formatter);

        String custName = io.getString("Enter the Customer Name:",true);
        Tax myTax = getTax("\nEnter the state this order taking place in: ", true);
        Product myProduct = getProduct("\nEnter the Product Type ordered: ", true);
        Double area = io.getDoubleRange("Enter the amount of product required (in sq/ft)", Double.MAX_VALUE, 1);

        //calculate and set all fields for newOrder
        newOrder = setOrderInfo(newOrder, myTax, myProduct, custName, area, date);

        displayOrderInfo(newOrder);
        String uSure = io.getString("\nEnter Y to commit new order to database.\nEnter N to discard.");

        if (uSure.equalsIgnoreCase("Y")) {
            oDao.create(newOrder);
            io.displayString("\nOrder Saved!");
        } else {
            io.displayString("\nOrder Discarded");
        }
    }

    private void editOrder() {
        displayOrders("\nEnter the date the order is from (mmddyyyy): ");

        //get order number
        Order edited = getOrder("Enter the order number you want to edit:");

        displayOrderInfo(edited);
        
        LocalDate myDate = io.getLocalDate("\nEnter the Order Date ("+edited.getOrderDate().format(formatter)+"):", formatter, false);
        String custName = io.getString("Enter the Customer Name (" + edited.getCustName() + "): ", false);
        Tax myTax = getTax("\nEnter the state this order taking place in (" + edited.getState() + "): ", false);
        Product myProduct = getProduct("\nEnter the Product Type ordered (" + edited.getProdType() + "): ", false);
        Double myArea = io.getDoubleRange("\nEnter the amount of product required ("+edited.getArea()+" sq/ft)", Double.MAX_VALUE, 1, false);
        
        
        if (custName.equals("")) {
            custName = edited.getCustName();
        }
        if (myTax == null) {
            myTax = tDao.read(edited.getState());
        }
        if (myProduct == null) {
            myProduct = pDao.read(edited.getProdType());
        }
        if (myArea == null){
            myArea = edited.getArea();
        }
        if(myDate==null || myDate.equals(edited.getOrderDate())){
            myDate=edited.getOrderDate();
            edited = setOrderInfo(edited, myTax, myProduct, custName, myArea, myDate );
            oDao.update(edited);
        }else{
            oDao.delete(edited);
            edited = setOrderInfo(edited, myTax, myProduct, custName, myArea, myDate );
            oDao.create(edited);
            io.displayString("\nOrder moved to "+ edited.getOrderDate().format(formatter));
        }

    }

    private void removeOrder() {
        displayOrders("\nEnter the date the order is from (mmddyyyy): ");

        //get order number
        Order toBeDeleted = getOrder("\nEnter the order number you want to delete:");

        //displayOrderInfo
        displayOrderInfo(toBeDeleted);

        //Check if sure
        String uSure = io.getString("\nEnter Y to delete order from database.\nEnter N to return to Main Menu.");

        if (uSure.equalsIgnoreCase("Y")) {
            oDao.delete(toBeDeleted);
            io.displayString("\nOrder was deleted from database.");
        } else {
            io.displayString("\nOrder was not deleted.");
        }

    }

    private void adminOptions(){
        
        io.displayString("ADMIN MENU");
        io.displayString("1. Edit Taxes");
        io.displayString("2. Edit Products");
        io.displayString("3. View Log");
        io.displayString("0. Go Back");
        
    }
    
    private Tax getTax(String prompt, boolean isRequired) {
        List<Tax> taxList = tDao.getTaxList();
        String whichState;
        List<String> stateNames = taxList.stream().map(s -> s.getState().toUpperCase()).collect(Collectors.toList());

        displayStates(taxList);

        //Make StringOptions thing so it will only either return one of those state names or ""
        whichState = io.getStringFromChoices(prompt, isRequired, stateNames);

        //Match the name of the state to the Tax object and return it
        //if string is null, return null Tax;
        if (whichState.equals("")) {

            return null;

        } else {

            return tDao.read(whichState);

        }

    }

    private Product getProduct(String prompt, boolean isRequired) {
        List<Product> productList = pDao.getProductList();
        String whichProduct;
        List<String> productNames = productList.stream().map(p -> p.getProdType().toUpperCase()).collect(Collectors.toList());

        displayProducts(productList);

        //Make StringOptions thing so it will only either return one of those state names or ""
        whichProduct = io.getStringFromChoices(prompt, isRequired, productNames);

        //Match the name of the state to the Tax object and return it
        //if string is null, return null Tax;
        if (whichProduct.equals("")) {

            return null;

        } else {

            return pDao.read(whichProduct);

        }
    }

    private Order getOrder(String prompt) {
        List<Order> tempList = oDao.listOrders();

        boolean valid = false;

        while (!valid) {
            int choice = io.getInt(prompt);
            for (Order o : tempList) {
                if (o.getOrderNumber() == choice) {
                    return o;
                }
            }
            io.displayString("\nNo orders found with ID# " + choice);
        }

        return null;

    }

    private void displayStates(List<Tax> taxList) {
        io.displayString("\nID        State          Tax Rate");
        io.displayString("--        -----          ---------");
        for (Tax t : taxList) {
            io.displayString(String.format("%n%-9d %-14s %-1.2f%%", t.getTaxId(), t.getState(), t.getTaxRate()));
        }
    }

    private void displayProducts(List<Product> productList) {
        io.displayString("\nID        Product Type   Material Cost(per sq/ft) Labor Cost(per sq/ft) ");
        io.displayString("--        ------------   ------------------------ ---------------------");
        productList.stream().forEach((p) -> {
            io.displayString(String.format("%n%-9d %-14s $%-23.2f $%-10.2f", p.getProdId(), p.getProdType(), p.getMatCostPerSqFt(), p.getLaborCostPerSqFt()));
        });
    }

    private void displayOrderInfo(Order o) {
        io.displayString("\n                 Order Information");
        io.displayString("-----------------------------------------------------");
        io.displayString(String.format("%-25s%-20s", "Customer Name:", o.getCustName()));
        io.displayString(String.format("%n%-25s%-20s", "State:", o.getState()));
        io.displayString(String.format("%-25s%-20.2f%%", "Tax Rate:", o.getTaxRate()));
        io.displayString(String.format("%n%-25s%-20s", "Product Type:", o.getProdType()));
        io.displayString(String.format("%-25s%-20.2f", "Area:", o.getArea()));
        io.displayString(String.format("%-25s$%-20.2f", "Mat Cost (sq/ft):", o.getMatCostPerSqFt()));
        io.displayString(String.format("%-25s$%-20.2f", "Labor Cost ((sq/ft):", o.getLaborCostPerSqFt()));
        io.displayString(String.format("%n%-25s$%-20.2f", "Total Material Cost:", o.getMatCost()));
        io.displayString(String.format("%-25s$%-20.2f", "Total Labor Cost:", o.getLaborCost()));
        io.displayString(String.format("%-25s$%-20.2f", "Total Tax:", o.getTaxTotal()));
        io.displayString(String.format("%n%-25s$%-20.2f", "Final Total:", o.getTotalCost()));

    }

    private void displayOrderList(List<Order> tempList) {

        io.displayString("Order #    Customer Name      State   Product      Area       Total");
        io.displayString("-------    -------------      -----   -------      ----       -----");

        for (Order o : tempList) {
            io.displayString(String.format("%-10d %-18s %-7s %-12s %-6.2f %10.2f", o.getOrderNumber(),
            o.getCustName(), o.getState(),o.getProdType(), o.getArea(), o.getTotalCost()));
        }
    }

    private double rnd(double d) {
        return (Math.round(d * 100.0) / 100.0);
    }

    private Order setOrderInfo(Order myOrder, Tax myTax, Product myProduct, String custName, double area , LocalDate date) {

        myOrder.setCustName(custName);
        myOrder.setState(myTax.getState());
        myOrder.setTaxRate(myTax.getTaxRate());
        myOrder.setProdType(myProduct.getProdType());
        myOrder.setArea(area);
        myOrder.setMatCostPerSqFt(myProduct.getMatCostPerSqFt());
        myOrder.setLaborCostPerSqFt(myProduct.getLaborCostPerSqFt());

        //calculated parts
        myOrder.setMatCost(rnd(myProduct.getMatCostPerSqFt() * area));
        myOrder.setLaborCost(rnd(myProduct.getLaborCostPerSqFt() * area));
        myOrder.setTaxTotal(rnd((myTax.getTaxRate() / 100) * (myOrder.getLaborCost() + myOrder.getMatCost())));
        myOrder.setTotalCost(rnd(myOrder.getLaborCost() + myOrder.getMatCost() + myOrder.getTaxTotal()));
        
        myOrder.setOrderDate(date);

        return myOrder;
    }

}
