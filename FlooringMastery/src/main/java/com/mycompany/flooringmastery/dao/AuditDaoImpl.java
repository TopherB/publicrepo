/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dao;

import com.mycompany.flooringmastery.dto.Audit;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class AuditDaoImpl implements AuditDao {
    
    private String fileName = "log.txt";
    private static final String ENCODETOKEN = ",";
    private static final String DECODETOKEN = ",(?!\\\\)";
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
    
    List<Audit> auditList = new ArrayList();
    int nextId=1;
    
    public AuditDaoImpl(){
        auditList = decode();
        
        if(auditList.size()==0){
            nextId = 1;
        }else{
            for(Audit a : auditList){
                if(a.getAuditId()>=nextId){
                    nextId = a.getAuditId()+1;
                }
            }
        }
    }
    
    @Override
    public void logCreate(Audit newAudit){
        //need nextId
        
        newAudit.setAuditId(nextId);
        nextId++;
        
        auditList.add(newAudit);
        encode();
        
    }
    
    
    private void encode() {
        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter(fileName));

            for (Audit a : auditList) {
                
                out.print(a.getAuditId());
                out.print(ENCODETOKEN);

                out.print(a.getOrderId());
                out.print(ENCODETOKEN);
                
                out.print(a.getOperation());
                out.print(ENCODETOKEN);
                
                out.print(a.getOrderDate().format(formatter));
                out.print(ENCODETOKEN);

                out.print(a.getAccessTime());
                out.println("");

            }
        } catch (IOException ex) {

        } finally {
            out.close();
        }
    }
    
    private List<Audit> decode() {
        List<Audit> tempList = new ArrayList();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(fileName)));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split(DECODETOKEN);
                Audit myAudit = new Audit();

                int auditId = Integer.parseInt(stringParts[0]);
                myAudit.setAuditId(auditId);
                
                int orderId = Integer.parseInt(stringParts[1]);
                myAudit.setOrderId(orderId);

                myAudit.setOperation(stringParts[2]);
                
                LocalDate orderDate = LocalDate.parse(stringParts[3], formatter);
                myAudit.setOrderDate(orderDate);
                
                LocalDateTime myAccessTime = LocalDateTime.parse(stringParts[4]);
                myAudit.setAccessTime(myAccessTime);

                tempList.add(myAudit);

            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        return tempList;
    }
    
}
