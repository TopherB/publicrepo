/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dao;

import com.mycompany.flooringmastery.dto.Order;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface OrderDao {

    Order create(Order myOrder);
    Order read(int orderId);
    void update(Order editedOrder);
    void delete(Order toBeDeleted);

    List<Order> listOrders();
    List<Order> getListByDate(LocalDate date);
    
    
}
