/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dao;

import com.mycompany.flooringmastery.dto.Order;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author apprentice
 */
public class OrderDaoImpl implements OrderDao {
    
    private static File openFile;
    private static final String ENCODETOKEN = ",";
    private static final String DECODETOKEN = ",(?!\\\\)";
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");

    List<Order> orderList;
    private Integer nextId = 1;

    public OrderDaoImpl() {

    }

    @Override
    public Order create(Order newOrder) {
        
        openFile = createOrderFile(newOrder.getOrderDate());

        orderList = decode(openFile); //converts todays OrderFile to List
        
        findNextId();
        newOrder.setOrderNumber(nextId); //sets the orderNumber to the nextId in the List
        
        orderList.add(newOrder); //add the order to the list

        encode(openFile); //encode the list

        return newOrder;
    }

    @Override
    public Order read(int orderId) {
        for(Order o : orderList){
            if(o.getOrderNumber()==orderId){
                return o;
            }
        }

        return null;    
    }

    @Override
    public void update(Order editedOrder) {
        for(Order o : orderList){
            if(o.getOrderNumber()==editedOrder.getOrderNumber()){
                orderList.set(orderList.indexOf(o), editedOrder);
                break;
            }
        }
        encode(openFile);
    }

    @Override
    public void delete(Order toBeDeleted) {
        for(Order o : orderList){
            if(o.getOrderNumber()==toBeDeleted.getOrderNumber()){
                orderList.remove(toBeDeleted);
                break;
            }
        }
        encode(openFile);
    }

    
    private void findNextId() {
        if (orderList == null) {
            nextId = 1;
        } else {
            for (Order o : orderList) {

                if (o.getOrderNumber() >= nextId) {
                    nextId = o.getOrderNumber() + 1;
                }
            }
        }
    }

    @Override
    public List<Order> listOrders() {
        return new ArrayList(orderList);
    }

    private void encode(File fileName) {
        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter(fileName));

            for (Order o : orderList) {

                out.print(o.getOrderNumber());
                out.print(ENCODETOKEN);

                out.print(sanitize(o.getCustName()));
                out.print(ENCODETOKEN);

                out.print(o.getState());
                out.print(ENCODETOKEN);

                out.print(o.getTaxRate());
                out.print(ENCODETOKEN);

                out.print(o.getProdType());
                out.print(ENCODETOKEN);

                out.print(o.getArea());
                out.print(ENCODETOKEN);

                out.print(o.getMatCostPerSqFt());
                out.print(ENCODETOKEN);

                out.print(o.getLaborCostPerSqFt());
                out.print(ENCODETOKEN);

                out.print(o.getMatCost());
                out.print(ENCODETOKEN);

                out.print(o.getLaborCost());
                out.print(ENCODETOKEN);

                out.print(o.getTaxTotal());
                out.print(ENCODETOKEN);

                out.print(o.getTotalCost());
                out.print(ENCODETOKEN);
                
                out.print(o.getOrderDate().format(formatter));
                out.println("");
                

            }
        } catch (IOException ex) {

        } finally {
            out.close();
        }
    }

    private List<Order> decode(File fileName) {
        List<Order> tempList = new ArrayList();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(fileName)));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split(DECODETOKEN);
                stringParts[1] = unsanitize(stringParts[1]);
                Order myOrder = new Order();

                int ordNum = Integer.parseInt(stringParts[0]);
                myOrder.setOrderNumber(ordNum);

                myOrder.setCustName(stringParts[1]);
                myOrder.setState(stringParts[2]);

                double taxRate = Double.parseDouble(stringParts[3]);
                myOrder.setTaxRate(taxRate);

                myOrder.setProdType(stringParts[4]);

                double area = Double.parseDouble(stringParts[5]);
                myOrder.setArea(area);

                double matPer = Double.parseDouble(stringParts[6]);
                myOrder.setMatCostPerSqFt(matPer);

                double laborPer = Double.parseDouble(stringParts[7]);
                myOrder.setLaborCostPerSqFt(laborPer);

                double matCost = Double.parseDouble(stringParts[8]);
                myOrder.setMatCost(matCost);

                double laborCost = Double.parseDouble(stringParts[9]);
                myOrder.setLaborCost(laborCost);

                double taxTotal = Double.parseDouble(stringParts[10]);
                myOrder.setTaxTotal(taxTotal);

                double totalCost = Double.parseDouble(stringParts[11]);
                myOrder.setTotalCost(totalCost);
                
                LocalDate orderDate = LocalDate.parse(stringParts[12],formatter);
                myOrder.setOrderDate(orderDate);

                tempList.add(myOrder);

            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        openFile = fileName;
        return tempList;
    }
    
    

    //Makes a File "OrderFiles/Orders_MMddyyyy.txt". 
    //If the file exists it does not create it, otherwise it does write it to disc
    
    private File createOrderFile(LocalDate date) {
        

        String currentDateString = "OrderFiles/Orders_"+date.format(formatter)+".txt";
        File currentDateFileName = new File(currentDateString);
        if (!currentDateFileName.isFile()) {
            try {
                currentDateFileName.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(OrderDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        
        return currentDateFileName;
    }
    
    @Override
    public List<Order> getListByDate(LocalDate date){
        String userFileName = ("OrderFiles/Orders_" + date.format(formatter).toString() + ".txt");
        File userFile = new File(userFileName);

        File folder = new File("OrderFiles/");
        File[] listOfFiles = folder.listFiles();
        
        for (File f : listOfFiles) {
            if (f.equals(userFile)) {
                orderList = decode(userFile);
                return listOrders();
            }
        }
        
        List<Order> blankList = new ArrayList();
        return blankList;
    }
    
    private String sanitize(String s){
        
        s= s.replaceAll("\\\\n", "n");
        s= s.replaceAll(",", ",\\\\");
        
        return s;
    }
    private String unsanitize(String s){
  
            s= s.replaceAll(",\\\\", ",");
        
        return s;
    }
           
    

}
