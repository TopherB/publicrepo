/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dao;

import com.mycompany.flooringmastery.dto.Order;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author apprentice
 */
public class OrderDaoInMemoryImpl implements OrderDao {
    
    
    
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");

    private Integer nextId = 1;
    List<Order> orderList = new ArrayList();

    public OrderDaoInMemoryImpl() {

    }

    @Override
    public Order create(Order newOrder) {
        
        findNextId();
        newOrder.setOrderNumber(nextId);
        orderList.add(newOrder); //add the order to the list

        return newOrder;
    }

    @Override
    public Order read(int orderId) {
        for(Order o : orderList){
            if(o.getOrderNumber()==orderId){
                return o;
            }
        }

        return null;
    }

    @Override
    public void update(Order editedOrder) {
        for(Order o : orderList){
            if(o.getOrderNumber()==editedOrder.getOrderNumber()){
                orderList.set(orderList.indexOf(o), editedOrder);
                break;
            }
        }

    }

    @Override
    public void delete(Order toBeDeleted) {
        for(Order o : orderList){
            if(o.getOrderNumber()==toBeDeleted.getOrderNumber()){
                orderList.remove(toBeDeleted);
                break;
            }
        }

    }

    private void findNextId() {
        if (orderList == null) {
            nextId = 1;
        } else {
            for (Order o : orderList) {

                if (o.getOrderNumber() >= nextId) {
                    nextId = o.getOrderNumber() + 1;
                }
            }
        }
    }

    @Override
    public List<Order> listOrders() {
        return new ArrayList(orderList);
    }

    
    
    @Override
    public List<Order> getListByDate(LocalDate date){

        
        List<Order> blankList = new ArrayList();
        return blankList;
    }
    
   


}
