/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dao;

import com.mycompany.flooringmastery.dto.Product;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ProductDaoImpl implements ProductDao {
    
    private static String FILENAME = "ProductData.txt";
    private static final String TOKEN = ",";
    
    private List<Product> productList = new ArrayList();
    
    public ProductDaoImpl (){
        
        productList = decode();
        
    }
    
    @Override
    public Product read(String productName){
        for(Product p : productList){
            if(p.getProdType().equalsIgnoreCase(productName)){
                return p;
            }
        }
        
        return null;
    }

    
    private List<Product> decode() {
        List<Product> tempProdList = new ArrayList();
        
        try{
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));
            
            while(sc.hasNextLine()){
                
                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split(TOKEN);
                Product myProduct = new Product();
                
                int prodId = Integer.parseInt(stringParts[0]);
                myProduct.setProdId(prodId);
                myProduct.setProdType(stringParts[1]);
                double matCost = Double.parseDouble(stringParts[2]);
                myProduct.setMatCostPerSqFt(matCost);
                double laborCost = Double.parseDouble(stringParts[3]);
                myProduct.setLaborCostPerSqFt(laborCost);
                
                tempProdList.add(myProduct);
            }
        }catch (FileNotFoundException ex){
            
        }
        
        return tempProdList;
    }
    
    public List<Product> getProductList(){
        return new ArrayList(productList);
    }
    
    
    public Product create(Product newProduct){
        
        return null;
    }
    
    public void update(Product changedProduct){
        
    }
    
    public void delete(Product toBeDeleted){
        
    }
    
    public void encode(){
        
    }


    
}
