/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dao;

import com.mycompany.flooringmastery.dto.Tax;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class TaxDaoImpl implements TaxDao {
    
    private static String FILENAME = "TaxData.txt";
    private static final String TOKEN = ",";
    
    private List<Tax> taxList = new ArrayList();
    
    public TaxDaoImpl (){
        
        taxList = decode();
        
    }
    
    @Override
    public Tax read(String stateName){
        for(Tax t: taxList){
            if(t.getState().equalsIgnoreCase(stateName)){
                return t;
            }
        }
        
        return null;
    }

    
    private List<Tax> decode() {
        List<Tax> tempTaxList = new ArrayList();
        
        try{
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));
            
            while(sc.hasNextLine()){
                
                String currentLine = sc.nextLine();
                String[] stringParts = currentLine.split(TOKEN);
                Tax myTax = new Tax();
                
                int taxId = Integer.parseInt(stringParts[0]);
                myTax.setTaxId(taxId);
                myTax.setState(stringParts[1]);
                double taxRate = Double.parseDouble(stringParts[2]);
                myTax.setTaxRate(taxRate);

                
                tempTaxList.add(myTax);
            }
        }catch (FileNotFoundException ex){
            
        }
        
        return tempTaxList;
    }
    
    public List<Tax> getTaxList(){
        return new ArrayList(taxList);
    }
    
    public Tax create(Tax newTax){
        
        return null;
    }
    
    public void update(Tax changedTax){
        
    }
    
    public void delete(Tax toBeDeleted){
        
    }
    
    public void encode(){
        
    }
}
