/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dto;



/**
 *
 * @author apprentice
 */
public class Product {
    
    private int prodId;
    private String prodType;
    private double matCostPerSqFt;
    private double laborCostPerSqFt;

    public int getProdId() {
        return prodId;
    }

    public String getProdType() {
        return prodType;
    }

    public double getMatCostPerSqFt() {
        return matCostPerSqFt;
    }

    public double getLaborCostPerSqFt() {
        return laborCostPerSqFt;
    }

    public void setProdId(int prodId) {
        this.prodId = prodId;
    }

    public void setProdType(String prodType) {
        this.prodType = prodType;
    }

    public void setMatCostPerSqFt(double matCostPerSqFt) {
        this.matCostPerSqFt = matCostPerSqFt;
    }

    public void setLaborCostPerSqFt(double laborCostPerSqFt) {
        this.laborCostPerSqFt = laborCostPerSqFt;
    }
    
    
    
    
}
