/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testFlooringMastery;

import com.mycompany.flooringmastery.dao.OrderDao;
import com.mycompany.flooringmastery.dao.ProductDao;
import com.mycompany.flooringmastery.dao.TaxDao;
import com.mycompany.flooringmastery.dto.Order;
import com.mycompany.projectconsoleio.ConsoleIO;
import java.time.LocalDate;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class testFMDao {
    
    OrderDao oDao;
    ConsoleIO io;
   // TaxDao tDao;
   // ProductDao pDao;
    
    Order testOrder = new Order();
    List<Order> testList;
    
    LocalDate date = LocalDate.now();
    
    public testFMDao() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        this.oDao = ctx.getBean("orderDao" , OrderDao.class);
       // this.tDao = ctx.getBean("taxDao" , TaxDao.class);
      //  this.pDao = ctx.getBean("productDao", ProductDao.class);
        
    }
    
    @Before
    public void setUp() {
        
        testOrder.setCustName("Topher Benjamin");
        testOrder.setState("MI");
        testOrder.setProdType("Wood");
        testOrder.setArea(100);
        
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void addOrder(){
        Order result = new Order();
        result = oDao.create(testOrder);
        
        assertNotNull(result.getOrderNumber());
        
    }
    
    @Test
    public void removeOrder(){

        Order toBeRemoved = oDao.create(testOrder);
        
        List<Order> testList1 = oDao.listOrders();
        
        int sizeInitial = testList1.size();
        

        oDao.delete(toBeRemoved);
        List<Order> testList2 = oDao.listOrders();
        int sizeAfterRemoval = testList2.size();

        assertEquals(sizeInitial - 1, sizeAfterRemoval);

    }
    
    @Test
    public void testReadOrder() {
        Order toBeRead = oDao.create(testOrder);

        Order checkedOrder = oDao.read(toBeRead.getOrderNumber());

        assertEquals("Topher Benjamin", checkedOrder.getCustName());
    }

    @Test
    public void testListOrder() {

        //Arrange
        Order actual = oDao.create(testOrder);

        //Act
        List<Order> testList = oDao.listOrders();

        //Assert
        assertNotEquals(0, testList.size());
    }

    public void testUpdateOrder() {
        Order original = oDao.create(testOrder);
        original.setCustName("Different");
        
        oDao.update(original);
        
        Order edited = oDao.read(original.getOrderNumber());
        assertNotEquals(edited.getCustName(), original.getCustName());
        
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
